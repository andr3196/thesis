
"""
Author: Andreas Springborg
Date: 2019-03-22
Goal:

"""
import matplotlib
matplotlib.use("TkAgg")

from matplotlib import pyplot
from matplotlib import cm
import numpy as np


def make_color_list(n):
    viridis = cm.get_cmap('viridis', 20)
    x = np.linspace(0, 1, n)
    return viridis(x)
