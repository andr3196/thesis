
"""
Author: Andreas Springborg
Date: 2019-04-05
Goal:

"""
import platform
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.family'] = "serif"
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rc('text.latex', preamble='\\usepackage{amsmath},\\usepackage{amssymb}')
if platform.system() == "Darwin":
    from .matplotlib_mod import pyplot
else:
    from matplotlib import pyplot
import matplotlib.gridspec as gridspec
