
# About
This code base contains the code base for the analysis of
- Problems of random bilinear Hamiltonians
- A simple Ising model spin chain
- Added random tunable couplings to the Ising model


The overall structure of the code base is:

- Framework:
    - clustering: clustering routines and Hopkins metric
    - collection: Objects for uniform landscape sampling
    - control: Manipulations of control signals
    - fidelity: Interpolation of fidelities
    - io: Input and output of files
    - models: ML models - default definitions and building routines
    - noise: High frequency detection in control signals
    - optim: Wrappers for QuTiP
    - problems: Definitions of objects for the systems studied
    - procedure: Routines for moving data around, loading etc.
    - qsl: Loading precomputed Tmin
    - quantum: Definitions of states and unitaries. Functions for generating quantum problems
    - termination: Default termination conditions
    - utility: Helper functions to make end meet.


- Scenarios:
    - benchmark: The benchmark of different ML models.
        - random: Figure 5.2
    - controls: Examples of what optimal controls look like
        - spinchain: Figure 3.3
    - filtering: The fidelity as a function the filter strength
        - spinchain: Figure 3.4
    - landscape: Mean distance and fidelity metrics as a function of duration
        - spinchain: Figure 3.2 (a)
        - randspinchain: Figure 3.9
        - spinglass: Figure 3.12
    - perturbation: The local decrease in fidelity away from an optimum in a random direction
        - spinchain: Figure 3.8
    - ridges:
        - spinchain: Figures 3.5 (a) and (b)


In each scenario the file "core.py" contains the common routines for realising the scenario.
In each scenario/setup we define the computation in "display.py" and generate the figures in "display.py".



# Initially
To run the code ensure that:
- The necessary dependencies are downloaded. The necessary requirements are listed in "requirements.txt"
- The data folder called "distance" is downloaded and placed as a subfolder of "data"
- The data folder called "ml_training_data" is downloaded and placed as a subfolder of "data"

The data/collected folder must contain the following structure:

```
collected
+-- run1
|   +-- phase
|   +-- statistic
|
+-- run2
|   +-- annealing
|   +-- swapping
|
+-- run5
|
+-- run6
|   +-- clustering
|   +-- filtering
|   +-- interpolations
|   +-- metrics
|   +-- perturbation
|   +-- swapping
|
+-- run7
```

In addition, it is necessary to add the folder "model_errors" in the "data" folder to save the errors after training models. 


# The data
Most data takes the form of a triple:
"seeds": A matrix of shape (n_samples, n_features) with each row representing a randomly genrated seed.
"ctrls": A matrix of shape (n_samples, n_features) with each row representing an optimised control
"fids": A vector of shape (n_samles,) with the fidelity in ```fids[i]``` being the fidelity of ```ctrls[i,:]```.

We refer to each of these as "fields" of the numpy FileObject yielded by np.load(...).

Thus, for files such as f = "spinchain_L_6_J_1_g_1_Ti_8.npz", we have
```
fileobj = np.load(f)

seeds = fileobj["seeds"]
ctrls = fileobj["ctrls"]
fids = fileobj["fids"]
```


# Components
The main modes of analysis are
- ridges
- correlation
- machine learning
- metrics

In this project we consider bilinear control Hamiltonians:

 H(t) = Hd + b c(t) H_c,
 where Hd is the drift Hamiltonian, Hc the control Hamiltonian, b>0 the control scale, and -1 <= c <= 1 is the control signal.

Common parameters of different problems are:
- n: The Hilbert space dimension corresponding to order of the drift and control matrices
- p: Indicator for the exact realisation of a random Hamiltonian. Concretely, this integer is used as a seed for the random number generator in QuTiP used to generate the drift and control Hamiltonians as well as the unitary target or target state.
- b: The control scale meaning the magnitude (bounds) of the control field
- T: The duration of the control such that the control is divided into N_ts timesteps between 0 and T.



For each scenario multiple physical systems may be studied. These are separated into subfolders as follows:
- main: An example problem n=4, p=0, b=3 scanned more densely (compared the random Hamiltonians below).
- random: The general problem with n=2,3,..., 11, p = 0, 1, ..., 19, b=3 and durations T/T_min = 0.2, 0.3, ..., 1.5
- spinchain: The simple control control of L spins in a loop with coupling around the loop:

    H = - 1/2 sum_i( gZ_i + bc(t)X_i + J/2 Z_iZ_{i+1})

where X, Z are Pauli spin matrices and J, g are coupling contants.
- randspinchain: A simple extension of the spin chain:

    H = - 1/2 sum_i( gZ_i + bc(t)X_i + (J + a*J_i)/2 Z_iZ_{i+1}).

    Here a is a tuning parameter and J_i are random values

- spinglass: A further extension of the spin chian problem:

    H = - 1/2 (sum_i( gZ_i + bc(t)X_i + J/2 Z_iZ_{i+1}) + sum_i(sum_j(J_ij/2 Z_iZ_j))).

