close all
%% In this file we produce plots from the model scans

mode = 1;

%% First we load data
base_path = get_data_basepath('singlequbit/analysis/fitting/collected/mat');
%% Data filename
if mode == 1 | mode == 4
data_filename = 'new_fitted_models_data.mat';
full_file = fullfile(base_path, data_filename);
models_data = load(full_file);
elseif mode == 5
data_filename = 'new_fitted_models_seeds.mat';
full_file = fullfile(base_path, data_filename);
models_data = load(full_file);
else 
data_filename = 'fitted_models_data_full.mat';
full_file = fullfile(base_path, data_filename);
models_data_meta = load(full_file);
models_data = models_data_meta.models_data;
end



%                   1           2            3             4            5
model_names = {"Basic_MLP", "Deep_MLP", "Shallow_CNN", "Deep_CNN", "Shallow_LSTM", "Deep_LSTM", "Shallow_GRU", "Deep_GRU", "XGB", "ADA", "SVR_RBF", "KNN", "Random_Baseline"};
%model_names = {"Shallow_LSTM", "Shallow_GRU", "Random_Baseline"};
view_line = zeros(size(model_names));
view_line(4) = 1;
view_line(6) = 1;
view_line(8) = 1;
%view_line(3) = 1;
view_line(13) = 1;
n_values = 2:7;





switch mode
    
    case 1
        % the plot of the MAE as a function of the problem size n
        plot1(models_data, model_names, n_values, view_line)
    case 2 
        % the plot of the MAE as a fct of number of layers
        plot2(models_data)
    case 3
        % the plot of the MAE as a fct of the amount of data
        plot3(models_data)
    case 4
        % the plot of the MAE as a function of the numer of parameters in
        % each model
        plot4(models_data, model_names)
    case 5
        % Fit Deep_CNN to (seeds, F) 
        plot5(models_data, n_values)
end




function plot1(models_data, model_names, n_values, view_line)
f = figure;
colors = distinguishable_colors(length(model_names));


line_styles = {'-', '--', ':','-.'};

marker_styles = '+o*xsd^v<>ph';
n_markers = length(marker_styles);

mid_point = floor(length(model_names)/2);

field = 'maes'; % 'maes'

hAx(1) = axes();
ax = gca;
ax.FontSize = 14;
hold on
for i = 1:mid_point+2
    
    
    
    name = model_names{i};
    model_data = models_data.(name);
    model_data_mean = mean(model_data.(field),1, 'omitnan');
    %plot(n_values, model_data_mean, 'Parent',hAx(1), 'Color', colors(i,:), 'LineStyle', line_styles{mod(i, length(line_styles)) + 1}, 'Linewidth', 2);
    p = plot(n_values, model_data_mean, 'Parent',hAx(1), 'Color', colors(i,:), 'Marker', marker_styles(i), 'LineStyle', '-', 'Linewidth', 2);
    if ~ view_line(i) == 1
        p.Visible = 'off';
    end
end

l1 = legend(cellfun(@(x) strrep(x,"_", " "), model_names(1:mid_point+2)), 'Location', 'best', 'Color', 'w', 'Fontsize', 12);
l1_pos = l1.Position;
l1_pos(1) = 0.15;
l1_pos(2) = 0.62;
l1.Position = l1_pos;

hAx(2) = copyobj(hAx(1),gcf);
delete( get(hAx(2),'Children') ) 
set(hAx(2), 'Color','none', 'XTick',[], 'YTick',[], 'Box','off')
p = gobjects(0);
for i = mid_point+3:length(model_names)
    
    
    if i > n_markers
        mkr = 'none';
        ls = line_styles{i - n_markers};
        
    else
        mkr = marker_styles(i);
        ls = '-';
        
    end
    
    name = model_names{i};
    model_data = models_data.(name);
    model_data_mean = mean(model_data.(field),1, 'omitnan');
    %p(end+1) = plot(n_values, model_data_mean,'Parent', hAx(2), 'Color', colors(i,:), 'LineStyle', line_styles{mod(i, length(line_styles)) + 1}, 'Linewidth', 2);
    p(end+1) = plot(n_values, model_data_mean,'Parent', hAx(2), 'Color', colors(i,:), 'Marker', mkr, 'LineStyle', ls, 'Linewidth', 2);
    if ~ view_line(i) == 1
        pn = p(end);
        pn.Visible = 'off';
    end
end

if strcmp(field, 'maes')
    y_limits = [0 0.35];
else
    y_limits = [0 0.5];
end

ylim(hAx(1), y_limits)
ylim(hAx(2), y_limits) 
legend(p, cellfun(@(x) strrep(x,"_", " "), model_names(mid_point+3:end)), 'Location', 'north', 'Color', 'none', 'Fontsize', 12)

xlabel('Dimension, $n$', 'FontSize', 17, 'Interpreter', 'latex')
xticks(2:7)
ylabel('Mean absolute error', 'FontSize', 17, 'Interpreter', 'latex')
 
if strcmp(field, 'maes')
    filename = 'model_scan_total.pdf';
else
    filename = 'model_scan_high.pdf';
end 
box on
%print(filename, '-dpdf')
%print_to_images(filename)
f.Color = 'w';
end


function plot2(models_data)

data = models_data.('MLP_L_scan');
n_layers = 1:20;

plot(n_layers, data.maes)

xlabel('Number of layers, L')
ylabel('MAE')

print('layer_scan.pdf', '-dpdf')

end

function plot3(models_data)

data = models_data.('MLP_data_amount_scan');
n_samples = double(data.training_samples);

%f = fit( log(n_samples).', data.maes.', 'poly1');
semilogx(n_samples, data.maes)
%hold on
%plot(n_samples, exp(f(log(n_samples))), 'r-')
%semilogx(n_samples, f(n_samples), 'r-')

xlabel('Number of training samples, N_{samples}', 'FontSize', 15)
ylabel('MAE', 'FontSize', 15)

print('data_scan.pdf', '-dpdf')
end

function plot4(models_data, model_names)

num_params = [14593, 185857, 11901, 232161, 51001, 235001, 40801, 184601];
n_7_mean_mae = zeros(size(num_params));
hold on 
for i = 1:length(num_params)
    m_name = model_names{i};
    np_data = models_data.(m_name);
    means = mean(np_data.maes, 1, 'omitnan');
    n_7_mean_mae(i) = means(end);
    plot(num_params(i), n_7_mean_mae(i), 'bx')
    disp_name = strrep(m_name,"_", " ");
    if i == 5 
        y_shift = 0.0015;
    else
        y_shift = -0.0005;
    end
    text(num_params(i) + 2000, n_7_mean_mae(i) + y_shift, disp_name, 'Fontsize', 13)
end
xlabel('Number of parameters', 'Fontsize', 15)
ylabel('MAE for n = 7',  'Fontsize', 15)
xlim([0 3e5])

print('Model_complexity.pdf', '-dpdf')
end


function plot5(models_data, n_values)

%colors = distinguishable_colors(length(model_names));


%line_styles = {'-', '--', ':','-.'};

%marker_styles = '+o*xsd^v<>ph';
%n_markers = length(marker_styles);

mid_point = 1; %floor(length(model_names)/2);

field = 'maes'; % 'maes'

hAx(1) = axes();
hold on
for i = 1:mid_point+2

    model_data = models_data.('Deep_CNN');
    model_data_mean = mean(model_data.(field),1, 'omitnan');
    %plot(n_values, model_data_mean, 'Parent',hAx(1), 'Color', colors(i,:), 'LineStyle', line_styles{mod(i, length(line_styles)) + 1}, 'Linewidth', 2);
    plot(n_values, model_data_mean, 'Parent',hAx(1), 'Color', 'b', 'LineStyle', '-', 'Linewidth', 2);
end

l1 = legend(cellfun(@(x) strrep(x,"_", "\_"), model_names(1:mid_point+2)), 'Location', 'best', 'Color', 'w');

xlabel('Dimension, $n$', 'FontSize', 17, 'Interpreter', 'latex')
xticks(2:7)
ylabel('Mean absolute error', 'FontSize', 17, 'Interpreter', 'latex')

if strcmp(field, 'maes')
    filename = 'model_scan_total_seeds.pdf';
else
    filename = 'model_scan_high.pdf';
end 
%print(filename, '-dpdf')
%print_to_images(filename)
end



