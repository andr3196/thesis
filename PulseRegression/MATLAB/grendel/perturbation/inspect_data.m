close all

%% Here we study local aspects of the data

data_meta = load_perturb_data('perturbation_scan2.mat');
data = data_meta.data; % input size = (nn, np, nb, nseeds, nAmpls, nT)
durations = data_meta.durations;

mode = 9;

switch mode
    
    case 1
        % Increasing b should always improve the unperturbed control, as
        % one can simply lower the control correspondingly to compensate
        inspect_data1(data)
        
    case 2
        % See if there are interesting features for b=0.5
        inspect_data2(data)
    case 3
        % See if data can be fitted with exponential
        inspect_data3(data)
    case 4
        
        inspect_data4(data)
    case 5
        
        inspect_data5(data)
    case 6
        % See if Lmean 1 step from optimum is lowest around TQS
        inspect_data6(data)
        
    case 7
        % See the distribution of 1 step drops for each time
        inspect_data7(data)
        
    case 8
        % See if there is a pattern in number of unique solutions
        inspect_data8(data)
        
    case 9
        % See if there is a pattern in the avgerage drop across seeds and
        % problems
        inspect_data9(data)
        
        
end




function inspect_data1(data)

n = 2;
p = 4;
i_p = p + 1;
i_n = n-1;

b_leg = {'0.5', '1.0', '3.0'};

hold on
for bi = 1:3
    
    times = 1:9;
    d = squeeze(data(i_n, i_p, bi, 1, 1, :));
    plot(times, d)
    
end
legend(b_leg)




end

function inspect_data2(data)

n = 7;
i_n = n-1;
i_b = 1;
t = 9;

ampls = linspace(0.0, 0.05, 25);

for p = 0:19
    i_p = p + 1;
    d1 = squeeze(data(i_n, i_p, i_b, :, :, t));
    if any(d1(:,1) >0.999)
        figure
        plot(ampls, d1)
        title(['p = ' num2str(p)])
        
    end
end

end

function inspect_data3(data)

my_fittype = fittype(@(a, b, c, x) a*exp(b*x) + c);
n = 7;
i_n = n-1;
i_b = 2;
t = 7;

ampls = linspace(0.0, 0.05, 25);

for p = 0:19
    i_p = p + 1;
    d1 = squeeze(data(i_n, i_p, i_b, :, :, t));
    if any(d1(:,1) >0.999)
        figure
        plot(ampls, d1)
        title(['p = ' num2str(p)])
        hold on
        for c = 1:100
            p_fit = fit(ampls.', d1(c,:).', my_fittype, 'StartPoint', [1, 1/-0.002, 0]);
            plot(p_fit, '--')
        end
        
    end
end

end

function inspect_data4(data)

p = 0;
i_p = p + 1;
i_b = 3;
t = 7;

ampls = linspace(0.0, 0.05, 25);

for n = 2:7
    i_n = n - 1;
    d1 = squeeze(data(i_n, i_p, i_b, :, :, t));
    if any(d1(:,1) > 0.1)
        figure
        plot(ampls, d1)
        title(['n = ' num2str(n)])
    end
end

end

function inspect_data5(data)

n = 2;
p = 1;
i_p = p + 1;
i_n = n-1;
i_b = 1;
%t = 8;
for t = fliplr(1:9)
    ampls = linspace(0.0, 0.05, 25);
    d1 = squeeze(data(i_n, i_p, i_b, :, :, t));
    plot(ampls, d1)
    title(['p = ' num2str(p)])
    pause
end
end


function inspect_data6(data)

n = 7;
p = 1;
i_p = p + 1;
i_n = n-1;
i_b = 2;
times = 1:9;
d1 = squeeze(mean(data(i_n, i_p, i_b, :, :, :), 4));
plot(times, d1)

end


function inspect_data7(data)

n = 7;
p = 4;
i_p = p + 1;
i_n = n-1;
i_b = 3;
%t = 8;
fidelity_bins = linspace(0, 1, 21);
for t = fliplr(1:9)
    d1 = squeeze();
    histogram(d1, fidelity_bins)
    hold on 
    pause
end
end


function out = get_num_uniq_solutions(data)

% Find the  average number of unique solutions for each (n,p,t)
n_n = 6;
n_T = 9;
n_b = 3;

d_size = [n_T, n_n, n_b];

out = nan(d_size);

tot_iter = prod(d_size);
it = 0;
for i_n = 1:n_n
    
   for i_T = 1:n_T
       
       for i_b = 1:n_b
           it = it + 1;
           d1 = squeeze(data(i_n, :, i_b, :, :, i_T)); % (20, 100, 25)
           num_uniq_solutions = 0;
           if isnan(d1(1,1,1))
              continue 
           end
           
           n_p = 20;
           for i_p = 1:20
              d = squeeze(d1(i_p,:,:));
              if any(isnan(d))
                 n_p = n_p -1;
                 continue
              end
              C = uniquetol(d, 1e-5, 'ByRows', true);
              num_uniq_solutions = num_uniq_solutions + size(C,1);
           end
           out(i_T, i_n, i_b) = num_uniq_solutions/20;
           
           disp(['Progress: ' num2str(it/tot_iter*100)])
       end
   end
   
end

end


function inspect_data8(data)

num_sol = get_num_uniq_solutions(data);

times = 1:9;
bs = [0.5, 1.0, 3.0];
ns = 2:7;

[X,Y,Z] = meshgrid(ns, times, bs);

slice(X,Y,Z,num_sol/100,[],9,[0.5,1.0, 3.0])
colorbar
xlabel('Matrix order, n')
ylabel('Durations, T')
zlabel('Scale, b')

           
end

function out = get_avg_1step_drop(data)

% Find the  average number of unique solutions for each (n,p,t)
n_n = 6;
n_T = 9;
n_b = 3;

d_size = [n_T, n_n, n_b];

out = nan(d_size);

tot_iter = prod(d_size);
it = 0;
for i_n = 1:n_n
    
   for i_T = 1:n_T
       
       for i_b = 1:n_b
           it = it + 1;
           d1 = squeeze(data(i_n, :, i_b, :, :, i_T)); % (20, 100, 25)
           
           drop = (d1(:,:,2)-d1(:,:,1))./d1(:,:,1);
           avg_drop = mean(drop, 2, 'omitnan');
           p_avg_drop = mean(avg_drop,1, 'omitnan');
           out(i_T, i_n, i_b) = p_avg_drop;
           
           disp(['Progress: ' num2str(it/tot_iter*100)])
       end
   end
   
end

end

function inspect_data9(data)

avg_drop = get_avg_1step_drop(data);

times = 1:9;
bs = [0.5, 1.0, 3.0];
ns = 2:7;

[X,Y,Z] = meshgrid(ns, times, bs);

slice(X,Y,Z,avg_drop,[],9,[0.5,1.0, 3.0])
colorbar
xlabel('Matrix order, n')
ylabel('Durations, T')
zlabel('Scale, b')
           
end

