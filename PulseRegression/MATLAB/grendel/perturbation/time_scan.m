
%% 
% Allow time scans in various ways 

mode = 1;

data_meta = load_perturb_data('perturbation_scan.mat');
data = data_meta.data; % input size = (nn, np, nb, nseeds, nAmpls, nT)

switch mode
    
    case 1
        scan1(data)
    case 2
        scan2(data)
        
end


function scan1(data)

times = linspace(0, 1, 11);

figure

for i_n = 1:6
    n = i_n + 1;
    subplot(2,3,i_n)
    % get slice of data
    n_data = data(i_n, :, end-1, :, 1, :);
    
    % Take max across seeds
    n_data = max(n_data, [], 4);
    
    
    % Take mean across seeds
    %n_data = mean(n_data, 4);
    
    
    n_data = squeeze(n_data);
    
    
    hold on
    for i_p = 1:20
        plot(times, n_data(i_p, :))
    end
    
    title(['n = ' num2str(n) ])
    xlabel('Duration')
    ylabel('Fidelity')
end
end

function scan2(data)

times = linspace(0, 1, 11);
p = 1;

figure

for i_n = 1:6
    n = i_n + 1;
    subplot(2,3,i_n)
    % get slice of data
    n_data = data(i_n, p, end, :, 1, :);
    
    % Take max across seeds
    n_data = max(n_data, [], 4);
    
    
    % Take mean across seeds
    %n_data = mean(n_data, 4);
    
    
    n_data = squeeze(n_data);
    size(n_data)
    
    
    hold on
    for i_p = 1:20
        plot(times, n_data(i_p, :))
    end
    
    title(['n = ' num2str(n) ])
    xlabel('Duration')
    ylabel('Fidelity')
end
end
