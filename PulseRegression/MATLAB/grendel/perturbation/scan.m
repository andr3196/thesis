close all

%% In this file i load and plot the perturbation scan from grendel

data_meta = load_perturb_data('perturbation_scan2.mat');
data = data_meta.data; % input size = (nn, np, nb, nseeds, nAmpls, nT)
durations = data_meta.durations;

% file with approximate T_QSLs
savedata_filename_TQSL_approx = 'QSL_scan.mat';


% data from perturb_n_6_p_12_b_010_T_340.npz
%data(5, 13, 2, 1, :, 3) % from run n=6, p=12, bi=1, Ti=3

mode = 8;

switch mode
    
    case 1
        scan1(data);
    case 2
        scan2(data);
    case 3
        % What happens to the characteristic length as we increase n?
        scan3(data);
    case 4
        
        show_example_perturbs(data)
        
    case 5
        show_all_TQSL_perturbs(data)
        
    case 6
        
        show_above_and_below(data)
        
    case 7
        % Plot the 1 step drop as a function of the scaled QSL
        scan4(data, durations)
        
    case 8
        % Plot multiplicity as a function of T
        scan5(data, durations)
        
        % How does the characteristic length depend on duration?
        
        % How does the characteristic length depend on control scale?
        % How different is the characteristic length for different peaks?
        % plot a distribution N_peaks(F)
        % Is the characteristic length different of converging ctrls?
        % compare low seeds to high seeds
        % are all seeds the same below QSL
        
        
    otherwise
        disp('Doing nothing')
        
end



function scan1(data)

amplitudes = linspace(0.0, 0.5, 25);


%my_fittype = fittype(@(a, b, c, d, x) a/(b*(x-c).^2 + d));

for it = 1:size(data,6)
    
    figure; %('units','normalized','outerposition',[0 0 1 1])
    
    for i_n = 1:6
        n = i_n + 1;
        subplot(2,3,i_n)
        % get slice of data
        n_data = data(i_n, :, 2, :, :, it);
        
        % Take seed with best original fidelity
        [~, seed_max] = max(n_data, [], 4);
        n_data = n_data(:,:,:,seed_max(1), :,:);
        
        n_data = squeeze(n_data);
        %figure
        
        plot(amplitudes, n_data)
        
        %         hold on
        %         lengths = [];
        %         for i_p = 1:20
        %            if any(isnan(n_data(i_p,:)))
        %                n_data(i_p,:)
        %            else
        %                plot(amplitudes, n_data(i_p,:), '.')
        %
        %                %p_fit = fit(amplitudes.', n_data(i_p,:).', 'exp1');% , 'Startpoint', [1, -50, 0.12])
        %                %coeffs = coeffvalues(p_fit);
        %                %lengths(end+1) =  -1/coeffs(2);
        %                %p_fit = fit(amplitudes.', n_data(i_p,:).', my_fittype, 'Startpoint', [1, -1.5, 0.12])
        %                %p_fit = fit(amplitudes.', n_data(i_p,:).', 'rat02')
        %                %plot(p_fit)
        %                %pause
        %            end
        %            legend('off')
        %          end
        
        title(['n = ' num2str(n) ])
        xlabel('Perturbation amplitude')
        ylabel('Fidelity')
        axis([0 0.5 0 1])
    end
    
    title(['T ~ ' num2str(it)])
    %title('expfit')
    
    
end

%figure
%histogram(lengths)


end


function scan2(data)


b_values = [0.5 1.0 3.0];
times = linspace(0, 1, 9);

[A, B] = meshgrid(times, b_values);

for p = 1:20
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for i_n = 1:6
        n = i_n + 1;
        subplot(2,3,i_n)
        % get slice of data
        n_data = data(i_n, p, :, :, 1:2, :);
        
        % Take mean across seeds
        n_data = mean(n_data, 4);
        
        l_data = (n_data(1, 1, :, 1, 2, :) - n_data(1, 1, :, 1, 1, :))./n_data(1, 1, :, 1, 1, :);
        
        l_data = squeeze(l_data);
        size(A)
        size(l_data)
        
        surf(A, B, l_data)
        
        title(['n = ' num2str(n) ])
        ylabel('control scale, b')
        xlabel('Duration, T')
        zlabel('fidelity pct change')
        %axis([0 0.05 0 1])
    end
    
    %title(['p ~ ' num2str()])
    pause
    
end
end

function QSL_data = load_approximate_QSL()
data_savepath = get_collected_savepath();
QSL_data_struct = load(fullfile(data_savepath, 'QSL_scan.mat')); %(n_times, n_b, n_m)
QSL_data = QSL_data_struct.QSL_data; % has b values 0.5, 0.75, ... 3.0
QSL_data = squeeze(QSL_data(:,[1, 3, 11], 1));
QSL_data(isnan(QSL_data)) = 40.0;


end


function [T, T_QSL] = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T)

T_QSL = qsl_data(i_n, i_p, i_b); % actual QSL for this problem
T_QSL_approx = qsl_data_approx(i_n, i_b); % Estimated QSL for this problem
T = T_QSL_approx - 8.0 + 1.0*i_T;

if T >= T_QSL
    s=' ';
else
    s=' not ';
end

disp(['T is' s 'above the QSL'])

end

function data = load_calculated_QSL()
qsl_meta = load('perturbation_qsls.mat');
data = qsl_meta.d;
end



function show_example_perturbs(data)

qsl_data = load_calculated_QSL();
qsl_data_approx = load_approximate_QSL();
ampls = linspace(0, 0.05, 25);
n = 7;

p = 2;
i_p = p + 1;
i_n = n-1;
i_b = 3;
i_T = 9;
[T, T_QSL]  = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T)


d1 = squeeze(data(i_n, i_p, i_b, :, :, i_T));
plot(ampls, d1)
xlabel('Perturbation amplitude, A', 'Fontsize', 15)
ylabel('Average Fidelity','Fontsize', 15)
print('perturb_example_n_7_p_2_T_205.pdf', '-dpdf')

end


function show_all_TQSL_perturbs(data)

qsl_data = load_calculated_QSL();
qsl_data_approx = load_approximate_QSL();
ampls = linspace(0, 0.05, 25);
n = 7;

for p = 0:19
    %p = 2;
    i_p = p + 1;
    i_n = n-1;
    i_b = 2;
    %i_T = 7;
    p
    for i_T = fliplr(1:9)
        [T, T_QSL]  = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T);
        %         if T <= T_QSL +0.1 && T >= T_QSL -0.1
        %             d1 = squeeze(data(i_n, i_p, i_b, :, :, i_T));
        %             plot_example(ampls, d1, p, T, T_QSL)
        %             break
        %         end
    end
    
end

end


function show_above_and_below(data)

qsl_data = load_calculated_QSL();
qsl_data_approx = load_approximate_QSL();
ampls = linspace(0, 0.05, 25);
n = 7;

p = 0;
i_p = p + 1;
i_n = n-1;
i_b = 2;
i_T_list = 1:9;
for i_T = i_T_list
    [T, T_QSL]  = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T);
    d1 = squeeze(data(i_n, i_p, i_b, :, :, i_T));
    plot_example(ampls, d1, p, T, T_QSL)
    
end


end

function plot_example(ampls, d1, p, T, TQSL)
figure('Position', [10 10 600 200])
plot(ampls, d1)
title(['p=' num2str(p) ' T=' num2str(T) ' TQSL=' num2str(TQSL)])
xlabel('Perturbation amplitude, A', 'Fontsize', 15)
ylabel('Average Fidelity','Fontsize', 15)
end


function out = get_avg_1step_drop(data)

out = (data(:, :, :, :, 2, :) - data(:,:, :, :, 1, :))./data(:,:, :, :, 1, :);
%out = data(:, :, :, :, 2, :) - data(:,:, :, :, 1, :);
out = squeeze(mean(out, 4, 'omitnan'));
end

function out = get_avg_laststep_drop(data)

%out = (data(:, :, :, :, 2, :) - data(:,:, :, :, 1, :))./data(:,:, :, :, 1, :);
%out = data(:, :, :, :, end, :) - data(:,:, :, :, 1, :);
out = squeeze(mean(out, 4, 'omitnan'));
end

function scan4(data, durations)

i_b = 2;
drops = get_avg_1step_drop(data); % (n,p,b,T)
qsl_data = load_calculated_QSL();
qsl_data_approx = load_approximate_QSL();
hold on
colors = 'brgkmc';
obs = [];
labels = cell(1,6);
for i_n = 1:6
    c = colors(i_n);
    
    for i_p = 1:20
        
        for i_T = 1:9
            [~, T_QSL]  = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T);
            T = durations(i_n, i_p, i_b, i_T);
            p = plot(T/T_QSL, drops(i_n, i_p, i_b, i_T), [c '.']);
        end
    end
    
    obs = [obs p];
    labels{i_n} = ['n=' num2str(i_n+1)];
    
end

xlim([0 1.8])
xlabel('T/T_{QSL}', 'Fontsize', 15)
ylabel('1-step relative drop', 'Fontsize', 15)
legend(obs, labels, 'Location', 'southeast', 'Fontsize', 15)

print('dropvsT.pdf', '-dpdf')

end

function out = get_num_uniq_solutions(data)

% Find the  average number of unique solutions for each (n,b,t)
n_n = 6;
n_T = 9;
n_b = 3;
n_p = 20;

d_size = [n_n, n_p, n_b, n_T];

out = nan(d_size);

tot_iter = prod(d_size);
it = 0;
for i_n = 1:n_n
    
    for i_p = 1:n_p
        
        for i_T = 1:n_T
            
            
            for i_b = 1:n_b
                it = it + 1;
                d1 = squeeze(data(i_n, i_p, i_b, :, :, i_T)); % (20, 100, 25)
                if all(isnan(d1))
                    continue
                end
                %C = uniquetol(d1, 1e-5, 'ByRows', true);
                C = uniquetol(d1(:,1), 1e-5);
                out(i_n, i_p, i_b, i_T) = size(C, 1);
                disp(['Progress: ' num2str(it/tot_iter*100)])
            end
        end
        
    end
    
end

end


function scan5(data, durations)

i_b = 2;
multiplicity = get_num_uniq_solutions(data); % (n,p,b,T)
qsl_data = load_calculated_QSL();
qsl_data_approx = load_approximate_QSL();
hold on
colors = 'brgkmc';
obs = [];
labels = cell(1,6);
for i_n = 1:6
    c = colors(i_n);
    
    for i_p = 1:20
        
        for i_T = 1:9
            [~, T_QSL]  = get_duration(qsl_data,qsl_data_approx, i_n, i_p, i_b, i_T);
            T = durations(i_n, i_p, i_b, i_T);
            p = plot(T/T_QSL, multiplicity(i_n, i_p, i_b, i_T)/100, [c '.']);
        end
    end
    
    obs = [obs p];
    labels{i_n} = ['n=' num2str(i_n+1)];
    
end

xlim([0 1.8])
xlabel('T/T_{QSL}', 'Fontsize', 15)
ylabel('Fraction of unique optima', 'Fontsize', 15)
legend(obs, labels, 'Location', 'southeast', 'Fontsize', 15)

print('numsolutionsvsT.pdf', '-dpdf')

end

