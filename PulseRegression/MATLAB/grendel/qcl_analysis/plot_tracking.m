close all
% File IO
p = 0;
mode = 'stretch';

data = load(['tracking_n_2_p_' num2str(p) '_b_3.0_' mode '.mat']);
ctrls = data.ctrls;
fids = data.fids;

s_fids = (fids-min(fids))/(max(fids)-min(fids));

%ctrls(isnan(ctrls)) = 0;
ctrls_old = ctrls;
s_ctrls = stretch(ctrls);
s_ctrls(isnan(s_ctrls)) = 0;
ctrls_old(isnan(ctrls_old)) = 0;
d_ctrl = abs(ctrls_old(2:end,:)-s_ctrls(1:end-1, :));
d = sum(d_ctrl,2);


% Define
dt = 0.1;

n_durs = size(ctrls, 1);
n_times_max = size(ctrls,2);
durs = linspace(0.2, 1.5, n_durs);
times = dt*(0:n_times_max-1);


cmap = colormap('jet');
hold on
for i = 1:size(ctrls, 1)
    
    %if durs(i) < 0.95 || durs(i) > 1.05
    %    continue
    %end
    
    plot3(times, durs(i)*ones(1,n_times_max), ctrls(i,:), 'Color', get_fidelity_color(cmap, s_fids(i)));
    
end

grid on
xlabel('Time, t', 'FontSize', 15)
ylabel('T/T_QSL', 'FontSize', 15)
zlabel('Control amplitude, c', 'FontSize', 15)
c = colorbar;
caxis([min(fids) max(fids)])
c.Label.String = 'Fidelity';
c.Label.FontSize = 15;
colormap('jet')
view([32 32])
print('control_extension_n2p0b3_stretch', '-dpdf')


function c = get_fidelity_color(cmap, F)
    n_colors = size(cmap, 1);
    i = round(F*(n_colors-1)) + 1;
    c = cmap(i,:);

end


function ctrls = stretch(ctrls)

n_ts = size(ctrls, 2);
n_c = size(ctrls, 1);
x = 1:n_ts;

for i = 1:n_c-1
    c = ctrls(i, :);
    l = sum(~isnan(c));
    x_l = x(1:l);
    x_new = linspace(x_l(1), x_l(end), l+1);
    c_new = interp1(x_l, c(1:l), x_new);
    ctrls(i,1:l+1) = c_new;
end

end