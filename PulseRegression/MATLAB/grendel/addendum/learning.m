%% In this scenaio we calculate the typical (mean) Euclidian distance from a seed to the final optimized control.
close all

% Load data
%data_relative_path = 'data/distance/collected/run1/data/';
%data_filename = 'collected_curvatures.mat';
%path_to_project_root = '../../../';

%full_path = fullfile(path_to_project_root, data_relative_path, data_filename);
%data_meta = load(full_path);
%curvs = data_meta.curvs;  % n_n=10, n_p=20, n_b=3, n_T=14, n_seeds=500, n_metrics=5




mode = 2;

switch mode
    
    case 1
        % Calculate mean MAEs for the new data set
        part1()
    case 2
        % Calculate mean MAEs for the merged data set
        part2()
    case 3
        part3()
        % Plot GPRQ scan of n=2,..,7
        
        
end



function part1()
data_relative_path = 'data/distance/collected/run1/fit/';
data_filename = 'mae_data_n_11_b_3.0_optseed.mat';
path_to_project_root = '../../../';

full_path = fullfile(path_to_project_root, data_relative_path, data_filename);
data_meta = load(full_path);
maes = data_meta.maes;  % n_p = 10, n_metrics: 6 (1-3: Opt, 4-6: seeds)
maes
maes_means = mean(maes, 1, 'omitnan')
maes_std = std(maes, [], 1, 'omitnan')
end

function part2()
data_relative_path = 'data/learning/merged_data';
data_filename = 'mae_data_n_7_b_3.0.mat';
path_to_project_root = '../../../';

full_path = fullfile(path_to_project_root, data_relative_path, data_filename);
data_meta = load(full_path);
maes = data_meta.maes;  % n_p = 10, n_metrics: 3
maes = maes(1:9, :);
maes_means = mean(maes, 1, 'omitnan')
maes_std = std(maes, [], 1, 'omitnan')
end


function part3()
data_relative_path = 'GPRQ_scan';

parts = {'seeds', 'ctrls'};
styles = {'r-', 'b--'};

i_offset = 180;

maes = zeros(9,6);
n_values = 2:7;

ps = [0:7 9];
for k = 1:2
    part = parts{k};
    style = styles{k};
    for i = 1:9
        p = ps(i);
        filename = ['log_' num2str(i_offset + p)];
        filepath = fullfile(data_relative_path,part, filename);
        fid = fopen(filepath);
        res_cell = textscan(fid, '%f%f%f');
        mae_vals = res_cell{1};
        v = nan(1,6);
        v(1:length(mae_vals)) = mae_vals;
        
        maes(i, :) = v;
    end
   
    maes_means = mean(maes, 1, 'omitnan');
    plot(n_values, maes_means, style)
    hold on
end
legend({'seeds', 'optimized'})
ylabel('MAE (test set)')
xlabel('Problem size, n')
%print('GPRQ_n_scan.png', '-dpng')
end


