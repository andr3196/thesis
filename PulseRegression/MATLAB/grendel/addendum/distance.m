%% In this scenaio we calculate the typical (mean) Euclidian distance from a seed to the final optimized control.
close all

% Load data
data_relative_path = 'data/distance/collected/run1/data/';
data_filename = 'collected_distances.mat';
path_to_project_root = '../../../';

full_path = fullfile(path_to_project_root, data_relative_path, data_filename);
data_meta = load(full_path);
dists = data_meta.dists;  % n_n=10, n_p=20, n_b=3, n_T=14, n_seeds=500, n_metrics = 2

% 1 = Euclidian, 2 = RMS
metric = 1;
dists = squeeze(dists(:,:,:,:,:,metric));


mode = 7;

switch mode
    
    case 1
        % plot histogram of distances of a single example
        part1(dists)
    case 2
        % Repeat 1 for all T in the same figure
        part2(dists)
        
    case 3
        % Create surf of mean(D) across seeds for all (n, T) for a single p
        part3(dists)
    case 4
        % Scatter plot the distribution of distances as a function of
        % T/T_QSL
        part4(dists)
    case 5
        % Study the change in distribution around the mean as a function of
        % duration
        part5(dists)
    case 6
        % Plot histogram for each p
        part6(dists)
    case 7
        %study distances for single problem
        part7(dists)
       
end


% Figures
% 1) Mean across seed, p for various n, T, b
% 2) Seed distribution (variance) with duration
% 3) Seed distribution for various p



function part1(dists)

n_i = 4;
p_i = 4;
b_i = 1;
T_i = 14;

row_dists  = dists(n_i, p_i, b_i, T_i, :);
[n, edg] = histcounts(row_dists);
bin_c = edg(1:end-1) + 0.5 * diff(edg);
f = fit(bin_c.', n.', 'gauss1');
histogram(row_dists)


hold on
plot(f)



end

function part2(dists)

n_i = 10;
p_i = 1;
b_i = 3;

hold on
for T_i = 1:14
    row_dists  = dists(n_i, p_i, b_i, T_i, :);
    histogram(row_dists)
end
legend
end

function part3(dists)

%b_i = 3;


durations = 0.2:0.1:1.5;
n_values = 2:11;

[X, Y] = meshgrid(durations, n_values);

for b_i = 1:3
    subplot(3,1, b_i)
    
    Z = mean(dists(:,:,b_i,:,:), 5, 'omitnan');
    Z = squeeze(mean(Z, 2, 'omitnan'));
    
    
    surf(X, Y, Z)
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
    zlabel('$\overline{||\bf{c}^* - \bf{c}_{init}||}$', 'Interpreter', 'Latex', 'FontSize', 15)
    view([12 11])
    
end

%print('MeanDistancebAll', '-dpdf', '-fillpage')

end

function part4(dists)
ni = 7;
pi = 4;
bi = 2;
durations = 0.2:0.1:1.5;
d = squeeze(dists(ni, pi, bi, :, :));

plot(durations, d, '.')



end



function part5(dists)
%bi = 3;
durations = 0.2:0.1:1.5;

for bi = 1:3
    subplot(3, 1, bi)
    d_std = std(dists(:, :, bi, :, :),[], 5, 'omitnan');
    d_std_mean = squeeze(mean(d_std, 2, 'omitnan'));
    hold on
    colors = distinguishable_colors(10);
    for n_i = 1:10
        plot(durations, d_std_mean(n_i, :), '-', 'Color', colors(n_i, :))
    end
    for n = 2:11
        n_labels{n-1} = ['n=' num2str(n)];
    end
    if bi == 1
        legend(n_labels, 'Location', 'EastOutside', 'FontSize', 15)
    end
    gca.FontSize = 15;
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('$\overline{\rm{std}(||\bf{c}^* - \bf{c}_{init}||)}$', 'Interpreter', 'Latex', 'FontSize', 15)
end

print('MeanDistanceStdbAll', '-dpdf', '-fillpage')


end



function part6(dists)

n_i = 4;
b_i = 1;
T_i = 8;

hold on

edges = 0:2:36;

for p = 1:20
    row_dists  = dists(n_i, p, b_i, T_i, :);
    histogram(row_dists, edges)
end

end


function part7(dists)


durations = 0.2:0.1:1.5;


n_i = 4;
p_i = 6;
b_i = 1;

d = dists(n_i,p_i,b_i,:,:);
%d = squeeze(mean(d, 5, 'omitnan'));
d = squeeze(d);
plot(durations, d)
%xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
%ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
%zlabel('$\overline{||\bf{c}^* - \bf{c}_{init}||}$', 'Interpreter', 'Latex', 'FontSize', 15)
%view([12 11])


%print('MeanDistancebAll', '-dpdf', '-fillpage')

end






