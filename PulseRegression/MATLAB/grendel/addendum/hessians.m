%% In this scenaio we calculate the typical (mean) Euclidian distance from a seed to the final optimized control.
%close all

% Load data
data_relative_path = 'data/distance/collected/run1/data/';
data_filename = 'collected_curvatures.mat';
path_to_project_root = '../../../';

full_path = fullfile(path_to_project_root, data_relative_path, data_filename);
data_meta = load(full_path);
curvs = data_meta.curvs;  % n_n=10, n_p=20, n_b=3, n_T=14, n_seeds=500, n_metrics=5

% Metrics:
% Gaussian curvature 
% Mean curvature 
% Unflatness
% Maximum eigenvalue
% Minimum eigenvalue


figure


mode = 5;

switch mode
    
    case 1
        % plot Gaussian curvature example
        part1(curvs)
    case 2
        % plot Mean curvature example
        part2(curvs)
    case 3
        % Plot the mean of the mean curvature
        part3(curvs)
    case 4
        part4(curvs)
        % Surface plot of fraction of Gaussian curvatures
    case 5
        part5(curvs)
        % Surface plot of log mean curvature
    case 6
        % Surface plot of unflatness
        part6(curvs)
        
       
end



function part1(curvs)

n_i = 10;
p_i = 4;
b_i = 3;
T_i = 14;

row_curvs  = squeeze(curvs(n_i, p_i, b_i, T_i, :, 1));
row_curvs = row_curvs(abs(row_curvs) > 1 & abs(row_curvs) < 1e10)


%[n, edg] = histcounts(row_curvs);
%bin_c = edg(1:end-1) + 0.5 * diff(edg);
%f = fit(bin_c.', n.', 'gauss1');
edges = -10:0.1:10;
histogram(row_curvs, edges)


%hold on
%plot(f)



end

function part2(curvs)

n_i = 1;
p_i = 4;
b_i = 3;
T_i = 10;

row_curvs  = squeeze(curvs(n_i, p_i, b_i, T_i, :, 2));
%[n, edg] = histcounts(row_curvs);
%bin_c = edg(1:end-1) + 0.5 * diff(edg);
%f = fit(bin_c.', n.', 'gauss1');
edges = -10:0.1:10;
is_out = isoutlier(row_curvs, 'median', 'ThresholdFactor', 3);
n_out = sum(sum(is_out));
disp(['Removed ' num2str(n_out) ' outliers'])
row_curvs(is_out) = NaN;
histogram(row_curvs) %, edges)


%hold on
%plot(f)



end

function part3(curvs)

%b_i = 3;


durations = 0.2:0.1:1.5;
n_values = 2:11;

[X, Y] = meshgrid(durations, n_values);

for b_i = 1:3
    subplot(3,1, b_i)
    
    Z = mean(curvs(:,:,b_i,:,:, 2), 5, 'omitnan');
    Z = squeeze(mean(Z, 2, 'omitnan'));
    
    histogram(Z)
 
    Z(abs(Z) > 10e8) = NaN;
    
    surf(X, Y, Z)
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
    zlabel('mean of mean curvature', 'Interpreter', 'Latex', 'FontSize', 15)
    view([12 11]) 
end

%print('MeanDistancebAll', '-dpdf', '-fillpage')

end

function part4(curvs)
% We plot a surface of fraction of vanishing curvatures


durations = 0.2:0.1:1.5;
n_values = 2:11;
n_seeds = 500;
[X, Y] = meshgrid(durations, n_values);

for b_i = 1:3
    subplot(3,1, b_i)
    
    
    d = squeeze(curvs(:,:,b_i, :,:,1));
    
    % It seems that curvatures are either infinite or vanishing
    vanish_th = 1;
    is_vanishing = abs(d) < vanish_th;
    Z = squeeze(mean(sum(is_vanishing, 4)/n_seeds, 2));
    
    
    surf(X, Y, Z)
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
    zlabel('flat', 'Interpreter', 'Latex', 'FontSize', 15)
    %view([12 11])
    
end

%print('MeanDistancebAll', '-dpdf', '-fillpage')

end

function part5(curvs)

x = [0.2 1.5];
y = [2 11];
for b_i = 1:3
    subplot(3,1, b_i)
    
    d = squeeze(curvs(:, :, b_i, :, :, 2));
    %d(isoutlier(d, 'median', 4)) = NaN;
    d = d + min(d(:)) + 1;
    d = log10(d);
    Z = squeeze(mean(mean(d, 4, 'omitnan'), 2));
    imagesc(x,y, Z)
    colorbar
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
end

%print('MeanDistancebAll', '-dpdf', '-fillpage')

end


function part6(curvs)
%  Same as 5, but for flatness


durations = 0.2:0.1:1.5;
n_values = 2:11;
n_seeds = 500;
[X, Y] = meshgrid(durations, n_values);

x = [0.2 1.5];
y = [2 11];

for b_i = 1:3
    subplot(3,1, b_i)
    
    d = squeeze(curvs(:, :, b_i, :, :, 3));
    %d(isoutlier(d, 'median', 4)) = NaN;
    d = log10(d);
    d_seed_mean = mean(d, 4, 'omitnan');
    Z = squeeze(mean(d_seed_mean, 2, 'omitnan'));
    imagesc(x,y, Z)
    colorbar
    %surf(X, Y, Z)
    xlabel('$T/T_{QSL}$', 'Interpreter', 'Latex', 'FontSize', 15)
    ylabel('n', 'Interpreter', 'Latex', 'FontSize', 15)
    %zlabel('flat', 'Interpreter', 'Latex', 'FontSize', 15)
    %view([12 11])
    
end

%print('MeanDistancebAll', '-dpdf', '-fillpage')

end


