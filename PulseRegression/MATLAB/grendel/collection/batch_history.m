close all

%% In this file we want to show the history of GRAPE optimizing the seeds
base_path = get_data_basepath('singlequbit/analysis/fitting/collected/mat');

data_filename = "collection_metadata.mat";
full_file = fullfile(base_path, data_filename);
meta_data = load(full_file);  % size: (n_n, n_p, n_col_max)

mode = 1;

switch mode
    
    case 1
        % Draw 3d plot of optimization history
        make_figure(meta_data)
    case 2
        % Find best model for guessing number of GRAPE iterations
        guess_nit_func(meta_data)
    case 3
        % Find best model for guessing number of GRAPE iterations
        guess_nit_func_new()
        
        
end


function make_figure(meta_data)

n_values = 2:7;
nit = meta_data.n_it;
fmu = meta_data.F_mu;

hold on
for n_i = 1:6
    n = n_values(n_i);
    y1 = squeeze(nit(n_i, :, :));
    z1 = squeeze(fmu(n_i, :, :));
    x1 = n*ones(size(y1));
    
    filt = y1 ~= 200;
    y1 = y1(filt);
    z1 = z1(filt);
    x1 = x1(filt);
    
    
    
    plot3(x1, y1, 1-z1, '.')
    
end
xlabel('Matrix order, n')
ylabel('Num GRAPE it')
zlabel('Mean Infidelity, F_\mu')
ylim([0 100])

%q = linspace(0, 50, 100);
%f = exp(-q/2);
%f2 = exp(-q/12*log(2));
%plot3(ones(size(q)), q, f)
%plot3(ones(size(q)), q, f2)
view([60 27])
end


function data = iteration_histories()
data = struct;
% n=7, p=0, b=1.0
d1.nit = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60];
d1.infids = 1 - [0.09484778366321218, 0.14918606837492687, 0.43478409923941985, 0.5758844354487469, 0.7183880145058019, 0.8465963223704753, 0.8962574867833291, 0.9214474731095853, 0.9490870359136212, 0.9716253968732453, 0.9866887102516764, 0.9923442872325622, 0.9954553483531142, 0.9969813707967221, 0.9983845582999206, 0.9994874561832158];

% n=3, p=0, b=1.0
d2.nit = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 80, 100, 120, 140, 160, 180, 200, 220];
d2.infids = 1 - [0.25377765940353897, 0.3855373566446436, 0.7188899496925603, 0.910091770937621, 0.9468065539061796, 0.9518377802707565, 0.9542310701694876, 0.9559044878950697, 0.9574550176818798, 0.9585422141934913, 0.9592807152448442, 0.9612159509364254, 0.9620604532922794, 0.9624744151775563, 0.9627030311858963, 0.9628209869361314, 0.9629148981967387, 0.9629369009031852, 0.9629439954086585, 0.9629478866507892, 0.9629501389426247, 0.9629509284171002, 0.9629512218174505, 0.962951311083482];

% n=6, p=0, b=1.0
d3.nit = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 80, 100, 120, 140, 160, 180, 200, 220];
d3.infids = 1 - [0.050120165043414454, 0.05108665856123947, 0.0787256024763013, 0.14407314114671269, 0.2139623151427547, 0.36101491988701173, 0.3859575768527659, 0.4369604568592695, 0.4456690559422652, 0.447220273492527, 0.4477314889514603, 0.44784755355938366, 0.4478514268451958, 0.44785157755465027, 0.44785158232917405, 0.4478515823476869, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684, 0.44785158234769684];


data.d1 = d1;
data.d2 = d2;
data.d3 = d3;
end


function guess_nit_func(meta_data)
n=4;
p = 4;
pi = p + 1;
ni = n - 1;
all_nit = meta_data.n_it;
all_fmu = meta_data.F_mu;

nit = squeeze(all_nit(ni, pi, :));
fmu = squeeze(all_fmu(ni, pi, :));

nit = nit(~isnan(nit));
fmu = 1 - fmu(~isnan(fmu));

nit(end) = [];
fmu(end) = [];

plot(nit, fmu, '.')
hold on

%my_func = fittype('gauss1');
my_func = fittype( @(a, b, c, x) b./(a*x.^2 + b^2) + c );
%my_func = fittype( @(b, c, x) b*exp(-c*x) );

f = fit(nit, fmu, my_func, 'StartPoint', [1, 1, 0]);
plot(f, 'r-')


end

function [func, startpoint] = define_fit(type)

switch type
    
    case 'sigmoid'
        func = fittype( @(a, b, c, d, x) a./(1 + b*exp(-c*x)) + d );
        startpoint = [-1, 1, 1, 1];
    
    case 'doubleexp'
        func = fittype('exp2');
        startpoint = [];
    case 'doublegaussian'
        func = fittype('gauss2');
        %startpoint = [1, 1, 1, 1, 1, 1];
        startpoint = [];
    case 'gaussian'
        func = fittype(@(a1, c1, d1, x) a1*exp(-(x./c1).^2) + d1);
        startpoint = [0.75, 2, 0];
    case 'exponential'
        func = fittype( @(a, b, c, x) a*exp(-b*x) + c);
        startpoint = [1, 1, 0];
    case 'lorentzian'
        func = fittype( @(a, b, c, d, x) c./((x-a).^2 + b^2) + d );
        startpoint = [-1, 3, 1, 0.03];
    case 'lorentzgauss'
        func = fittype( @(a, b, c, d, x) a*exp(-x.^2/c^2) + b^2./(x.^2 + d^2) );
        startpoint = [1, 1, 1, 1];
     
    
end



end

function guess_nit_func_new()

data = iteration_histories();
d = data.d2;
nit = d.nit;
infids = d.infids;

%nit = nit(1:end-5);
%infids = infids(1:end-5);


plot(nit, infids, '.')
hold on

[my_func, startpoint] = define_fit('sigmoid');

if startpoint
f = fit(nit.', infids.', my_func, 'StartPoint', startpoint)
else
f = fit(nit.', infids.', my_func) 
end
plot(f, 'r-')


end