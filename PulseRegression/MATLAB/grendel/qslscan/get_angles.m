function angles = get_angles(Hds, Hcs)
% data size (dim1, dim1, p)

vd = to_vectors(Hds);
vc = to_vectors(Hcs);

angles = zeros(1, size(vd, 3));

for i =1:size(vd,2)
    angles(i) = abs(dot(vd(:,i),vc(:,i))/(norm(vd(:,i))*norm(vc(:,i))));
end

end


function vec = to_vectors(A)
    s = size(A);
    vec_len = s(1)*s(2);
    vec = reshape(A, [vec_len, s(3:end)]);
    
end





