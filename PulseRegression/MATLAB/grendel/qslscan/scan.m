close all

%% We load the QSL-scan mat-file and plot the QSL for various problems
% as a function of n and p


% Get data
base_path = get_data_basepath('singlequbit/generation/qslscan/');
project_section = 'collected/mat';
load_filename = 'qslscan_data.mat';
full_path = fullfile(base_path, project_section, load_filename);
data_meta = load(full_path);
data = data_meta.data;


%% Define
n_values = 2:11;
b_values_str = {"b = 0.5", "b = 1.0", "b = 3.0"};


%% Select
mode = 1;
switch mode
    case 1
        plot1(n_values, data, b_values_str)
    case 2
        plot2(n_values, data, b_values_str)
    case 3
        plot3(n_values, data, b_values_str)
    case 4
        % Get QSLs for perturbation problems
        save1(n_values, data)
end

%% Plot T_QSL(n)
function plot1(n_values, data, b_values)


% figure 
figure('Position', [440   378   560   350])
%f = figure;
%f.Position
% Remove outliers
data(data > 100) = NaN;

% Find the general tendency, thus take mean across various problems
mean_data = mean(data, 2, 'omitnan');


styles = {'b.', 'r.', 'g.'};
for bi = 1:3
    tqsls = data(:,:, bi);
    p_i = plot(n_values + 0.1*(bi-2)*ones(size(n_values)), tqsls, 'MarkerSize', 8, 'Marker', '.', 'LineStyle', 'none', 'Color', styles{bi}(1));
    ps(bi) = p_i(1);
    if bi == 1
        hold on
    end
    mu = mean(tqsls, 2, 'omitnan');
    f = fit(n_values.', mu, 'poly1')
    f_vals = f(n_values.');
    plot(n_values, f_vals, styles{bi}(1))
end

legend(ps, b_values, 'Location', 'northwest', 'FontSize', 15);
xlabel("Dimension, n", 'FontSize', 15, 'Interpreter', 'Latex')
ylabel("$T_{min}$", 'FontSize', 18, 'Interpreter', 'Latex')
axis([1.8 11.2 0 90])

print_to_images('QSL_scan.pdf')

%print('QSL_scan.pdf', '-dpdf')

end

%% Plot std(TQSL) as function of b 
function plot2(n_values, data, b_values)

% Remove outliers
data(data > 100) = NaN;

% Find the general tendency, thus take mean across various problems
std_data = std(data, 0,  2, 'omitnan');


styles = {'b.', 'r.', 'g.'};
for bi = 1:3
    tqsls = std_data(:,:, bi);
    p_i = plot(n_values + 0.1*(bi-2)*ones(size(n_values)), tqsls, 'MarkerSize', 8, 'Marker', '.', 'LineStyle', 'none', 'Color', styles{bi}(1));
    ps(bi) = p_i(1);
    if bi == 1
        hold on
    end
end

legend(ps, b_values, 'Location', 'northwest', 'FontSize', 15);
xlabel("Matrix order, n", 'FontSize', 15)
ylabel("STD_{QSL}", 'FontSize', 15)
%#axis([1.8 11.2 0 90])


%print('QSL_scan.pdf', '-dpdf')

end




function plot3(n_values, data, b_values)
% define 
bi = 1;
ni = 1;

% Data dimensions (dim1, dim2, n, p, b, metric)
% dim1: rows of problem matrices
% dim2: cols of problem matrices
% metric: Hd, Hc, UTarg


% Load Hamiltonians and targets
data_meta = load('../../../data/qslscan/collected/run1/problems.mat');
problems = data_meta.problems;  % (11, 11, 10, 40, 3, 3)
Hds = squeeze(problems(1:ni+1,1:ni+1,ni,:, bi, 1));
Hcs = squeeze(problems(1:ni+1,1:ni+1,ni,:, bi, 2));

angles = get_angles(Hds, Hcs);

% Remove outliers
data(data > 100) = NaN;


tqsls = data(ni, :, bi);

%mu = mean(tqsls, 2, 'omitnan');
%f = fit(n_values.', mu, 'poly1');
%f_vals = f(n_values.');
norm_tqsls = tqsls; % - f_vals;
size(norm_tqsls)
plot(angles, tqsls, '.')




end



function save1(n_values, data)

size(data)
d = data(1:6,1:20,:);
save('../perturbation/perturbation_qsls.mat', 'd')

% save (6,20, 3)

end

