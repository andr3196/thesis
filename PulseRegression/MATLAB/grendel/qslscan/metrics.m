function norms = get_frobenius_norms(arr, n_values)
    arr_size = size(arr);
    norms = zeros(arr_size(3:4));
    for i = 1:arr_size(3)
        n = n_values(i);
        for j = 1:arr_size(4)
            norms(i,j) = norm(arr(1:n, 1:n, i, j), 'fro')/n;
        end
    end


end
