
% Define
n = 4;

% Setup 
Hc = rand_hermit(n);
U0 = rand_unit(n);
x0 = to_vector(U0);

% Search
options = optimset('MaxIter', 5000, 'MaxFunEvals', 10000);
xopt = fminsearch(@(x) f(Hc, x), x0, options);
Vopt = reshape(xopt, [n, n]);


% Result
disp('Commutator: ')
Vopt*Hc - Hc*Vopt

disp('Unitarity')
Vopt'*Vopt


function v = to_vector(M)

v = reshape(M, [numel(M), 1]);

end

function M = to_sqmatrix(v)

n2 = numel(v);
n = sqrt(n2);

M = reshape(v, [n,n]);

end

function U = rand_unit(n)

U = expm(1i*rand_hermit(n));

end


function H = rand_hermit(n)
A = rand(n) + 1i*rand(n);
H = 0.5*(A + A');

end


function  r = f(Hc, x)

V = to_sqmatrix(x);

R = V'*Hc*V - Hc;

r = norm(R,'fro');


end