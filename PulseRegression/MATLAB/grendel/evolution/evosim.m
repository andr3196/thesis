close all
L = 2;
% Define 
sx = [0 1; 1 0];
sz = [1 0; 0 -1];


% Drift hamiltonian
g = 1.0;
Hd1 = kron(eye(2),g*sz);
Hd2 = kron(g*sz, eye(2));
Hd = Hd1 + Hd2;

% Control hamiltonian
J = 1.0;
Hc1 = kron(eye(2),g*sx);
Hc2 = kron(g*sx, eye(2));
Hc = Hc1 + Hc2;


% Evolution 
n = 2^L;
U = eye(n);
dt = 0.1;
T_final = 10;
times = 0:dt:T_final;

res = zeros(n, n, length(times));

for i = 1:length(times)
   
    res(:,:,i) = U;
    H = Hd + Hc;
    U = expm(-1i*H*dt)*U;
    
end

subplot(1,2,1)
plt1 = heatmap(real(res(:,:,1)));
subplot(1,2,2)
plt2 = heatmap(imag(res(:,:,1)));

colorbar
pause

for i = 2:length(times)
   set(plt1, 'ColorData', real(res(:,:,i)))
   set(plt2, 'ColorData', imag(res(:,:,i)))
   pause
end 

