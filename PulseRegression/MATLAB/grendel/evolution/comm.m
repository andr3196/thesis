function r = comm(A,B)

    r = A*B - B*A;