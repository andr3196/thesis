function path = get_data_basepath(project)

if ~exist('project', 'var')
    project = 'singlequbit/generation/single_scan/';
end

backtrack_path = repmat('../',1,count(cd, '/'));

base_data_path = 'Volumes/Transcend/Thesis/pulseregression/';

path = strcat(backtrack_path, base_data_path, project);