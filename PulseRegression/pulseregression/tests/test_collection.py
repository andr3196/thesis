"""
Author: Andreas Springborg
Date: 03/12/2018
Goal:

"""
import unittest
from pathlib import Path

from qutip import Qobj
import numpy as np
from scipy.stats import truncnorm

from pulseregression.framework.utility.control import TimeSpec
from pulseregression.legacy.qutip_mod import GRAPEControlGenerator
from pulseregression.scenarios.collection import DataCollector
from pulseregression.scenarios.collection.uniform.main1.core import CollectionConfiguration, \
    DynamicTerminationConditions


class TestCollection(unittest.TestCase):

    def setUp(self):
        self.c1 = CollectionConfiguration(n=2,
                                          p=1,
                                          b=3.0,
                                          batch_size=10,
                                          dt=0.1,
                                          bin_max=3,
                                          t_before=1.0,
                                          t_after=1.0,
                                          )
        self.c2 = CollectionConfiguration(n=3,
                                          p=4,
                                          b=3.0,
                                          batch_size=10,
                                          dt=0.1,
                                          bin_max=3,
                                          t_before=1.0,
                                          t_after=1.0,
                                          min_progress=1
                                          )
        self.collector = DataCollector(self.c1, 7)
        self.collector2 = DataCollector(self.c2, 7)
        self.dyn_tc = DynamicTerminationConditions(self.c2)

        self.time_spec = TimeSpec(n_steps=21, step_size=0.1)
        self.generator = GRAPEControlGenerator(self.c2, self.time_spec, self.dyn_tc)



    def test_config_can_yield_problem(self):
        self.assertTrue(isinstance(self.c1.H_d, Qobj))
        self.assertTrue(isinstance(self.c1.H_c, Qobj))
        self.assertTrue(isinstance(self.c1.U_0, Qobj))
        self.assertTrue(isinstance(self.c1.U_targ, Qobj))

        self.assertTupleEqual(self.c1.H_d.shape, (2,2))

    def test_config_can_yield_TimeSpec_range(self):
        # Range
        # T_start = T_QSL - T_before = 2.1 - 1.0 = 1.1
        # T_start = T_QSL + T_after = 2.1 + 1.0 = 3.1

        t_range = np.arange(1.1, 3.2, step=0.1)

        dt = 0.1
        i = 0
        for time_spec in self.c1.time_spec_range():
            self.assertAlmostEqual(1.1 + (i-1)*dt, time_spec.times[-1], 3)
            i += 1
        self.assertEqual(21, i)

    def test_config_can_return_correct_save_path(self):

        expected_path = "./run_n_2_p_1"

        self.assertEqual(Path(expected_path), self.c1.get_save_path())


    def test_collector_can_append_new_data(self):

        np.random.seed(1)
        c_seeds = np.random.random((20,7))
        c_opt = np.random.random((20, 7))
        F = np.random.random((20,))

        self.collector.collect(c_seeds, c_opt, F)

    def test_collector_can_fill_array(self):


        np.random.seed(1)

        while self.collector2.should_continue():
            c_seeds = np.random.random((20, 7))
            c_opt = np.random.random((20, 7))
            F = np.random.random((20,))
            self.collector2.collect(c_seeds, c_opt, F)

        self.assertEqual(self.collector2.n_to_collect, self.collector2.n_collected)

    def test_can_generate_data(self):

        seeds, ctrls, fidelities = self.generator.generate(self.c2.batch_size,self.time_spec.n_time_steps, self.c2.n_ctrls)

        self.assertTupleEqual(seeds.shape, (self.c2.batch_size, self.time_spec.n_time_steps))
        for f in fidelities:
            self.assertNotEqual(0, f)

        self.dyn_tc.update_max_iter(1)

        seeds, ctrls, fidelities = self.generator.generate(self.c2.batch_size, self.time_spec.n_time_steps,
                                                           self.c2.n_ctrls)

        self.assertTupleEqual(seeds.shape, (self.c2.batch_size, self.time_spec.n_time_steps))
        for f in fidelities:
            self.assertNotEqual(0, f)



    def test_can_gather_data_sheets_into_cubes(self):

        d1 = np.zeros((10,8))
        d1c = np.ones((10, 8))
        d2 = np.zeros((10, 9))
        d2c = np.zeros((10, 9))
        d3 = np.zeros((10, 10))
        d3c = np.zeros((10, 10))

        v1 = np.ones((10,))
        v2 = 2*np.ones((10,))
        v3 = 3*np.ones((10,))

        data = [(d1, d1c, v1), (d2, d2c, v2), (d3, d3c, v3)]

        seed, ctrl, fid = DataCollector.gather_sheets(data)

        self.assertTupleEqual((10, 10, 3), seed.shape)
        self.assertTupleEqual((10, 10, 3), ctrl.shape)
        self.assertTupleEqual((10, 3), fid.shape)


    def mock_generate_uniform(self, shape, f_min=0.0, f_max=1.0):

        s = np.random.random(shape)
        o = np.random.random(shape)
        f = np.random.random((shape[0],))*(f_max-f_min) + f_min
        return s, o, f

    def mock_generate_exponential(self, shape, mean = 0.015 ):

        s = np.random.random(shape)
        o = np.random.random(shape)
        f = np.random.exponential(1/mean)
        mask = f<1.0
        n_extra = sum(np.logical_not(mask))
        f = f[mask]
        f_1 = np.random.random((n_extra,))
        f = np.stack([f, f_1])
        return s, o, f

    def mock_generate_truncated_gaussian(self, shape, mean=0.15):

        s = np.random.random(shape)
        o = np.random.random(shape)
        f = truncnorm.rvs(a=0.0, b=1.0, loc=mean, scale=0.2, size=shape[0])
        return s, o, f



    def test_can_get_optimal_GRAPE_it(self):

        np.random.seed(6)

        shape = (self.collector.batch_size, 7)

        s, o, f = self.mock_generate_truncated_gaussian(shape)

        self.collector.append_batch_history(0, np.mean(f))
        self.collector.collect(s,o,f)

        self.assertEqual(self.collector.collect_it, 1)
        self.assertTupleEqual(self.collector.batch_history[0], (0, np.mean(f)))

        n_it = self.collector.get_optimal_n_GRAPE_it()

        self.assertEqual(1, n_it)

        s, o, f = self.mock_generate_truncated_gaussian(shape,mean=0.25)

        self.collector.append_batch_history(1, np.mean(f))
        self.collector.collect(s, o, f)

        self.assertEqual(self.collector.collect_it, 2)
        self.assertTupleEqual(self.collector.batch_history[1], (1, np.mean(f)))

        n_it = self.collector.get_optimal_n_GRAPE_it()















