"""
Author: Andreas Springborg
Date: 2019-05-10
Goal:

"""
import numpy as np


# Set defaults

class StylingDefaults():

    def __init__(self):
        self.fontsize = 13
        self.latexfontsize = 16
        self.largefontsize = 20
        self.ticksize = 13
        # matplotlib colors 'c1'... 'c9'
        self.colors = ['#1f77b4', '#ff7f0e',
                       '#2ca02c',
                       '#d62728',
                       '#9467bd',
                       '#8c564b',
                       '#e377c2',
                       '#7f7f7f',
                       '#bcbd22',
                       '#17becf']

    def set_ticksize(self, ax):
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(self.fontsize)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(self.fontsize)


defaults = StylingDefaults()
