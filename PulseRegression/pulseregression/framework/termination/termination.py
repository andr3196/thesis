"""
Author: Andreas Springborg
Date: 2019-04-20
Goal:

"""
from qutip.control.termcond import TerminationConditions


def get_default_termination_conditions():
    tc = TerminationConditions()
    tc.max_iterations = 200
    tc.fid_err_targ = 1e-6
    tc.max_wall_time = 600
    tc.min_gradient_norm = 1e-22
    return tc


def get_moderate_termination_conditions():
    tc = TerminationConditions()
    tc.max_iterations = 200
    tc.fid_err_targ = 1e-6
    tc.max_wall_time = 300
    tc.min_gradient_norm = 1e-8
    return tc


def get_no_optimization_termination_conditions():
    tc = TerminationConditions()
    tc.max_iterations = 0
    return tc
