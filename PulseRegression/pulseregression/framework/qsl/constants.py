
"""
Author: Andreas Springborg
Date: 21/02/2019
Goal: Define QSLscan related constants.

"""
from pathlib import Path
import platform
qsl_data_filename = "qsldata.npz"

n_n = 10
n_p = 40
n_b = 3


if platform.system() == "Darwin":
    base = Path().home() / "Dropbox/Programming/Thesis/PulseRegression"
else:
    base = Path.home() / "thesis/PulseRegression"

path_to_single_files =  base / "data/qslscan/runs/run1/data"
path_to_gathered_file = base / "data/qslscan/collected/run1"
