"""
Author: Andreas Springborg
Date: 20/02/2019
Goal:

"""
from ..qsl.constants import qsl_data_filename, path_to_gathered_file
import numpy as np


""" Define """

b_values = [0.5, 1.0, 3.0]

"""        """


def map_to_indices(n, p, b):
    if n < 2 or n > 11:
        raise ValueError("No known QSL for n={}.".format(n))
    ni = n - 2
    if p < 0 or p >= 40:
        raise ValueError("No known QSL for p={}.".format(p))
    pi = p
    if b not in b_values:
        raise ValueError("No known QSL for b={}.".format(b))
    bi = b_values.index(b)
    return ni, pi, bi


class UnknownQSLError(Exception):
    pass


def lookup_qsl(n, p, b):
    ni, pi, bi = map_to_indices(n, p, b)
    f = np.load(path_to_gathered_file / qsl_data_filename)
    qsl_cube = f["qsl"]  # shape(n_n=10, n_p=)
    T_qsl = qsl_cube[ni, pi, bi]
    if T_qsl == 0:
        raise UnknownQSLError("The archive holds no record of the QSL for this problem.")
    return T_qsl
