"""
Author: Andreas Springborg
Date: 23/02/2019
Goal: Utility methods regarding the execution of the different scenarios

"""
import os
from pathlib import Path, PosixPath
from typing import Iterable, Union

import numpy as np

from pulseregression.framework.utility.string import to_numeric


class RepeatedRunError(Exception):
    pass


def repetition_guard(full_path:Union[Path, str]):
    """ Check for the existence of the file given by full_path.
        Raises an RepeatedRunError if the file is found
    """
    if os.path.isfile(full_path):
        print("File already exists")
        raise RepeatedRunError()


def make_file_identifier(base="run", **kwargs):
    """ Generate a string tag on the form '{base}_{kw_key1}_{kw_val1}_...._{kw_valn}' """
    s = [base]
    for (v, val) in kwargs.items():
        if isinstance(val, tuple) or isinstance(val, list):
            val, vf = val
        else:
            vf = ""
        if val is None:
            s.append("*")
        else:
            s.append(("{}_{" + vf + "}").format(v, val))

    return "_".join(s)


def get_duration_index(filename, key="Ti_", end=None):
    """ Extracts the duration index from filename as the number that follows 'key' """
    i = filename.find(key)
    if i >= 0:
        begin = i + len(key)
        if end is not None:
            end = filename.find(end, begin)
        return int(filename[begin:end])
    else:
        raise ValueError("Could not find 'Ti_' in filename")


def field_loader(file_iterable: Iterable, fields: Union[str, list, tuple], duration_tags=None, prepend_Ti=True):
    """ Iterator for  the field(s) from each filepath yielded by the iterator

    :param file_iterable:
    :param fields:
    :param duration_tags:
    :param prepend_Ti:
    :return:
    """
    if isinstance(file_iterable, str) or isinstance(file_iterable, PosixPath):
        file_iterable = [file_iterable]
    if not isinstance(fields, tuple) and not isinstance(fields, list):
        fields = [fields]
    if duration_tags is None:
        duration_tags = ["Ti_", None]
    elif len(duration_tags) == 1:
        duration_tags.append(None)
    for filename in file_iterable:
        f = np.load(filename)
        field_vals = []
        for field in fields:
            field_vals.append(f[field])
        if prepend_Ti:
            Ti = get_duration_index(filename.stem, key=duration_tags[0], end=duration_tags[1])
            yield (Ti,) + tuple(field_vals)
        else:
            if len(field_vals) == 1:
                yield field_vals[0]
            else:
                yield field_vals



def data_iterator(n_n, n_p, n_b, n_T):
    """ Yield tuples of data parameters
    """
    for ni in range(n_n):
        for pi in range(n_p):
            for bi in range(n_b):
                for Ti in range(n_T):
                    yield (ni, pi, bi, Ti)


def get_parameters_from_filename(file: str):
    """ Get define N,P,B from a filename like 'run_n_N_p_P_b_B_*.npz' """
    parts = file.split("_")
    return int(parts[2]), int(parts[4]), float(parts[6])


def get_parameters_from_pathobj(file: Path):
    return get_parameters_from_filename(file.stem)


def single_control_pair_iterator(file_iterator, priority="initial"):
    """ Loads the ith control from the specified run
        returns generator if no i is supplied
    """

    for (Ti, ctrls, fids) in field_loader(file_iterator, ["ctrls", "fids"]):

        if priority == "fidelity":
            fid_order = np.argsort(1-fids)
            fids = fids[fid_order]
            ctrls = ctrls[fid_order, :]

        for i in range(len(fids)):
            yield Ti, ctrls[i, :], fids[i]


def extract_field(filepaths, begin, end=None):
    """ Given a list of Path-objects, extract the filename and locate the tag 'begin', return the tag as a number """

    res = []
    for filepath in filepaths:
        filename = filepath.stem
        i = filename.find(begin) + len(begin)
        if end is not None:
            e = filename.find(end, i)
        else:
            e = None
        p_str = filename[i:e]
        res.append(to_numeric(p_str))
    return res


def get_problem_filepaths(load_path, **kwargs):
    """ Returns a sorted list of filepaths matchin the given kwargs
    """

    # Get files
    tag = make_file_identifier(**kwargs)
    file_list = list(load_path.glob(tag + "*.npz"))
    Tis = extract_field(file_list, "Ti_")
    return [filepath for _, filepath in sorted(zip(Tis, file_list))]


def get_filenames(path, base="run", file_extension="*.npz", sort_tag="Ti_", ignore=None,  **kwargs):
    """ Get files named "{base}_{kw_key1}_{kw_val1}_...._{kw_valn}" in folder given by path
    """
    tag = make_file_identifier(base=base, **kwargs)
    file_list = list(path.glob(tag + file_extension))
    if ignore is not None:
        file_list = [file for file in file_list if ignore not in str(file)]
    if sort_tag is None:
        return file_list
    elif isinstance(sort_tag, list):
        begin, end = sort_tag
    else:
        begin = sort_tag
        end = None
    Tis = extract_field(file_list, begin=begin, end=end)
    return [filepath for _, filepath in sorted(zip(Tis, file_list))]


def select_files(file_list, tags):
    """
    Select those files from a list that contain a tag from tags
    :param file_list:
    :param tags:
    :return: The list of files including a tag
    """

    if not isinstance(tags, list) and not isinstance(tags, tuple):
        tags = [tags]

    out = []
    for file in file_list:
        for tag in tags:
            if tag in str(file):
                out.append(file)
                break
    return out
