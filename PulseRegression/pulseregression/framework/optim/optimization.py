"""
Author: Andreas Springborg
Date: 2019-04-05
Goal: Wrappers for Qutip optimization routines

"""
import numpy as np
from numpy.linalg import LinAlgError
from qutip import Qobj
from qutip.control import pulseoptim

from pulseregression.framework.control.control import optim_generate_seeds
from pulseregression.framework.termination.termination import get_default_termination_conditions
from pulseregression.framework.utility import get_n_time_steps


def optimize(optim, seeds, tc, return_hess_eigvals=False, verbose=False):
    """ Return the optimized controls for each of the seeds for the problem defined by the optimizer

    :param verbose: Prints a statement for each iteration
    :param optim: The optimizer that defines the problem and the optimiztion routine
    :param seeds: The initial guesses for the optimal controls
    :param tc: The termination conditions
    :param return_hess_eigvals: Whether to return the BFGS Hessians
    :return:
    """
    n_seeds, n_time_steps = seeds.shape

    ctrls = np.zeros(seeds.shape)
    fids = np.zeros((n_seeds,))
    if return_hess_eigvals:
        hess_eigvals = np.zeros((n_time_steps, n_seeds))
        hess_eigvals[:] = np.nan

    for i in range(n_seeds):
        pulse = seeds[i, :, np.newaxis]
        optim.dynamics.initialize_controls(pulse)
        result = optim.run_optimization(term_conds=tc)
        ctrls[i, :, np.newaxis] = result.final_amps
        fids[i] = (1 - result.fid_err) ** 2

        if return_hess_eigvals:
            try:
                eigs = np.linalg.eigvalsh(optim.hessian.Hk)
                hess_eigvals[:, i] = eigs
            except LinAlgError:
                pass
        if verbose:
            print("Finished optimizing seed {} / {}".format(i + 1, n_seeds))
    if return_hess_eigvals:
        return ctrls, fids, hess_eigvals
    return ctrls, fids


def perform_optimization(optim, n_seeds, tc, return_hess_eigvals=False, verbose=False):
    """Generate n_seeds random seeds, optimize each one and return the seeds, the optimized controls and their fidelities
    :param optim: The QuTiP unitary optimiser
    :param n_seeds: The number of seeds to optimise
    :param tc: The termination conditions
    :param return_hess_eigvals: Whether to return the eigenvalues of the optimisation Hessian
    :param verbose: Print a statement after each optimisation
    :return: seeds (n_seeds, n_features), ctrls (n_seeds, n_features), fidelities of controls (n_seeds,)
    """
    seeds = optim_generate_seeds(n_seeds, optim)
    out = optimize(optim, seeds, tc, return_hess_eigvals=return_hess_eigvals, verbose=verbose)
    if return_hess_eigvals:
        ctrls, fids, inv_hessians = out
        return seeds, ctrls, fids, inv_hessians
    else:
        ctrls, fids = out
        return seeds, ctrls, fids


def calculate_fidelity_with_optimizer(optim, seed=None):
    """ Given an optimiser, calculate the fidelity

    :param optim: The optimiser
    :param seed: The optional seed to be optimised
    :return: The fidelity of the optimised seed.
    """
    seed = prepare_seed(optim, seed)
    optim.dynamics.initialize_controls(seed)
    return (1 - optim.dynamics.fid_computer.get_fid_err()) ** 2


def calculate_drift_fidelity(optim, seed=None):
    """ Calculate the drift fidelity at a given duration by zeroing the control

    :param optim: The qutip unitary optimizer
    :param seed: Ignored
    :return: The fidelity of the system evolving only under the influence of the drift hamiltonian.
    """
    seed = prepare_seed(optim, seed) * 0
    optim.dynamics.initialize_controls(seed)
    return (1 - optim.dynamics.fid_computer.get_fid_err()) ** 2


def calculate_fidelity(state, target, sts=False):
    """ Calculates the overlap between the two unitaries U and target or if sts is True between two states.


    :param state: The current unitary or state
    :param target: The target unitary or state
    :param sts: Whether to calculate state overlap. Default False.
    :return: The overlap (fidelity) if the state and target.
    """

    if sts:
        return np.abs(np.dot(np.conj(state).T, target)) ** 2

    n = state.shape[0]
    return np.abs(1 / n * np.trace(np.dot(np.conj(target).T, state))) ** 2


def run_grape(optim, tc=None, seed=None):
    """ Given an optimizer, run GRAPE with the given TerminationConditions on either the specified seed or a randomly
    generated seed.

    The parameters of this random seed is given by the optimizer.

    :param optim: the qutip optimiser
    :param tc: The optional termination conditions object
    :param seed: The optional seed to optimise
    :return: The optimised control and its fidelity
    """

    if tc is None:
        tc = get_default_termination_conditions()

    seed = prepare_seed(optim, seed)
    optim.dynamics.initialize_controls(seed)
    result = optim.run_optimization(term_conds=tc)
    return np.reshape(result.final_amps, (len(seed),)), (1 - result.fid_err) ** 2


def get_optimizer_function(H_d: Qobj, H_c: Qobj, initial: Qobj, target: Qobj, dt: float = None, N_ts: int = None,
                           amp_lbound: float = -1.0, amp_ubound: float = 1.0, method: str = "FMIN_L_BFGS_B", method_params=None):
    """ Generate a function which takes as input a duration and returns an optimiser for that duration.

    :param H_d: The drift matrix
    :param H_c: The control matrix (possibly multiplied by control scale b)
    :param initial: The initial state or unitary
    :param target:  The target state or unitary
    :param dt: the The time step (must specify either this or n_ts)
    :param N_ts: The number of time steps between 0 and T. (must specify this or dt)
    :param amp_lbound: The lower bound on the control amplitude
    :param amp_ubound: The upper bound on the control amplitude
    :param method: The optimisation method
    :param method_params: Parameters for the optimsation method
    :return: A function which takes the duration as input and return the optimiser
    """

    if method_params is "default":
        method_params = {'max_metric_corr': 10, 'accuracy_factor': 1e-3,
                                                                'ftol': 1e-22}
    elif method_params is "moderate":
        method_params = {'max_metric_corr': 10, 'accuracy_factor': 1e3,
                                                                'ftol': 1e-15}
    if dt is None and N_ts is None:
        raise ValueError("Must specify either dt or N_ts")

    def optimizer_function(T_final):
        num_steps = N_ts
        if N_ts is None:
            num_steps = get_n_time_steps(T_final, dt)
        print("n time steps for optimiser: {}".format(num_steps))
        return pulseoptim.create_pulse_optimizer(H_d, [H_c], initial, target,
                                                 num_steps,
                                                 T_final,
                                                 amp_lbound=amp_lbound, amp_ubound=amp_ubound,
                                                 method_params=method_params,
                                                 optim_method=method,
                                                 dyn_type='UNIT',
                                                 fid_params={'phase_option': 'PSU'},
                                                 init_pulse_type="RND", pulse_scaling=(amp_ubound - amp_lbound) / 2,
                                                 gen_stats=False)

    return optimizer_function


def get_optimizer_for_problem(problem, T, n_ts=None, dt=None, amp_max=1.0):
    """ Generates an QuTiP unitary optimiser

    Uses defaults: Uniform random seeds between -amp_max and amp_max. Unitary time evolution, ignores global phase.
    For optimisation method parameters see get_optimiser_generator.

    :param problem: Tuple of drift matrix, control matrix, as well as initial state or unitary matrix and target state
    or unitary.
    :param T: Total duration
    :param n_ts: Number of time steps (must specify this or dt)
    :param dt: The step size (must specify this or n_ts)
    :param amp_max: The maximum control amplitude (upper bound)
    :return: a QuTiP optimiser
    """
    H_d, H_c, initial, target = problem
    optim_gen = get_optimizer_function(H_d, H_c, initial, target, N_ts=n_ts, dt=dt, amp_lbound=-amp_max,
                                       amp_ubound=amp_max)
    return optim_gen(T)


def prepare_seed(optim, seed=None):
    """ Given an optimiser, prepare the seed for optimisation

    If the seed is None, use the optimiser to generate a random seed. Reshape the seed for the optimiser.

    :param optim: The QuTiP optimizer object.
    :param seed: The optional seed to prepare
    :return: A reshaped seed ready for optimisation.
    """
    if seed is None:
        seed = optim.pulse_generator.gen_pulse()
    return np.reshape(seed, (len(seed), 1))
