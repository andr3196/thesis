"""
Author: Andreas Springborg
Date: 2019-05-09
Goal:

"""
import numpy as np
from scipy.signal import find_peaks
from scipy.spatial.distance import pdist
from sklearn.cluster import KMeans


def get_cluster_centres(ctrls, fids, amp_max=1.0, transform_f=None):
    """ Get the centre (mean) of each cluster in the set of controls

    :param ctrls: The controls (n_samples, n_features)
    :param fids: The fidelities of the controls (n_samples,)
    :param amp_max: The maximum amplitude of the controls
    :param transform_f: (None or "mean") Whether to compute the mean of fidelities for each cluster
    :return: cluster centres (n_clusters, n_features), list of (means of, if transform_f) fidelities, list of indices in
    the original array
    """
    labels = get_cluster_labels(ctrls, amp_max)
    clusters = split_to_clusters(ctrls, fids, labels)

    c_means = []
    for cluster in clusters:
        c, f, i = cluster
        c_means.append(np.mean(c, axis=0))
    centres_list = []
    fidelities_list = []
    indices_list = []
    for cluster in clusters:
        c, f, ind = cluster
        c_mean = np.mean(c, axis=0)
        centres_list.append(c_mean)
        indices_list.append(ind)
        if transform_f is "mean":
            fidelities_list.append(np.mean(f))
        else:
            fidelities_list.append(f)
    return np.stack(centres_list, axis=0), fidelities_list, indices_list


def get_clusters(ctrls, fids, amp_max=1.0):
    """ Get list of 3-tuples defining clusters

    :param ctrls: The controls (n_samples, n_features)
    :param fids: The fidelities of the controls (n_samples,)
    :param amp_max: The maximum amplitude of the controls
    :return: list of 3-tuples. Each tuple contains the data points for a single cluster
    """
    labels = get_cluster_labels(ctrls, amp_max=amp_max)
    return split_to_clusters(ctrls, fids, labels)


def split_to_clusters(ctrls, fids, labels):
    """Separate ndarray of controls into a list of triples (ctrls, fids, indices) denoting a cluster of controls given by the
    labels

    :param ctrls: The controls (n_samples, n_features)
    :param fids: The fidelities of the controls (n_samples,)
    :param labels: The label for each data point
    :return: list of 3-tuples. Each tuple contains the data points for a single cluster
    """
    out = []
    un_labels = np.unique(labels)
    n_clusters = len(un_labels)

    for i in range(n_clusters):
        l = un_labels[i]
        filt = labels == l
        indices = np.flatnonzero(filt)
        out.append((ctrls[filt, :], fids[filt], indices))

    return out


def get_cluster_labels(ctrls, amp_max=1.0):
    """ Get labels identifying which cluster each control belongs to

    :param ctrls: The controls (n_samples, n_features)
    :param amp_max: The maximum amplitude of the controls
    :return: ndarray of shape (n_samples,) of integer labels
    """
    k = get_num_clusters(ctrls, amp_max=amp_max)

    if k == 1:
        return np.ones((ctrls.shape[0]), dtype=np.int)
    kmeans = KMeans(n_clusters=k).fit(ctrls)
    return kmeans.labels_

def get_num_clusters(ctrls,
                     amp_max: float = 1.0,
                     p=3,
                     distance_cutoff=0.2,
                     n_right_bins=5):
    """ Estimate the number of clusters in the distribution of controls, ctrls.

    :param ctrls: The controls (n_smaples, n_features)
    :param amp_max: The maximum amplitude of the controls
    :param p: Distances are calculated as L_p norms
    :param distance_cutoff: The upper limit of local fluctuations. Distances below this threshold are not counted.
    :param n_right_bins: Number of extra bins added to the end of the distance spectrum to capture d=1.0
    :return: The k (int) to use for k-means (the number of clusters in the dataset)
    """

    # Calculate normalized distances
    all_dists = pdist(ctrls, 'minkowski', p=p)
    max_dist = ctrls.shape[1] ** (1 / p) * 2 * amp_max
    all_dists /= max_dist

    # Determine distance spectrum
    num_bins = int(np.rint(ctrls.shape[1] * (1.0 - distance_cutoff)))
    right_edge = 1 / ctrls.shape[1] * n_right_bins
    edges = np.linspace(distance_cutoff, 1.0 + right_edge, num_bins + n_right_bins)
    counts, _ = np.histogram(all_dists, edges)

    # Find peaks
    p_ind, props = find_peaks(counts, height=0, prominence=0)
    d = len(p_ind) + 1  # add contribution from all_dists < distance_cutoff

    # Each peak corresponds to a distance between two clusters
    # Thus the number of clusters, n, and the the number distances, d must be related as
    # n(n-1) = 2d

    # Hence the number of clusters must be
    D = np.ceil(np.sqrt(1 + 8 * d))

    if d < 3:  # The calculation does not hold for very small number of peaks
        n = d
    else:
        n = int((1 + D) / 2)
    return n




"""
    if plot:
        x = edges[p_ind] + 1 / (2 * ctrls.shape[1])
        y = counts[p_ind]
        cmap = cm.get_cmap("viridis")
        plt.clf()
        plt.subplot(121)
        # plt.plot([0, min_dist*0.4/max_dist], [max_count, max_count], 'k-')
        plt.plot(edges[:-1] + np.diff(edges) / 2, counts, 'b-')
        plt.plot(x, y, 'r.')
        plt.subplot(122)
        labels = KMeans(n).fit_predict(ctrls)
        un_labels = np.unique(labels)
        n_clusters = len(un_labels)

        for i in range(n_clusters):
            l = un_labels[i]
            filt = labels == l
            c_cluster = ctrls[filt, :]
            plt.plot(c_cluster.T, color=cmap(i / n_clusters))
        plt.draw()
        plt.waitforbuttonpress()
"""
