"""
File copied from

"""

import numpy as np
from sklearn.neighbors import NearestNeighbors


def hopkins(X, sample_fraction=0.1):
    """
    Compute hopkins metric. Implementation based on https://github.com/romusters/hopkins/tree/master
    :param X: {array-like} of shape = [n_samples, n_features]
    :param sample_fraction: The fraction of n_samples to use for describing the distribution. Must be small such that
    samples are likely uncorrelated.
    :return: The Hopkins metrics
    """
    n_points = X.shape[0]
    dim = X.shape[1]
    n_sample = int(sample_fraction * n_points)

    sample_indices = np.random.choice(n_points, n_sample, replace=False)

    X_sample = X[sample_indices, :]

    # Precompute NN-tree
    nbrs = NearestNeighbors(n_neighbors=1)
    nbrs.fit(X)

    # Define range of data
    X_max = np.max(X)
    X_min = np.min(X)

    # Sample min-max scaled points
    Y = (X_max - X_min) * np.random.random((n_sample, dim)) + X_min

    udists, _ = nbrs.kneighbors(Y, 1, return_distance=True)

    wdists, _ = nbrs.kneighbors(X_sample, 2, return_distance=True)

    wdists = wdists[:,1]

    H = np.sum(udists) /(np.sum(udists) + np.sum(wdists))

    return H
