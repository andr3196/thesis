"""
Author: Andreas Springborg
Date: 2019-05-10
Goal: Plot each cluster in a separate cluster

"""
from plotguard import pyplot as plt
from matplotlib import cm
import numpy as np


def cluster_plot(clusters, wait=False, hold=False, cmap=None):
    if cmap is None:
        cmap = cm.get_cmap("viridis")
    if hold:
        plt.clf()
    n_clusters = len(clusters)

    for i in range(n_clusters):
        c, f, _ = clusters[i]
        plt.plot(c.T, color=cmap((i+1) / n_clusters))

    if wait:
        plt.waitforbuttonpress()
    else:
        plt.show()

