"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""
import numpy as np
from scipy.signal import filtfilt, bessel

from pulseregression.framework.noise.noise import detect_noise


def apply_default_bessel_filter(ctrls, bounds, filt_val=0.4):
    filt = lambda x, wl: filtfilt(*bessel(3, wl, 'low'), x)

    shp = None
    if len(ctrls.shape) == 1:
        shp = ctrls.shape
        ctrls = np.reshape(ctrls, (1, len(ctrls)))
    out = apply_filter(ctrls, filt, filt_val, bounds=bounds)
    if shp is not None:
        out = np.reshape(out, shp)
    return out


def apply_filter(ctrls, filt, filt_val, bounds=None):
    ctrls1 = ctrls.copy()
    for i in range(ctrls.shape[0]):
        mask = detect_noise(ctrls1[i, :])
        if np.any(mask):
            c_mask = ctrls1[i, mask]
            if len(c_mask) > 12:
                ctrls1[i, mask] = filt(c_mask, filt_val)
    if bounds is not None:
        ctrls1[ctrls1 < bounds[0]] = bounds[0]
        ctrls1[ctrls1 > bounds[1]] = bounds[1]
    return ctrls1
