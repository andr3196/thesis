"""
Author: Andreas Springborg
Date: 2019-03-22
Goal:

"""
from plotguard import pyplot as plt
from matplotlib import cm
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter, find_peaks
from scipy.spatial.distance import pdist
from sklearn.cluster import KMeans

from pulseregression.framework.procedure.procedure import field_loader
from pulseregression.framework.utility import remove_repeated_controls, remove_negated_controls


def make_1d(x):
    s = x.shape
    if len(s) == 2:
        x = np.reshape(x, (s[1],))
    return x, s


def return_to_shape(x, shape):
    return np.reshape(x, shape)


def shift1d(x, k):
    x, shp = make_1d(x)
    n = len(x)
    x1 = np.pad(x, (np.abs(k), np.abs(k)), mode="edge")
    if k > 0:
        out = x1[0:n]
    elif k < 0:
        out = x1[-2 * k:n - 2 * k]
    else:
        out = x
    return return_to_shape(out, shp)


def squeeze1d(y, k, fill=0):
    y, shp = make_1d(y)
    n = len(y)
    xp = np.linspace(0, 1, n)
    x = np.linspace(0, 1, n + k)
    y2 = interp1d(xp, y)(x)

    s = k // 2

    if fill == "edge":
        y3 = np.ones((n,))
        y3[:n//2] = y[0]
        y3[n//2 +1:] = y[-1]
    else:
        y3 = fill * np.ones((n,))
    y3[s:s + n + k] = y2
    return return_to_shape(y3, shp)


def stretch1d(y, k=1, fill=0):
    y, shp = make_1d(y)
    if k < 0:
        return squeeze1d(return_to_shape(y, shp), k, fill=fill)
    n = len(y)
    xp = np.linspace(0, 1, n)
    x = np.linspace(0, 1, n + k)
    y2 = interp1d(xp, y)(x)
    s = int(np.floor(k / 2))
    return return_to_shape(y2[s:s + n], shp)


def stretch(ctrls, num_col=1):

    if num_col == 0:
        return ctrls

    n_dim = ctrls.ndim
    if n_dim == 1:
        remove_trailing_dimensions = True
        ctrls = np.reshape(ctrls, (1, len(ctrls)))

    n_ts = ctrls.shape[1]
    xp = np.linspace(0, 1, n_ts)
    x = np.linspace(0, 1, n_ts + num_col)
    out = interp1d(xp, ctrls, axis=1)(x)
    if remove_trailing_dimensions:
        out = np.reshape(out, (n_ts + num_col,))
    return out



def squeeze(ctrls, num_col=1):
    n_ts = ctrls.shape[1]
    xp = np.linspace(0, 1, n_ts)
    x = np.linspace(0, 1, n_ts - num_col)
    return interp1d(xp, ctrls, axis=1)(x)


def clean_controls(ctrls, fids=None, remove_repeated=True, remove_negated=False, num_dec: int = 4,
                   step_tol: float = 5e-3, verbose=False):
    n_ctrls = ctrls.shape[0]
    if remove_repeated:
        ctrls, inds1 = remove_repeated_controls(ctrls, num_dec=num_dec, step_tol=step_tol, return_indices=True)
        n_ctrls2 = ctrls.shape[0]
        if remove_negated:
            ctrls, inds2 = remove_negated_controls(ctrls, num_dec=num_dec, step_tol=step_tol, return_indices=True)
        n_ctrls3 = ctrls.shape[0]
        if verbose:
            print("Removed {} repeated and {} antisymmetric controls.".format(n_ctrls - n_ctrls2, n_ctrls2 - n_ctrls3))
    if fids is not None:
        fids = fids[inds1]
        if remove_negated:
            fids = fids[inds2]
        return ctrls, fids
    return ctrls


def has_interface(ctrl, n_interfaces=1):
    """ Determines if a control has the exact number of non (+1, -1) define given by n_interfaces """
    return np.sum(np.abs(ctrl) != 1.0) == n_interfaces


def get_interface_indices(ctrl, n_interfaces=1):
    """ Determines the first n_interfaces indices of non (+1, -1) control define"""
    return np.nonzero(np.abs(ctrl) != 1.0)[0][:n_interfaces]


def find_control_with_interface(file, n_interfaces=1):
    """ Searches through a n_controls-by-n_times matrix saved in an npz file, "file", as a field "ctrls" for a control
        that has n interfaces. Returns None is none is found.
    """
    for (Ti, ctrls) in field_loader([file], "ctrls"):

        for i in range(ctrls.shape[0]):
            if has_interface(ctrls[i, :], n_interfaces=n_interfaces):
                return ctrls[i, :]
    return None


def apply_smoothing(ctrls, beta=0.0, window_length=11, polyorder=1, method="savgol", deriv=0, delta=1.0, bounds=None):
    """
    Apply splines to smoothe ctrls

    """

    if method == "savgol":
        res = savgol_filter(ctrls, window_length, polyorder=polyorder, deriv=deriv, delta=delta, axis=1)
    elif method == "mean":

        m = np.mean(ctrls, axis=0)
        res = (1 - beta) * ctrls + beta * m
    else:
        raise NotImplementedError("Unknown smoothing method: {}".format(method))

    if bounds is not None:
        res[res < bounds[0]] = bounds[0]
        res[res > bounds[1]] = bounds[1]
    return res
