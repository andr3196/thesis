"""
Author: Andreas Springborg
Date: 2019-03-25
Goal:

"""
import numpy as np


def generate_seeds(n_samples: int, n_time_steps: int, a_max: float = 1.0, a_min: float = -1.0):
    """Generate a matrix of uniform random seeds within the range [a_min, a_max]

    :param n_samples:
    :param n_time_steps:
    :param a_max:
    :param a_min:
    :return:
    """
    return (a_max - a_min) * np.random.random((n_samples, n_time_steps)) + a_min


def optim_generate_seeds(n_samples, optim):
    """ Genrate a matrix of n_samples seeds based on the pulse_generator of the optimizer
    """
    n_time_steps = optim.pulse_generator.num_tslots
    seeds = np.zeros((n_samples, n_time_steps))
    for i in range(n_samples):
        seeds[i, :] = optim.pulse_generator.gen_pulse()
    return seeds


def get_optimal_control_pair(ctrls, fids):
    """ Given an array of ctrls and a list of fidelity return the control and fidelity with the highest fidelity """
    i = np.argmax(fids)
    return ctrls[i, :], fids[i]
