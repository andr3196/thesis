"""
Author: Andreas Springborg
Date: 2019-03-21
Goal: Addditional string methods

"""


def to_numeric(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            raise ValueError("Could not interpret {} as a number".format(s))
