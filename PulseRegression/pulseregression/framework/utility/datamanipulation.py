"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
import numpy as np
from scipy.spatial.distance import cdist, squareform, pdist


def calc_control_length(ctrls, padding=0):
    """
    Calculates the number of entries in each row which is different from the padding value
    :return: The length of the control in each row as a vector

    """
    return np.sum(ctrls != padding, axis=1)


def add_dimension(data):
    """
    Adds an extra dimension to all_controls feature matrices in data
    :param data: tuple of data set components
    :return: data with extra dimension
    """
    all_data = list(data[:])
    for i in range(0, len(data), 2):
        all_data[i] = np.expand_dims(all_data[i], axis=-1)
    return tuple(all_data)


def reshape(ctrls, fids):
    # Reshape
    print(ctrls.shape, fids.shape)
    n_T_final = ctrls.shape[2]
    print("n t final: ", n_T_final)
    ctrls_list = np.dsplit(ctrls, n_T_final)
    fids_list = np.hsplit(fids, n_T_final)
    ctrls = np.vstack(ctrls_list).squeeze()
    fids = np.vstack(fids_list).squeeze()
    return ctrls, fids


def remove_empty_rows(ctrls, fids):
    # Remove empty rows
    all_zero_rows = np.all(np.logical_or(ctrls == 0, np.isnan(ctrls)), axis=1)
    ctrls = ctrls[np.logical_not(all_zero_rows), :]
    fids = fids[np.logical_not(all_zero_rows)]
    return ctrls, fids


def shuffle(ctrls, fids):
    # randomize data
    rand_index = np.random.permutation(ctrls.shape[0])
    ctrls = ctrls[rand_index, :]
    fids = fids[rand_index]
    return ctrls, fids


def crop(ctrls, fids, N_samples):
    # crop number of samples if requested
    if N_samples is not None:
        n_limit = min(N_samples, len(fids))
        ctrls = ctrls[:n_limit, :]
        fids = fids[:n_limit]
    return ctrls, fids


def fill_nan(ctrls, val=0.0):
    # Replace NaNs with fill value
    ctrls[np.isnan(ctrls)] = val

    return ctrls


def stack(ctrl_list, fids_list=None, fill_val=0, concatenate=True):
    """

    NB: not backwards compatible - should accept both 1-D and 2-D arrays
    """

    max_length = max([x.shape[-1] for x in ctrl_list])

    if len(ctrl_list[0].shape) == 2:
        dim = 1
        cat_axis = 2
        padding_fct = lambda x: ((0, 0), (0, max_length - x.shape[dim]))
    elif len(ctrl_list[0].shape) == 1:
        dim = 0
        cat_axis = 1
        padding_fct = lambda x: ((0, max_length - x.shape[dim]),)
    else:
        raise ValueError("Unexpected number of dimensions of elements in ctrl_list")

    ctrl_list = [np.pad(x, padding_fct(x),
                        'constant', constant_values=(fill_val, fill_val)) for x in ctrl_list]
    if concatenate:
        ctrls = np.concatenate(ctrl_list, axis=0)
    else:
        ctrls = np.stack(ctrl_list, axis=cat_axis)

    if fids_list is not None:
        if isinstance(fids_list[0], float):
            fids = np.array(fids_list)
        else:
            fids = np.concatenate(fids_list)
        return ctrls, fids
    else:
        return ctrls


def prepend_vector(pre_vec, arr):

    n_rows = arr.shape[0]
    pre_arr = np.tile(pre_vec, (n_rows, 1))
    return np.concatenate((pre_arr, arr), axis=1)





def sort_by_rows(arr, return_indices=False):
    ind = np.lexsort(np.fliplr(arr).T, axis=0)
    if return_indices:
        return arr[ind, :], ind
    else:
        return arr[ind, :]


def remove_repeated_controls(ctrls, num_dec=4, step_tol=5e-3, return_indices=False):
    """ Given a set of controls, C, remove c_i if sum(abs(c_ij - c_kj)) < N_ts * step_tol for vectors c_i and c_k in
    C and i > k. May remove controls that are in fact not repeated if the controls form a sequence where each control
    is close to the one before. If return_indices=True, return the indices that produce the output from the input """

    r_ctrls = np.round(ctrls, num_dec)
    _, inds = sort_by_rows(r_ctrls, return_indices=True)
    ctrls = ctrls[inds, :]
    diffs = np.sum(np.abs(np.diff(ctrls, axis=0)), axis=1)
    tol = step_tol * ctrls.shape[1]
    unique = np.insert(diffs > tol, 0, [True])
    if return_indices:
        out_order = np.arange(ctrls.shape[0])
        out_order = out_order[inds]
        return ctrls[unique, :], out_order[unique]
    return ctrls[unique, :]


def remove_negated_controls(unique_ctrls, num_dec=4, step_tol=5e-3, return_indices=False):
    """ Given a set of controls, C, remove c_i if -c_i is also in C. """

    super_ctrls = np.vstack((unique_ctrls, -unique_ctrls))
    is_orig = np.concatenate((np.ones((unique_ctrls.shape[0],)), np.zeros((unique_ctrls.shape[0],)))) == 1
    r_ctrls = np.round(super_ctrls, num_dec)
    _, inds = sort_by_rows(r_ctrls, return_indices=True)
    super_ctrls = super_ctrls[inds, :]
    is_orig = is_orig[inds]
    diffs = np.sum(np.abs(np.diff(super_ctrls, axis=0)), axis=1)
    tol = step_tol * unique_ctrls.shape[1]
    unique = np.logical_and(np.insert(diffs > tol, 0, [True]), is_orig)
    if return_indices:
        out_indices = np.arange(super_ctrls.shape[0])
        out_indices = out_indices[inds]
        out_indices = out_indices[unique]
        out_indices[out_indices >= unique_ctrls.shape[0]] -= unique_ctrls.shape[0]
        return super_ctrls[unique, :], out_indices
    return super_ctrls[unique, :]
