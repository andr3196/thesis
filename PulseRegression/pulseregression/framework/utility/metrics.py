"""
Author: Andreas Springborg
Date: 13/02/2019
Goal: Defines functions for calculating predictions metrics

"""
from logging import warning

import numpy as np
from scipy.spatial.distance import pdist, squareform
from sklearn.metrics import mean_absolute_error


def calculate_min_fidelity(fids):
    """ Get the minimum fidelity """
    return np.min(fids)


def calculate_quantile(fids, q):
    """ Get the qth fidelity quantile"""
    return np.quantile(fids, q)


def calculate_max_fidelity(fids):
    """ Get the maximum fidelity """
    return np.max(fids)


def calculate_median_fidelity(fids):
    """ Get the maximum fidelity """
    return np.median(fids)


def calculate_errors(loss_history, test_phase_data, fid_threshold):
    """ Calculate the test set error, the minimum validation error and the error on the part of the test set with a
    fidelity above fid_threshold """
    test_error = mean_absolute_error(test_phase_data[0], test_phase_data[1])
    if loss_history is not None:
        min_error = np.min(loss_history[1])
    else:
        min_error = np.nan
    high_fids_filter = test_phase_data[0] > fid_threshold
    high_calc_fids = test_phase_data[0][high_fids_filter]
    high_pred_fids = test_phase_data[1][high_fids_filter]
    if np.any(high_fids_filter):
        high_error = mean_absolute_error(high_calc_fids, high_pred_fids)
    else:
        high_error = np.nan
    return test_error, min_error, high_error


def calculate_order_metric(ctrls, max_amp=1.0, normalize_to=None):
    """ Calculate the order metric given by:


        q(T) = mean_t( mean_n(  (ctrls - mean_n(ctrls))^2  ) )/ (range**2)
        where mean_n is taken across different controls (rows of ctrls) and mean_t is taken across time (cols of ctrls)

        if normalize_to is not none, a second set of controls must be given that define maximum disorder

    """
    means = np.mean(ctrls, axis=0)
    variances = np.square(ctrls - means)
    mean_variances = np.mean(variances, axis=0)
    if normalize_to is not None:
        q = calculate_order_metric(normalize_to, max_amp)
    else:
        q = 1.0
    return np.mean(mean_variances) / max_amp ** 2 / q


def calculate_mean_magnetization(ctrls, max_amp=1.0):
    """ Calculate the mean magnetization of a set of controls. The magnetization is given as

        M(T) = mean_n(   abs(mean_t( ctrls )  )  )
        where mean_n is taken across different controls (rows of ctrls) and mean_t is taken across time (cols of ctrls)
    """
    ms = np.mean(ctrls, axis=1)
    return np.mean(np.abs(ms)) / max_amp


def calculate_symmetric_fraction(ctrls, noise_level=0.02):
    """ Calculate the mean magnetization of a set of controls. The magnetization is given as

        M(T) = mean_n(   abs(mean_t( ctrls )  )  )
        where mean_n is taken across different controls (rows of ctrls) and mean_t is taken across time (cols of ctrls)
    """
    dc = ctrls - (- np.fliplr(ctrls))
    mags = np.abs(np.sum(dc, axis=1))
    n_sym = np.sum(mags < ctrls.shape[1] * noise_level)

    return n_sym / ctrls.shape[0]


def count_num_unique(ctrls, threshold=1.0):
    """ Count the number of ctrls that are unique. Uniqueness is given as having a Euclidian distance larger than
    threshold """
    c2 = np.ones((ctrls.shape[0],))
    all_dists = pdist(ctrls)
    dists = squareform(all_dists)
    for i in range(ctrls.shape[0]):
        for j in range(i + 1, ctrls.shape[0]):
            if dists[i, j] < threshold:
                c2[j] = 0
    return np.sum(c2)


def calculate_bangbang_fraction(ctrls, violations_th=0, bang_th=0.99, max_amp=1.0):
    """ Calculate the fraction of the controls of length N_ts that have N_ts-threshold control define equal to their
    extremal define """

    return calculate_bangsingular_fraction(ctrls, violations_th, sing_th=0, bang_th=bang_th, max_amp=max_amp)


def calculate_bangsingular_fraction(ctrls, violations_th=0, sing_th=0.01, bang_th=0.99, max_amp=1.0):
    """ Calculate the fraction of the controls of length N_ts that consists of bangs (|amp| > bang_th*amp_max) and singular sections (|amp| < singular_th)
        The counter allows violations_th number of amplitudes not fullfilling these conditions
    """

    n = ctrls.shape[0]
    n_non_bb = np.sum(
        np.logical_not(np.logical_or(np.abs(ctrls) >= bang_th * max_amp, np.abs(ctrls) <= sing_th * max_amp)), axis=1)
    n_bb = np.sum(n_non_bb <= violations_th)
    return n_bb / n


def calculate_bangbangness(ctrls, max_amp=1.0):
    """ Calculate bang-bang-likeness:

        B = 1/N_ts sum_i c_i^2
    """
    return np.mean(np.square(ctrls), axis=0) / max_amp ** 2


def calculate_jitter(ctrls, max_amp=1.0):
    """ Calculate the mean squared difference between the control at T and T+dT normalized to [0,1]

        J = 1/range_width^2 mean_i (c_i-c_(i+1))^2

    """
    return np.mean(np.square(np.diff(ctrls, axis=1)), axis=1) / (2 * max_amp) ** 2


def calculate_mean_distance(ctrls, metric="euclidean", normalize=False, max_amp=1.0, single_ctrl_value=np.nan):
    """ Calculate the mean distance between each pair of controls """

    if ctrls.shape[0] == 1:
        return single_ctrl_value

    if normalize:
        norm = np.sqrt(ctrls.shape[1] * (2 * max_amp) ** 2)
        if metric != "euclidean":
            warning("Normalization only works for Euclidean distance")
    else:
        norm = 1.0
    return np.mean(pdist(ctrls, metric)) / norm
