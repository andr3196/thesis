
"""
Author: Andreas Springborg
Date: 29/01/2019
Goal:

"""
from pulseregression.framework.utility import calculate_errors


class ErrorCollector(object):

    def __init__(self, high_fid_threshold: float = 0.9):
        self.high_fid_threshold = high_fid_threshold
        self.total_test_error = []
        self.min_valid_error = []
        self.high_fid_test_error = []
        self.phase_data = []

    def collect(self, loss_history, test_data):
        test_error, min_error, high_error = calculate_errors(loss_history, test_data,self.high_fid_threshold)
        self.total_test_error.append(test_error)
        self.min_valid_error.append(min_error)
        self.high_fid_test_error.append(high_error)
        self.phase_data.append(test_data)

    def print_last(self):
        if self.total_test_error:
            print(self.total_test_error[-1], self.min_valid_error[-1], self.high_fid_test_error[-1])
        else:
            print("Empty collector")

    def get_all_errors(self):
        return self.total_test_error, self.min_valid_error, self.high_fid_test_error


