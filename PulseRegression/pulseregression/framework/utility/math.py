"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
from scipy.stats import norm
import numpy as np


def double_folded_gaussian_pdf(x, mu, sigma):
    return 0.1 * (norm.pdf(x, loc=mu, scale=sigma)
                  + norm.pdf(x, loc=-mu, scale=sigma)
                  + norm.pdf(2 - x, loc=mu, scale=sigma)
                  + norm.pdf(2 - x, loc=-mu, scale=sigma))


