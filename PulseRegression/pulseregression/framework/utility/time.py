"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
import numpy as np


def get_n_time_steps(T, dt):
    return np.rint(T / dt).astype(int)


def get_real_duration(Ti, T_min, T_max, n_step):
    return (T_max - T_min) * Ti / n_step + T_min


def make_duration_range(T_QSL, low, high, scale=None, step=None, num_decimals=1):
    """
    Creates a range of process durations from low*T_QSL to high*T_QSL with step size scale*T_QSL
    :param step: The time separation of consecutive durations
    :param T_QSL: The quantum speed limit
    :param scale: The step size relative to the T_QSL
    :param low: The lower duration limit as a fraction of the QSL  (< 1)
    :param high: The upper duration limit as a fraction of the QSL (> 1)
    :param num_decimals: The rounding of the result
    :return: ndarray of durations
    """
    if T_QSL == 0.0:
        raise ValueError("Invalid QSL: {}".format(T_QSL))
    if (step is None) == (scale is None):
        raise ValueError("Must specifiy 1 and just 1 of step and scale")
    if step is not None:
        scale = step / T_QSL
    else:
        step = T_QSL * scale
    n_steps_below = np.floor((1 - low) / scale)
    n_steps_above = np.floor((high - 1) / scale)
    r = np.round((np.arange(0, n_steps_below + n_steps_above + 1) - n_steps_below) * step + T_QSL, num_decimals)
    return r[r > 0.0]



class TimeSpec(object):
    """ Defines a time series for a control. Can be specified as

    A) end, n_steps (start optional)
    B) end, step_size (start optional)
    C) n_steps, step_size (start optional)
    D) times ndarray (all_controls other arguments must be None)

    """

    def __init__(self, start: float = None, end: float = None, n_steps: int = None, step_size: float = None,
                 times: np.ndarray = None):
        default_n_steps = 30
        if start is None:
            start = 0
        if times is None:
            if end is not None:
                if n_steps is not None:
                    self.times = np.linspace(start, end, n_steps, endpoint=False)
                elif step_size is not None:
                    self.times = np.arange(start, end, step_size)
                else:
                    self.times = np.linspace(start, end, default_n_steps, endpoint=False)
            elif all([n_steps, step_size]):
                self.times = np.linspace(start, start + (n_steps - 1) * step_size, n_steps)
            else:
                raise ValueError("Invalid time specification")
        else:
            if any([start, end, n_steps, step_size]):
                raise ValueError("Invalid time specification")
            self.times = times
        self.n_time_steps = len(self.times)
        if self.n_time_steps > 1:
            self.dt = self.times[1] - self.times[0]
        elif step_size:
            self.dt = step_size
        else:
            self.dt = end
        self.T_final = self.times[-1] + self.dt

    def __getitem__(self, item):
        return self.times[item]
