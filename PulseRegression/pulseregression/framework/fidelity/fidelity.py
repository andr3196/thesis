"""
Author: Andreas Springborg
Date: 2019-04-26
Goal:

"""
import numpy as np
import warnings


def interpolate_for_tmin(T_range, fids, F_min=0.999):
    """ Interpolate fidelities at different durations to obtain T_min

    If Fi <=

    :param T_range:
    :param fids:
    :param F_min:
    :return:
    """

    data_it = iter(zip(T_range, fids))

    T, fids = next(data_it)

    F_cur = np.max(fids)
    T_cur = T

    for T, fids in data_it:

        T_last = T_cur
        F_last = F_cur

        T_cur = T
        F_cur = np.max(fids)

        if F_cur > F_min:
            break
    else:
        warnings.warn("No datapoint above QSL fidelity = {}. Final was {}".format(F_min, F_cur))
        return np.nan

    # Perform linear interpolation to get QSL
    return (F_min - F_last) / (F_cur - F_last) * (T_cur - T_last) + T_last
