"""
Author: Andreas Springborg
Date: 2019-04-05
Goal:

"""
import numpy as np
from plotguard import pyplot as plt
from pulseregression.framework.utility.time import get_real_duration


def display_distance_distribution(distances, Ti=None, n_step=14, T_min=0.2, T_max=1.6, norm=1.0, n=None, p=None, b=None,
                                  close_after_plotting=False,
                                  filter=None):
    """
    Plots a histogram of the distances
    :param Ti: The duration index
    :param distances: The 1D-array of distances
    :param n_step: The number of duration steps in the range
    :param T_min: The min duration
    :param T_max: The max duration
    :param norm: The normalization factor (sets the scale for largest possible distance)
    :param n: problem order
    :param p: problem index
    :param b: control scale
    :param close_after_plotting: Close figure after plotguard
    :param filter: To subselect only particular Ti
    """

    if filter is not None and not filter(Ti):
        return
    T = get_real_duration(Ti, T_min, T_max, n_step)
    plt.figure()
    plt.hist(distances / norm, np.linspace(0, 1, 201))
    plt.title("T/T_QSL= {:03.2f}".format(T))
    plt.xlabel("Normalized opt-opt distances")
    plt.ylabel("Counts")
    if n is not None and p is not None and b is not None and Ti is not None:
        plt.savefig("distance_distributions_n_{}_p_{}_b_{}_Ti_{}.png".format(n, p, b, Ti), format="png")
    else:
        plt.savefig("distance_distributions_Ti_{}.png".format(Ti), format="png")
    if close_after_plotting:
        plt.close()
    else:
        plt.show()
