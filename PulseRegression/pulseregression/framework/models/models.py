"""
Author: Andreas Springborg
Date: 28/01/2019
Goal: this file holds build functions for various types of models

"""
from typing import Iterable, Union

import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, RationalQuadratic
from tensorflow import keras as k
from hpsklearn import HyperoptEstimator, any_preprocessing, xgboost_regression, knn_regression, svr_rbf, \
    ada_boost_regression, random_forest_regression
from hyperopt import tpe


def build_mlp(input_dim: int, layer_units: int, n_layers: int, name: str, with_dropout=False, dropout_pct=0.15):
    layers = []
    for i in range(n_layers):
        if i == 0:
            layer = k.layers.Dense(units=layer_units, input_dim=input_dim, activation='relu')
        else:
            layer = k.layers.Dense(units=layer_units, activation='relu')
        layers.append(layer)
        if with_dropout:
            layers.append(k.layers.Dropout(dropout_pct))
    layers.append(k.layers.Dense(units=1, activation='sigmoid'))
    model = k.Sequential(layers=layers, name=name)
    model.compile(loss="mean_squared_error", optimizer="adam", metrics=["mae"])  # Mean squared error loss function
    return model


def batch_dense_layers(n_dense_layers: int, n_units: int, with_dropout: bool = False, dropout_pct: float = 0.15):
    layers = []

    for i in range(n_dense_layers):
        layers.append(k.layers.Dense(units=n_units, activation='relu'))
        if with_dropout:
            layers.append(k.layers.Dropout(dropout_pct))
    layers.append(k.layers.Flatten())
    return layers


def build_cnn(input_dim: int,
              n_conv_layers: int,
              n_units: Union[int, Iterable],
              name: str,
              kernel_size: int = 3,
              with_pooling: bool = True,
              pool_size: int = 2,
              n_dense_layers: int = 1,
              with_dropout: bool = False,
              dropout_pct: float = 0.15
              ):
    if isinstance(n_units, int):
        n_units = [n_units for _ in range(n_conv_layers)]

    layers = []
    for i in range(n_conv_layers):
        if i == 0:
            layer = k.layers.Conv1D(filters=n_units[i], kernel_size=kernel_size, input_shape=(input_dim, 1),
                                    activation='relu')
        else:
            layer = k.layers.Conv1D(filters=n_units[i], kernel_size=kernel_size, activation='relu')
        layers.append(layer)
        if with_pooling:
            layers.append(k.layers.MaxPool1D(pool_size))
    layers.extend(batch_dense_layers(n_dense_layers, n_units[-1], with_dropout, dropout_pct))
    layers.append(k.layers.Dense(units=1, activation='sigmoid'))
    model = k.Sequential(layers=layers, name=name)
    model.compile(loss="mean_squared_error", optimizer="adam", metrics=["mae"])  # Mean squared error loss function
    return model


def build_lstm(input_dim,
               n_lstm_layers: int,
               n_cells: int, name: str,
               n_dense_layers: int = 1,
               with_dropout=False,
               dropout_pct=0.15):
    return build_sequence_model(k.layers.LSTM, input_dim, n_lstm_layers, n_cells, name, n_dense_layers, with_dropout,
                                dropout_pct)


def build_gru(input_dim,
              n_gru_layers: int,
              n_cells: int, name: str,
              n_dense_layers: int = 1,
              with_dropout=False,
              dropout_pct=0.15):
    return build_sequence_model(k.layers.GRU, input_dim, n_gru_layers, n_cells, name, n_dense_layers, with_dropout,
                                dropout_pct)


def build_sequence_model(model_type,
                         input_dim,
                         n_model_layers: int,
                         n_cells: int, name: str,
                         n_dense_layers: int = 1,
                         with_dropout=False,
                         dropout_pct=0.15):
    if n_model_layers == 1:
        layers = [model_type(n_cells, input_shape=(input_dim, 1))]
    else:
        layers = [model_type(n_cells, return_sequences=True, input_shape=(input_dim, 1))]

    for i in range(n_model_layers - 1):
        if i < n_model_layers - 1:
            layers.append(model_type(n_cells, return_sequences=True))
        else:
            layers.append(model_type(n_cells))
    layers.extend(batch_dense_layers(n_dense_layers, n_cells, with_dropout, dropout_pct))
    layers.append(k.layers.Dense(units=1, activation='sigmoid'))
    model = k.Sequential(layers=layers, name=name)
    opt = k.optimizers.Adam(lr=0.5e-3)
    model.compile(loss="mean_squared_error", optimizer=opt, metrics=["mae"])  # Mean squared error loss function
    return model


""" Sklearn models """
n_max_evals = 25
trial_max_time = 600


def build_gaussian_process(kernel, input_dim):
    if kernel == "rbf":
        kern = RBF(np.ones((input_dim,)))
    elif kernel == "rational":
        kern = RationalQuadratic()
    else:
        raise NotImplementedError("Kernel '{}' not implemented".format(kernel))
    gp = GaussianProcessRegressor(kern, copy_X_train=False, n_restarts_optimizer=10)
    gp.name = "Gaussian_Proc"
    return gp


def build_estimator(a_regressor, name):
    estim = HyperoptEstimator(preprocessing=any_preprocessing("my_pre"),
                              regressor=a_regressor,
                              algo=tpe.suggest,
                              max_evals=n_max_evals,
                              trial_timeout=trial_max_time,
                              verbose=True
                              )
    estim.name = name
    return estim


def build_xgboost(name):
    return build_estimator(xgboost_regression("my_XGB"), name)


def build_svr_rbf(name):
    return build_estimator(svr_rbf("my_svr_rbf"), name)


def build_knn(name):
    return build_estimator(knn_regression("my_knn"), name)


def build_ada_boost(name):
    return build_estimator(ada_boost_regression("my_ada_boost"), name)


def build_random_forest(name):
    return build_estimator(random_forest_regression("my_random_forest"), name)


class RandomBaselineModel():

    def fit(self, X, y):
        # This model does not train
        pass

    def predict(self, X):
        return np.random.random((X.shape[0],))

    def save(self, name):
        # This model cannot be saved
        pass


def build_baseline_model(name):
    model = RandomBaselineModel()
    model.name = name
    return model
