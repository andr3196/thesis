from __future__ import print_function, division

import sys

from tensorflow import keras as k
from typing import Tuple

import matplotlib.pyplot as plt
from matplotlib import gridspec as gspc

import numpy as np
from tensorflow.contrib.specs.python import Reshape
from tensorflow.python.keras import Input
from tensorflow.python.keras.engine.training import Model
from tensorflow.python.keras.layers import LeakyReLU, BatchNormalization, Lambda, Activation
from tensorflow.python.keras.models import load_model, Sequential
from tensorflow.python.layers.core import Dense

from pulseregression.framework.io.fileio import retrieve_run

"""
Based on: https://github.com/eriklindernoren/Keras-GAN#gan
"""


class GAN(object):
    def __init__(self, control_dims: Tuple[int, ...], disc_model_file: str):
        self.img_rows = control_dims[0]
        self.img_cols = control_dims[1]
        self.control_shape = control_dims
        self.latent_dim = 100
        self.mean_fid_array = []

        # descriminator mapping
        self.x_low = 0.85
        self.x_high = 0.98
        self.metrics = ['d_loss', 'd_acc', 'g_loss']
        self.training_history = {}

        # For plotguard
        self.n_samples = 4
        self.n_mean_samples = 100
        self.sample_frequency = 400
        self.sample_runs = None

        optimizer = k.optimizers.Adam(0.0002, 0.5)

        # Build and compile the discriminator
        self.discriminator = self.build_discriminator(disc_model_file)
        self.evaluator = load_model(disc_model_file)
        # self.discriminator.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])

        # Build the generator
        self.generator = self.build_generator()

        # The generator takes noise as input and generates imgs
        z = k.Input(shape=(self.latent_dim,))
        img = self.generator(z)

        # For the combined model we will only train the generator
        self.discriminator.trainable = False

        # The discriminator takes generated images as input and determines validity
        validity = self.discriminator(img)

        # The combined model  (stacked generator and discriminator)
        # Trains the generator to fool the discriminator
        self.combined = Model(z, validity)
        # self.combined.compile(loss='binary_crossentropy', optimizer=optimizer)
        self.combined.compile(loss='mean_squared_error', optimizer=optimizer)

    def build_generator(self):

        model = Sequential()

        model.add(Dense(256, input_dim=self.latent_dim))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(1024))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(np.prod(self.control_shape), activation='tanh'))
        model.add(Reshape(self.control_shape))

        model.summary()

        noise = Input(shape=(self.latent_dim,))
        img = model(noise)

        return Model(noise, img)

    def build_discriminator(self, file_name):

        model = load_model(file_name)

        # Define selection mechanism
        # map range x_low to x_high to -2.5 to 2.5
        a = 5 / (self.x_high - self.x_low)
        b = 2.5 - a * self.x_high

        # use hard_sigmoid: x > 2.5 => 1, x < -2.5 => 0, else x -> 0.2*x + 0.5
        # map fidelity to this
        model.add(Lambda(lambda x: a * x + b))
        model.add(Activation('hard_sigmoid'))
        # model.add(Lambda(lambda x: tf.to_float(x > 0.95) )) # Transform fidelities into x < 0.95 -> 0 else -> 1
        model.summary()

        # control = Input(shape=self.control_shape)
        # validity = model(control)

        return model  # Model(control, validity)

    def train(self, epochs, batch_size=128, sample_interval=50):

        self.sample_frequency = sample_interval
        self.n_epochs = epochs
        # prepare history
        for metric in self.metrics:
            self.training_history[metric] = np.zeros(epochs)

        # Load the dataset
        controls, fidelities = retrieve_run(filename="time_scan_all1n359354", folder="../config/"
                                                                                     "data/time_scan_dataw01w12")

        filt = fidelities > self.x_high
        high_f_controls = controls[filt, :]

        print("There are {} high fidelitiy controls in the data set.".format(len(high_f_controls)))

        # Adversarial ground truths
        valid = np.ones((batch_size, 1))
        fake = np.zeros((batch_size, 1))

        for epoch in range(epochs):

            # ---------------------
            #  Train Discriminator
            # ---------------------

            # Select a random batch of images
            idx = np.random.randint(0, high_f_controls.shape[0], batch_size)
            controls_batch = high_f_controls[idx, :]

            noise = np.random.normal(0, 1, (batch_size, self.latent_dim))

            # Generate a batch of new images
            gen_controls = self.generator.predict(noise)

            # Train the discriminator

            d_loss_real = self.discriminator.train_on_batch(controls_batch[:, :, np.newaxis], valid)
            d_loss_fake = self.discriminator.train_on_batch(gen_controls, fake)
            d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

            # ---------------------
            #  Train Generator
            # ---------------------

            noise = np.random.normal(0, 1, (batch_size, self.latent_dim))

            # Train the generator (to have the discriminator label samples as valid)
            g_loss = self.combined.train_on_batch(noise, valid)

            # Save history
            res = [d_loss[0], d_loss[1], g_loss]
            for i in range(len(self.metrics)):
                self.training_history[self.metrics[i]][epoch] = res[i]

            # Plot the progress
            print("%d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, d_loss[0], 100 * d_loss[1], g_loss))

            # If at save interval => save generated image samples
            if epoch % sample_interval == 0:
                self.sample_controls(epoch)

    def sample_controls(self, epoch):
        # r, c = 5, 5
        noise = np.random.normal(0, 1, (self.n_mean_samples, self.latent_dim))
        gen_controls = self.generator.predict(noise)
        pred_fids = self.evaluator.predict(gen_controls)
        # times = np.linspace(0.1, 3.0, pred_fids.shape[0])
        mean_fid = np.mean(pred_fids)
        self.mean_fid_array.append(mean_fid)
        print("Mean fidelity at epoch {} is Fmean = {}".format(epoch, mean_fid))

        if self.sample_runs is None:
            self.n_sample_times = int(self.n_epochs / self.sample_frequency) + 1
            n_control_times = gen_controls.shape[1]
            self.sample_runs = np.zeros((self.n_sample_times, n_control_times, self.n_samples))

        i = int(epoch / (self.sample_frequency * 1.0))
        self.sample_runs[i, :, :] = np.squeeze(gen_controls[0:self.n_samples,:,:]).T

    def finalize(self):

        # This is where we construct the figure of the generated controls

        # We need one more run
        self.sample_controls(self.n_epochs)

        # We create the figure and separate it into a left and right part
        fig = plt.figure(figsize=(8, 14))
        outer = gspc.GridSpec(1, 2, width_ratios=[4, 1])

        # The number of rows in the figure is the number of sample_times
        rows = int(self.n_sample_times/2) + 1
        left_cols = self.n_samples

        # Create the left side
        left = gspc.GridSpecFromSubplotSpec(rows, left_cols,
                                            subplot_spec=outer[0])
        # Gather the dimensions of the left subplot
        boxes = np.zeros((rows, 4))

        # The control times
        times = np.arange(0.1, 3.1, 0.1)

        for i in range(rows):
            for j in range(left_cols):
                ax = plt.Subplot(fig, left[i, j])
                ax.plot(times, self.sample_runs[2*i, :, j])
                ax.set_ylim(-1, 1)
                ax.set_xlim(0, 3.1)
                if j > 0:
                    ax.set_yticks([])
                if i < rows - 1:
                    ax.set_xticks([])
                fig.add_subplot(ax)
            boxes[i, :] = list(ax.get_position().bounds)

        right = gspc.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[1])
        ax = plt.Subplot(fig, right[0])
        tick_pos = np.linspace(boxes[-1, 1] - boxes[-1, 3] / 2, boxes[0, 1] - boxes[0, 3] / 2, self.n_sample_times)
        tick_labels = [str(self.sample_frequency * i) for i in range(len(tick_pos))][::-1]
        self.mean_fid_array.reverse()
        ax.scatter(self.mean_fid_array, tick_pos)
        ax.set_ylim(boxes[-1, 1] - boxes[-1, 3], boxes[0, 1])
        ax.set_xlim(0, 1)
        ax.set_yticks(tick_pos)
        ax.set_yticklabels(tick_labels)
        ax.set_xlabel('Mean fidelity')
        ax.set_ylabel('Epoch number')
        fig.add_subplot(ax)
        fig.savefig('figures/GANsample_runs_and_fidelities.pdf', format='pdf')
        fig.show()


if __name__ == '__main__':
    gan = GAN(control_dims=(30, 1), disc_model_file="../NN/models/test_model")
    try:
        gan.train(epochs=2000, batch_size=64, sample_interval=200)
    except KeyboardInterrupt as e:
        print(sys.exc_info())
    gan.finalize()
    print("Mean fdelities: ", gan.mean_fid_array)
