
"""
Author: Andreas Springborg
Date: 2019-07-02
Goal:

"""
from pulseregression.framework.models.models import build_mlp, build_cnn, build_lstm, build_gru, build_gaussian_process, \
    build_xgboost, build_ada_boost, build_svr_rbf, build_knn, build_random_forest, build_baseline_model
from pulseregression.framework.utility import add_dimension, crop


def select_model(i):
    if i == 0:
        # Tiny MLP
        def model_fct(input_dim):
            return build_mlp(input_dim, 64, 4, "Basic_MLP", with_dropout=True)
    elif i == 1:
        # Deep MLP
        def model_fct(input_dim):
            return build_mlp(input_dim, 128, 12, "Deep_MLP", with_dropout=True)
    elif i == 2:
        # Shallow CNN
        def model_fct(input_dim):
            return build_cnn(input_dim, 1, 100, "Shallow_CNN")
    elif i == 3:
        # Deep CNN
        def model_fct(input_dim):
            return build_cnn(input_dim, 3, 160, "Deep_CNN", n_dense_layers=3, with_dropout=True)
    elif i == 4:
        # Shallow LSTM
        def model_fct(input_dim):
            return build_lstm(input_dim, 1, 100, "Shallow_LSTM")
    elif i == 5:
        # Deep LSTM
        def model_fct(input_dim):
            return build_lstm(input_dim, 3, 100, "Deep_LSTM", n_dense_layers=3)
    elif i == 6:
        # Shallow GRU
        def model_fct(input_dim):
            return build_gru(input_dim, 1, 100, "Shallow_GRU")
    elif i == 7:
        # Deep GRU
        def model_fct(input_dim):
            return build_gru(input_dim, 3, 100, "Deep_GRU", n_dense_layers=3)
    elif i == 8:
        # Gaussian Process (RBF kernel)
        def model_fct(input_dim):
            return build_gaussian_process(kernel="rbf", input_dim=input_dim)
    elif i == 9:
        # Gaussian Process (Rational Quadratic kernel)
        def model_fct(input_dim):
            return build_gaussian_process(kernel="rational", input_dim=input_dim)
    elif i == 10:
        # XG_BOOST Regression
        def model_fct(input_dim):
            return build_xgboost("XGB_Regressor")
    elif i == 11:
        # ADA_BOOST Regression
        def model_fct(input_dim):
            return build_ada_boost("ADAB_Regressor")
    elif i == 12:
        # Support Vector Regression (RBF kernel)
        def model_fct(input_dim):
            return build_svr_rbf("SVR_RBF_Regressor")
    elif i == 13:
        # KNN Regression
        def model_fct(input_dim):
            return build_knn("KNN_Regressor")
    elif i == 14:
        # Random Forest Regression
        def model_fct(input_dim):
            return build_random_forest("RandomForest_Regressor")
    elif i == 15:
        # A random baseline
        def model_fct(input_dim):
            return build_baseline_model("Random_Baseline")
    else:
        raise NotImplementedError("No model for i={}".format(i))
    if i in [2, 3, 4, 5, 6, 7]:
        wrapper = add_dimension
    elif i in [8, 9]:
        def crop_fixed(data):
            ctrls, fids = data[:2]
            ctrls, fids = crop(ctrls, fids, 4000)
            return tuple([ctrls, fids, *data[2:]])
        wrapper = crop_fixed
    else:
        wrapper = lambda x: x
    return model_fct, wrapper
