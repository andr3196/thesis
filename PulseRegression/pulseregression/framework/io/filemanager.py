"""
Author: Andreas Springborg
Date: 29/11/2018
Goal:

"""
from pathlib import Path
import re


class LocalFileManager:
    data_root = Path.home() / "../../Volumes/Transcend/Thesis"

    def __init__(self, path_chapter: str = "pulseregression/",
                 path_section: str = "qsl",
                 data_folder_in: str = "runs",
                 data_folder_out: str = "collected/npz",
                 figures_folder_out: str = "figures",
                 models_folder_out: str = "models",
                 data_subfolder: str = "data"
                 ):
        self.data_subfolder = data_subfolder
        self._models_folder_out = models_folder_out
        self._figures_folder_out = figures_folder_out
        self._data_path_chapter = path_chapter
        self._data_path_section = path_section
        self._data_folder_in = data_folder_in
        self._base_path = self.data_root / self._data_path_chapter / self._data_path_section
        self._data_folder_out = data_folder_out

    def get_full_model_path(self, folder:str = ""):
        return self._base_path / self._models_folder_out / folder

    def get_full_figures_path(self, folder:str =""):
        return self._base_path / self._figures_folder_out / folder

    def get_full_data_path(self, folder: str):
        return self._base_path / self._data_folder_in / folder / self.data_subfolder

    def get_all_files_in_folder(self, folder):
        return list(self.get_full_data_path(folder).glob("*.npz"))

    @staticmethod
    def filter_files(files, tag):
        pattern = re.compile('.*{}\D.*'.format(tag))
        return [file for file in files if pattern.match(file.name)]

    def get_npz_files(self, folders, tag=None):
        all_files = []
        for folder in folders:
            all_files.extend(self.get_all_files_in_folder(folder))
        if tag is not None:
            all_files = self.filter_files(all_files, tag)
        return all_files

    def get_collected_filepath(self):
        return self._base_path / self._data_folder_out

    @staticmethod
    def get_tag_for_value(**kwargs):
        tag = ""
        for (key, val) in kwargs.items():
            tag += "{}_{}".format(key, val)
            tag += "_"
        return tag[:-1]


if __name__ == "__main__":
    gfm = LocalFileManager(path_section="runcollection")
    n7files = gfm.get_npz_files(["run2.2"], "n_7")
