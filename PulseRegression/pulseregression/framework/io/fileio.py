import numpy as np
from typing import Tuple
from pathlib import Path
import os
import re
from pulseregression.framework.io.filemanager import LocalFileManager


def dump_run(filename: str, controls: np.ndarray, fidelities: np.ndarray, folder=".", n_controls=None) -> str:
    """ Save controls and fidelities and return  the filename of the created file
    "0" is appended to the name if no other number
        If there is already a file in the folder by the same name, a number is appended to the filename
    """
    if n_controls is None:
        run_desc = ""
    else:
        run_desc = "n" + str(n_controls)

    filename = get_next_available_filename(filename, folder)

    if not folder == ".":
        filename = folder + "/" + filename

    np.savez(filename + run_desc, controls=controls, fidelities=fidelities)
    return filename + run_desc


def get_next_available_filename(file_base, folder=".") -> str:
    file_list = os.listdir(folder)
    r = re.compile(file_base + "[0-9]*")
    run_files = []
    for file in file_list:
        match = r.match(file)
        if match:
            run_files.append(match.group(0))
    if not run_files:
        return file_base + "0"
    else:
        run_numbers = [int(name[len(file_base):]) for name in run_files]
        max_i = max(run_numbers)
        return file_base + str(max_i + 1)


def retrieve_run(filename: str, folder=None) -> Tuple[np.ndarray, np.ndarray]:
    """ Load controls and fidelities from a .npz file from a folder """
    if folder is not None:
        filename = folder + "/" + filename
    npzfile = np.load(filename + ".npz")
    return npzfile['controls'], npzfile['fidelities']


def retrieve_file(filename: str, folder=None) -> object:
    """ Load npz file and return NPZObject """
    if folder is not None:
        filename = folder + "/" + filename
    return np.load(filename + ".npz")


def dump_file(filename: str, data: dict, folder=".", n_controls=None) -> str:
    """ Save controls and fidelities and return  the filename of the created file"""
    if n_controls is None:
        run_desc = ""
    else:
        run_desc = "n" + str(n_controls)

    if not os.path.isdir(folder):
        os.mkdir(folder)

    filename = get_next_available_filename(filename, folder)

    fullfile = Path(folder) / (filename + run_desc)

    np.savez(fullfile, **data)
    return fullfile.with_suffix(".npz")


def save_log(filename: str, content: str, folder=".", tag="") -> None:
    """
    Save a JSON file containing the log
    :param filename:
    :param content: JSON string
    :param folder:
    :param tag: log file tag
    """
    file = Path(folder) / (filename + tag + ".json")

    with open(file, "w+") as meta_file:
        meta_file.write(content)


def load_data(data_tag):
    gfm = LocalFileManager(path_section="runcollection", data_folder_in="collected", data_subfolder="")
    files = gfm.get_npz_files(["npz"], data_tag)
    if not files:
        raise ValueError("No file matched the given path and tag")
    file = np.load(files[0])
    ctrls = file["ctrls"]
    fids = file["fids"]
    return ctrls, fids


def path_from_to(from_path, to_path):
    pass
