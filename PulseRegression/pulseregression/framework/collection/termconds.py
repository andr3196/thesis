
"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
import numpy as np
from qutip.control.termcond import TerminationConditions


class DynamicTerminationConditions():

    def __init__(self, config):
        self.tc = TerminationConditions()
        self.tc.fid_err_targ = config.fid_err_target
        self.tc.max_iterations = 0
        self.tc.max_wall_time = config.max_wall_time
        self.tc.min_gradient_norm = config.min_grad

    def update_max_iter(self, delta_max):
        if self.tc.max_iterations + delta_max >= 0:
            self.tc.max_iterations += delta_max

    def set_max_iter(self, new_max):
        if new_max >= 0:
            self.tc.max_iterations = new_max

    def get_term_conds(self):
        return self.tc

