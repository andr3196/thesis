"""
Author: Andreas Springborg
Date: 06/12/2018
Goal: These classes define strategies for mapping the target batch tsne fidelity to the number of GRAPE iterations

"""
from abc import ABC, abstractmethod
import numpy as np
import scipy.interpolate as interp
import scipy.optimize as opt



class Strategy(ABC):

    @abstractmethod
    def get_mapping_function(self, collector):
        pass

    def get_optimal_n_GRAPE_it(self, collector: 'DataCollector', fidelities):
        if collector.collect_it == 1:
            collector.sigma = np.mean([collector.sigma, np.std(fidelities)])
            #collector.accessibility = collector.calculate_accessibility()
            return collector._guess_n()
        else:

            mu_des = self.get_desired_mean(collector)
            f = self.get_mapping_function(collector)
            ni_it_guess = f(mu_des)
            n_it = np.rint(ni_it_guess)
            n_it = np.clip(n_it, a_min=0, a_max=collector.MAX_ITER)
            return int(n_it)

    @staticmethod
    def get_fmu_from_bin_index(i):
        return 0.05 * i + 0.025

    @staticmethod
    def get_desired_mean(collector):
        L = np.floor(collector._calculate_loss())
        L = collector.apply_historic_loss(L)
        collector.accessibility = collector.calculate_accessibility()
        total_loss = L + collector.n_in_each_bin + collector.accessibility
        i_opt = np.argmin(total_loss)
        collector.current_i = i_opt
        mu_des = Strategy.get_fmu_from_bin_index(i_opt)
        return mu_des


class InterpolatingStrategy(Strategy):

    def get_mapping_function(self, collector):
        if collector.batch_history.shape[1] > 2:
            f = interp.PchipInterpolator(collector.batch_history[0, :], collector.batch_history[1, :],
                                         extrapolate=True)
        else:
            f = interp.interp1d(collector.batch_history[0, :],
                                collector.batch_history[1, :],
                                kind=collector.get_interp_type(),
                                fill_value="extrapolate"
                                )
        return f


class SigmoidFitStrategy(Strategy):

    """
    Assumes that the tsne fidelity is given as a generalized sigmoid function of the number of iterations.
    Uses root finding to invert this relationship.
    """

    gamma = 0.005  # The percentage of the fitted maximum tsne after which the sigmoid is essentially flat

    @staticmethod
    def sigmoid(x, *params):
        return params[0] / (1 + params[1]*np.exp(- params[2] * x)) + params[3]

    @staticmethod
    def get_p0():
        # a, b, c = params 0 - 2
        return [2, 1, 0.3, -1]

    @staticmethod
    def get_default_n(popt):
        # The desired mu is unreachable
        # Select the effective maximum
        k = SigmoidFitStrategy.gamma * (1 + popt[2] / popt[0])
        return int(np.rint(1 / popt[1] * np.log((1 - k) / k)))

    # noinspection PyTypeChecker
    def get_mapping_function(self, collector):
        # fit history with sigmoid
        print(collector.batch_history)
        popt, _ = opt.curve_fit(self.sigmoid,
                                collector.batch_history[1, :],
                                collector.batch_history[0, :],
                                self.get_p0(), bounds=([0, 0, 0, -5], 5))

        collector.update_projected_max(popt)

        # Define a function = 0, when x (n_it) yields the desired n_it
        def root_func(x, mu_des):
            return self.sigmoid(x, *popt) - mu_des

        def mapping(mu_des):
            try:
                return opt.brentq(root_func, 0, collector.MAX_ITER, args=(mu_des,))
            except ValueError:
                return self.get_default_n(popt)

        return mapping
