"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
from pathlib import Path

from pulseregression.framework.collection.strategies import InterpolatingStrategy, SigmoidFitStrategy
from pulseregression.framework.qsl import lookup_qsl
from pulseregression.framework.quantum import init_problem
from pulseregression.framework.utility import make_duration_range, TimeSpec


class CollectionConfiguration:

    def __init__(self, n: int,
                 p: int,
                 b: float,
                 batch_size: int,
                 dt: float,
                 time_scale: float,
                 time_range_low: float,
                 time_range_high:float,
                 bin_max: int,
                 niter_selection_strategy: str,
                 n_ctrls: int = 1,
                 fid_err_target: float = 1e-3,
                 max_wall_time: int = 60,
                 min_grad: float = 1e-22,
                 min_progress: int = 20,
                 pulse_type: str = 'RND',
                 save_folder: str = '.',
                 data_file_base: str = 'run_',
                 seed_offset: int = 0,
                 lives: int = 3,
                 MAX_ITER=200,
                 should_run_parallel: bool = True):

        self.should_run_parallel = should_run_parallel
        self.niter_selection_strategy = self.get_iteration_strategy(niter_selection_strategy)
        self.MAX_ITER = MAX_ITER
        self.lives = lives
        self.min_grad = min_grad
        self.max_wall_time = max_wall_time
        self.fid_err_target = fid_err_target
        self.n_ctrls = n_ctrls
        self.min_progress = min_progress
        self.bin_max = bin_max
        #self.t_after = t_after
        #self.t_before = t_before
        self.dt = dt
        self.batch_size = batch_size
        self.pulse_type = pulse_type
        self.data_file_base = data_file_base
        self.save_folder = save_folder
        self.matrix_size = n
        self.problem_index = p
        self.ctrl_ampl = b
        self.seed_offset = seed_offset
        self.T_QSL = lookup_qsl(n, p, b)
        self.T_range = make_duration_range(self.T_QSL, time_scale, time_range_low, time_range_high)
        self.H_d, self.H_c, self.U_0, self.U_targ = init_problem(n=n,p=p, b=b)

    def time_spec_range(self):
        for t in self.T_range:
            yield TimeSpec(end=t, step_size=self.dt)

    def get_save_path(self):
        return Path(self.save_folder) / (self.data_file_base + self.get_tag_for_value(n=self.matrix_size,
                                                                                      p=self.problem_index))

    @staticmethod
    def get_tag_for_value(**kwargs):
        tag = ""
        for (key, val) in kwargs.items():
            tag += "{}_{}".format(key, val)
            tag += "_"
        return tag[:-1]

    @staticmethod
    def get_iteration_strategy(label: str):
        if label == "interpolating":
            return InterpolatingStrategy()
        if label == "sigmoidfit":
            return SigmoidFitStrategy()
        else:
            raise ValueError("Cannot interpret {} as a strategy.".format(label))
