
"""
Author: Andreas Springborg
Date: 2019-05-08
Goal: Define a noise detection algorithm

"""
import numpy as np
from scipy.signal import find_peaks


def detect_noise(signal, min_peak_dist=10):
    """
    This algorithm returns a mask of the signal of the indices that are deemed "noisy"
    :param signal:
    :param min_peak_dist:
    :return:
    """

    # Get spectrum of fluctuations
    d_sig = np.abs(np.diff(signal))
    p_indices, opts = find_peaks(d_sig, width=(0, 100))

    # Determine edges of noisy areas
    lefts = opts['left_bases'] - 1
    lefts[lefts < 0] = 0
    rights = opts['right_bases'] + 1
    rights[rights >= len(signal)] = len(signal) - 1

    # Detemine distances between noise peaks
    p_dists = np.diff(p_indices)

    # Get closely spaced peaks
    p_filt = p_dists < min_peak_dist
    p_filt = np.pad(p_filt, 1, 'constant', constant_values=False)
    d_filt = np.diff(p_filt)

    # Get indices of the bounds of the noisy regions
    peak_bound_indices = np.flatnonzero(d_filt)
    bound_indices = np.zeros(peak_bound_indices.shape)
    bound_indices[0::2] = lefts[peak_bound_indices[0::2]]
    bound_indices[1::2] = rights[peak_bound_indices[1::2]]
    bound_indices = bound_indices.astype(int)

    # Form mask
    is_noise = np.zeros(signal.shape) == 1
    for i in range(0, len(bound_indices), 2):
        is_noise[bound_indices[i]:bound_indices[i+1]] = True
    return is_noise