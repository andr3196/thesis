"""
Author: Andreas Springborg
Date: 14/02/2019
Goal: Holds function to define quantum problems

"""
from typing import Union

import numpy as np
from qutip import rand_herm, identity, rand_unitary, rand_ket, tensor, sigmaz, sigmax


def init_random_boundary_states(metric_type: str, matrix_size: int):
    """ Generate initial and target states/unitaries"""
    if metric_type == "sts":
        U_0 = rand_ket(matrix_size)
        U_targ = rand_ket(matrix_size)
    elif metric_type == "unitary":
        U_0 = identity(matrix_size)
        U_targ = rand_unitary(matrix_size)
    else:
        raise ValueError("Unknown metric type, {}".format(metric_type))
    return U_0, U_targ


def init_dynamics(seed_n: int, ctrl_ampl: float, matrix_size: int):
    """ Generate the drift and control matrices making up the problem """
    # set seed
    np.random.seed(seed_n)
    # init problem
    H_d = rand_herm(matrix_size)
    H_c = ctrl_ampl * rand_herm(matrix_size)
    return H_d, H_c


def init_problem(n, p, b, keep_random_state=False):
    H_d, H_c = init_dynamics(p, b, n)
    U_0, U_targ = init_random_boundary_states("unitary", n)
    if not keep_random_state:
        np.random.seed()
    return H_d, H_c, U_0, U_targ


def init_problem_as_vec(n, p, b, keep_random_state=False):
    H_d, H_c, _, U_targ = init_problem(n, p, b, keep_random_state=keep_random_state)
    return problem_to_vec(H_d, H_c, U_targ)


def problem_to_vec(H_d, H_c, U_targ):
    return np.concatenate([herm_to_vec(H_d.full()), herm_to_vec(H_c.full()), unit_to_vec(U_targ.full())])


def split_real_imag(v):
    """ Split a vector in real and imaginary parts """
    out = np.zeros((2 * len(v),))
    out[::2] = v.real
    out[1::2] = v.imag
    return out


def herm_to_vec(herm_M):
    """ Turn Hermitian n-by-n matrix into vector of real define"""
    iu = np.triu_indices_from(herm_M)

    return split_real_imag(herm_M[iu])


def unit_to_vec(unit_M):
    """ """
    return split_real_imag(unit_M.flatten())


def init_boundary_eigenstates(H_d_init, H_d_final, H_c, init_state_control_ampl, targ_state_control_ampl):
    H_init = H_d_init + init_state_control_ampl * H_c
    H_targ = H_d_final + targ_state_control_ampl * H_c
    _, psi_0 = H_init.groundstate()
    _, psi_targ = H_targ.groundstate()
    return psi_0, psi_targ


def to_array(L, *args):
    new_args = []
    for i, a in enumerate(args):
        if isinstance(a, float) or isinstance(a, int):
            new_args.append(a * np.ones((L,)))
        elif isinstance(a, list):
            new_args.append(np.array(a))
        elif isinstance(a, np.ndarray):
            new_args.append(a)
        else:
            raise ValueError("Cannot convert a of type {} to an ndarray".format(type(a)))
    if len(new_args) == 1:
        return new_args[0]
    return new_args


def generate_random_couplings(L, J0, alpha, p, noise_mode, noise_args=None):
    np.random.seed(p)
    if noise_mode is None:
        J_ii1 = 0
    elif noise_mode == "uniform":
        if noise_args is None or 'bounds' not in noise_args.keys():
            raise ValueError("Must specify 'bounds' for uniform noise mode")
        J_max, J_min = noise_args["bounds"]
        J_ii1 = (J_max - J_min) * np.random.random((L,)) + J_min
    elif noise_mode == "normal":
        if noise_args is None or 'sigma' not in noise_args.keys():
            raise ValueError("Must specify 'sigma' for normal noise mode")
        sigma = noise_args['sigma']
        J_ii1 = np.random.normal(scale=sigma, size=L)
    else:
        raise ValueError("Unexpected noise mode: {}".format(noise_mode))
    np.random.seed()
    return J0 + alpha * J_ii1


def init_spinsystem_dynamics(L: int,
                             J: Union[float, list, np.ndarray] = 1.0,
                             g: float = 1.0,
                             J_glass: Union[float, list, np.ndarray] = None):
    J, g = to_array(L, J, g)

    # Translate Pauli operators to spin operators
    f1 = 1 / 2  # S_z = hbar/2*sigma_z
    J *= f1 ** 2
    g *= f1

    if J_glass is None:
        H_g_terms, H_c_terms, H_J_terms = init_spinsystem_terms(L)
    else:
        J_glass = to_array(L * (L - 1) // 2, J_glass)
        J_glass *= f1 ** 2

        H_g_terms, H_c_terms, H_J_terms, H_glass_terms = init_spinsystem_terms(L, cross_int=sigmaz())
        H_glass = sum([Ji * term for Ji, term in zip(J_glass, H_glass_terms)])
    H_g = sum([gi * term for gi, term in zip(g, H_g_terms)])  # On site
    H_c = f1 * sum(H_c_terms)  # Controls
    H_J = sum([Ji * term for Ji, term in zip(J, H_J_terms)])  # Chain coupling

    if J_glass is None:
        return H_g, H_J, H_c
    else:
        return H_g, H_J, H_c, H_glass


def init_spinsystem_terms(L: int,
                          on_site_int=sigmaz(),
                          ctrl_int=sigmax(),
                          chain_coupling_int=sigmaz(),
                          cross_int=None
                          ):
    H_g_terms = [tensor([on_site_int if m == n else identity(2)
                         for n in range(L)])
                 for m in range(L)]
    H_c_terms = [tensor([ctrl_int if m == n else identity(2)
                         for n in range(L)])
                 for m in range(L)]

    if not isinstance(chain_coupling_int, list):
        chain_coupling_int = [chain_coupling_int, chain_coupling_int]
    zz_int_terms = []
    for l in range(L):
        int_term = [identity(2)] * L
        int_term[l] = chain_coupling_int[0]
        int_term[(l + 1) % L] = chain_coupling_int[1]
        zz_int_terms.append(tensor(int_term))

    if cross_int is None:
        return H_g_terms, H_c_terms, zz_int_terms
    else:
        cross_int_terms = []
        if not isinstance(cross_int, list):
            cross_int = [cross_int, cross_int]
        for i in range(L):
            for j in range(i + 1, L):
                int_term = [identity(2)] * L
                int_term[i] = cross_int[0]
                int_term[j] = cross_int[1]
                cross_int_terms.append(tensor(int_term))
        return H_g_terms, H_c_terms, zz_int_terms, cross_int_terms
