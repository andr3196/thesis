"""
Author: Andreas Springborg
Date: 20/02/2019
Goal:

"""
import timeit
import qutip.logging_utils as logging
import numpy as np
import types

logger = logging.get_logger()
import scipy.optimize as spopt
import qutip.control.errors as errors


class Hessian(object):

    def __init__(self, x0, gf0):
        self.xk = x0.copy()
        self.gfk = gf0.copy()

        try:  # this was handled in numeric, let it remaines for more safety
            gammak = np.dot(gf0, gf0) / np.dot(gf0, x0)
        except ZeroDivisionError:
            gammak = 1.0
        if not np.isfinite(gammak):  # this is patch for numpy
            gammak = 1.0

        self.Hk = gammak * np.eye(len(x0))
        self.I = np.eye(len(x0))

    def update(self, xk1, gfk1):

        sk = xk1 - self.xk
        yk = gfk1 - self.gfk

        self.xk = xk1.copy()
        self.gfk = gfk1.copy()

        try:  # this was handled in numeric, let it remaines for more safety
            gammak = 1.0 / (np.dot(yk, sk))
        except ZeroDivisionError:
            gammak = 1000.0
        if np.isinf(gammak):  # this is patch for numpy
            gammak = 1000.0

        A1 = self.I - yk[:, np.newaxis] * sk[np.newaxis, :] * gammak
        A2 = self.I - sk[:, np.newaxis] * yk[np.newaxis, :] * gammak
        self.Hk = np.dot(A1, np.dot(self.Hk, A2)) + (gammak * yk[:, np.newaxis] *
                                                     yk[np.newaxis, :])


def wrap_fprime(hess, fprime):
    def my_fprime(self, *args):
        xk1 = args[0]
        gfk1 = fprime(*args)
        hess.update(xk1, gfk1)
        return gfk1

    return my_fprime


def run_optimization(self, term_conds=None):
    """
    Optimise the control pulse amplitudes to minimise the fidelity error
    using the L-BFGS-B algorithm, which is the constrained
    (bounded amplitude define), limited memory, version of the
    Broyden–Fletcher–Goldfarb–Shanno algorithm.

    The optimisation end when one of the passed termination conditions
    has been met, e.g. target achieved, gradient minimum met
    (local minima), wall time / iteration count exceeded.

    Essentially this is wrapper to the:
    scipy.optimize.fmin_l_bfgs_b function
    This in turn is a warpper for well established implementation of
    the L-BFGS-B algorithm written in Fortran, which is therefore
    very fast. See SciPy documentation for credit and details on
    this function.

    If the parameter term_conds=None, then the termination_conditions
    attribute must already be set. It will be overwritten if the
    parameter is not None

    The result is returned in an OptimResult object, which includes
    the final fidelity, time evolution, reason for termination etc

    """
    self.init_optim(term_conds)
    term_conds = self.termination_conditions
    dyn = self.dynamics
    cfg = self.config
    self.optim_var_vals = self._get_optim_var_vals()
    self._build_method_options()

    st_time = timeit.default_timer()
    self.wall_time_optimize_start = st_time

    if self.stats is not None:
        self.stats.wall_time_optim_start = st_time
        self.stats.wall_time_optim_end = 0.0
        self.stats.num_iter = 1

    bounds = self._build_bounds_list()
    result = self._create_result()

    if self.approx_grad:
        fprime = None
    else:
        fprime = self.fid_err_grad_wrapper

    if 'accuracy_factor' in self.method_options:
        factr = self.method_options['accuracy_factor']
    elif 'ftol' in self.method_options:
        factr = self.method_options['ftol']
    elif hasattr(term_conds, 'accuracy_factor'):
        factr = term_conds.accuracy_factor
    else:
        factr = 1e7

    if 'max_metric_corr' in self.method_options:
        m = self.method_options['max_metric_corr']
    elif 'maxcor' in self.method_options:
        m = self.method_options['maxcor']
    elif hasattr(self, 'max_metric_corr'):
        m = self.max_metric_corr
    else:
        m = 10

    if self.log_level <= logging.INFO:
        msg = ("Optimising pulse(s) using {} with "
               "'fmin_l_bfgs_b' method").format(self.alg)
        if self.approx_grad:
            msg += " (approx grad)"
        logger.info(msg)

    # Prepare for tracking Hessian

    x0 = self.optim_var_vals
    try:
        gf0 = fprime(x0)
    except errors.GradMinReachedTerminate:
        gf0 = np.zeros(x0.shape)

    self.hessian = Hessian(x0, gf0)

    fprime = types.MethodType(wrap_fprime(self.hessian, fprime), self)

    try:
        optim_var_vals, fid, res_dict = spopt.fmin_l_bfgs_b(
            self.fid_err_func_wrapper, self.optim_var_vals,
            fprime=fprime,
            approx_grad=self.approx_grad,
            callback=self.iter_step_callback_func,
            bounds=self.bounds, m=m, factr=factr,
            pgtol=term_conds.min_gradient_norm,
            disp=self.msg_level,
            maxfun=term_conds.max_fid_func_calls,
            maxiter=term_conds.max_iterations)

        amps = self._get_ctrl_amps(optim_var_vals)
        dyn.update_ctrl_amps(amps)
        warn = res_dict['warnflag']
        if warn == 0:
            result.grad_norm_min_reached = True
            result.termination_reason = "function converged"
        elif warn == 1:
            result.max_iter_exceeded = True
            result.termination_reason = ("Iteration or fidelity "
                                         "function call limit reached")
        elif warn == 2:
            result.termination_reason = res_dict['task']

        result.num_iter = res_dict['nit']
    except errors.OptimizationTerminate as except_term:
        self._interpret_term_exception(except_term, result)

    end_time = timeit.default_timer()
    self._add_common_result_attribs(result, st_time, end_time)

    return result


def set_save_hessian(optim):
    setattr(optim, 'run_optimization', run_optimization)
    return optim
