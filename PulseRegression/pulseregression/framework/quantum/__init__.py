"""
Author: Andreas Springborg
Date: 2019-04-05
Goal:

"""
from .quantum import *


def init_spinchain_problem(L: int,
                           J: Union[float, list, str] = 1.0,
                           g: float = 1.0,
                           init_state_control_ampl: float = -2.0,
                           targ_state_control_ampl: float = 2.0
                           ):
    H_g, H_int, H_c = init_spinsystem_dynamics(L, J, g)
    H_c = - H_c
    H_d = - H_g - H_int
    psi_0, psi_targ = init_boundary_eigenstates(H_d, H_d, H_c, init_state_control_ampl, targ_state_control_ampl)
    return H_d, H_c, psi_0, psi_targ


def init_rand_spinchain_problem(L: int,
                                J0: Union[float, list, str] = 1.0,
                                g: float = 1.0,
                                alpha: float = 0.0,
                                noise_mode: str = None,
                                noise_args=None,
                                init_state_control_ampl: float = -2.0,
                                targ_state_control_ampl: float = 2.0,
                                p=None
                                ):
    Js = generate_random_couplings(L, J0, alpha, p, noise_mode, noise_args)
    H_g, H_int, H_c = init_spinsystem_dynamics(L, Js, g)
    H_c = - H_c
    H_d = - H_g - H_int
    psi_0, psi_targ = init_boundary_eigenstates(H_d, H_d, H_c, init_state_control_ampl, targ_state_control_ampl)

    return H_d, H_c, psi_0, psi_targ


def init_spinglass_problem(L: int,
                           J0: Union[float, list, str] = 1.0,
                           g: float = 1.0,
                           alpha: float = 0.0,
                           noise_mode: str = None,
                           noise_args=None,
                           init_state_control_ampl: float = -2.0,
                           targ_state_control_ampl: float = 2.0,
                           p=None
                           ):
    # init chain couplings
    J_chain = generate_random_couplings(L, J0, 0.0, p, noise_mode, noise_args)
    J_glass = generate_random_couplings(L * (L - 1) // 2, 0.0, 1.0, p, noise_mode, noise_args)

    H_g, H_chain, H_c, H_glass = init_spinsystem_dynamics(L, J_chain, g, J_glass=J_glass)
    H_c = - H_c
    H_d = - H_g - H_chain
    psi_0, psi_targ = init_boundary_eigenstates(H_d, H_d, H_c, init_state_control_ampl, targ_state_control_ampl)
    H_d = - H_g - (1 - alpha) * H_chain - alpha * H_glass
    return H_d, H_c, psi_0, psi_targ
