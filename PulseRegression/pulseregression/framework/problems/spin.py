"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""
import itertools

import numpy as np
from pathlib import Path
# duration range
from pulseregression.framework.optim.optimization import get_optimizer_for_problem
from pulseregression.framework.procedure import get_filenames
from pulseregression.framework.quantum import init_spinchain_problem, init_rand_spinchain_problem, \
    init_spinglass_problem
from pulseregression.framework.problems.core import QuantumProblem


class SpinChainDuration(object):

    def __init__(self, n_T=200, n_ts=200, T_max=4.0, T_offset=None):
        self.n_T = n_T
        self.n_ts = n_ts
        self.T_max = T_max
        self.dt = T_max / n_ts
        self.T_step = T_max / n_T
        if T_offset is None:
            self.T_offset = self.T_step
        else:
            self.T_offset = T_offset
        self.duration_range = self.T_step * np.arange(n_T) + self.dt


class SparseSamplingSpinChainDuration(object):

    def __init__(self, n_T=10, n_ts=200, T_max=4.0, n_samples=None, T_offset=None):
        if n_samples is None:
            n_samples = n_T
        self.n_T = n_T
        self.n_ts = n_ts
        self.T_max = T_max
        self.dt = T_max / n_ts
        self.T_step = T_max / n_T
        if T_offset is None:
            self.T_offset = self.T_step
        else:
            self.T_offset = T_offset
        self.duration_range = self.T_step * np.arange(n_samples) + self.T_offset


class SpinProblem(object):

    def __init__(self, L=6, J=1, g=1, max_ampl=4.0, n_seeds=100, filebase=None, spinchainduration=None):

        if spinchainduration is None:
            spinchainduration = SpinChainDuration()
        self.L = L
        self.J = J
        self.g = g
        self.max_ampl = max_ampl
        self.n_seeds = n_seeds
        self.transfer_all_fields(spinchainduration)
        self.transfer_all_fields(QuantumProblem())
        self._loadpath = None
        self._savepath = None
        self._ownloadpath = None
        self.problem = None
        self.filebase = filebase

    def bounds(self):
        return -self.max_ampl, self.max_ampl

    def range_width(self):
        return 2 * self.max_ampl

    def define(self):
        return [b for b in vars(self).values()]

    def define_names(self):
        return [b for b in vars(self).keys()]

    def transfer_all_fields(self, obj):

        for k, v, in vars(obj).items():
            setattr(self, k, v)

    def load_path(self):
        return self._loadpath

    def save_path(self, sub_path=None):
        if sub_path is not None:
            return self._savepath / sub_path
        return self._savepath

    def generate_optim(self, T_sample):
        return get_optimizer_for_problem(self.problem, T_sample, n_ts=self.n_ts, amp_max=self.max_ampl)

    def get_load_files(self, source=None, **kwargs):
        if source is None:
            s = self._loadpath
        elif source == "own":
            s = self._ownloadpath
        elif isinstance(source, Path):
            s = source
        else:
            raise NotImplementedError("Unknown source: {}".format(source))
        return get_filenames(s, base=self.filebase, L=self.L, J=self.J, g=self.g, **kwargs)

    def get_T(self, Ti):
        return self.T_step * Ti + self.T_offset


class SpinChainProblem(SpinProblem):

    def __init__(self, L=6, J=1, g=1):
        super().__init__(L, J, g, filebase="spinchain")
        self.problem = init_spinchain_problem(L, J, g)
        self._loadpath = Path("../../../../data/distance/runs/run6/data")
        self._ownloadpath = Path("../../../data/distance/runs/run6/data")
        self._savepath = Path("../../../../data/distance/collected/run6/")

    def load_path(self):
        return self._loadpath

    def save_path(self, sub_path=None):
        if sub_path is not None:
            return self._savepath / sub_path
        return self._savepath

    def generate_optim(self, T_sample):
        return get_optimizer_for_problem(self.problem, T_sample, n_ts=self.n_ts, amp_max=self.max_ampl)


class RandomSpinChainRealisation(SpinProblem):

    def __init__(self, alpha, p, L=6, J=1, g=1, J_rand_bounds=(-1.0, 1.0), noise_mode=None, noise_args=None):
        super().__init__(L, J, g, filebase="randspinchain",
                         spinchainduration=SparseSamplingSpinChainDuration(n_samples=16))
        if noise_mode is None:
            noise_mode = "uniform"
            noise_args = {"bounds": J_rand_bounds}
        self.noise_mode = noise_mode
        self.noise_args = noise_args
        self._alpha = alpha
        self.p = p
        self._loadpath = Path("../../../../data/distance/runs/run7/data")
        self._ownloadpath = Path("../../../data/distance/runs/run7/data")
        self._savepath = Path("../../../../data/distance/collected/run7/")
        self.J_rand_bounds = J_rand_bounds

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, val):
        self._alpha = val
        self.problem = init_rand_spinchain_problem(self.L, self.J, self.g, self._alpha, self.noise_mode,
                                                   self.noise_args, p=self._p)

    @property
    def p(self):
        return self._p

    @p.setter
    def p(self, val):
        self._p = val
        self.problem = init_rand_spinchain_problem(self.L, self.J, self.g, self._alpha, self.noise_mode,
                                                   self.noise_args, p=self._p)

    def get_load_files(self, **kwargs):
        return get_filenames(self._loadpath, base=self.filebase, L=self.L, J0=self.J, g=self.g,
                             a=[self._alpha, ":01.1f"], p=self.p, **kwargs)

    def problem_fct(self):
        return lambda p: init_rand_spinchain_problem(self.L, self.J, self.g, self._alpha, noise_mode=self.noise_mode,
                                                     noise_args=self.noise_args, p=p)


class RandomSpinChainProblem(SpinProblem):

    def __init__(self, alpha, p_range=None, L=6, J=1, g=1, J_rand_bounds=(-1.0, 1.0), noise_mode=None, noise_args=None):
        super().__init__(L, J, g, filebase="randspinchain",
                         spinchainduration=SparseSamplingSpinChainDuration(n_samples=16))
        if noise_mode is None:
            noise_mode = "uniform"
            noise_args = {"bounds": J_rand_bounds}
        self.noise_mode = noise_mode
        self.noise_args = noise_args
        self._alpha = alpha
        self.p_range = p_range
        self._loadpath = Path("../../../../data/distance/runs/run7/data")
        self._ownloadpath = Path("../../../data/distance/runs/run7/data")
        self._savepath = Path("../../../../data/distance/collected/run7/")
        self.J_rand_bounds = J_rand_bounds

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, val):
        self._alpha = val

    def problem(self, p):
        return init_rand_spinchain_problem(self.L, self.J, self.g, self._alpha, self.noise_mode,
                                                   self.noise_args, p=p)

    def get_load_files(self, **kwargs):
        return get_filenames(self._loadpath, base=self.filebase, L=self.L, J0=self.J, g=self.g,
                             a=[self._alpha, ":01.1f"], **kwargs)

    def problem_fct(self):
        return lambda p: init_rand_spinchain_problem(self.L, self.J, self.g, self._alpha, noise_mode=self.noise_mode,
                                                     noise_args=self.noise_args, p=p)


class RandomSpinChainProblemScan:

    def __init__(self, alpha_range):
        self.alpha_range = alpha_range
        self.n_alpha = len(alpha_range)

    def __getitem__(self, alpha):
        return RandomSpinChainProblem(alpha)

    def __iter__(self):
        self.iterparams = self.alpha_range.copy().tolist()
        return self

    def __next__(self) -> RandomSpinChainProblem:
        if self.iterparams:
            return self[self.iterparams.pop(0)]
        else:
            raise StopIteration


class SpinGlassRealisation(SpinProblem):

    def __init__(self, alpha, p, L=6, J=1, g=1, J_rand_bounds=(0, 1.0), noise_mode=None, noise_args=None):
        super().__init__(L, J, g, filebase="spinglass",
                         spinchainduration=SparseSamplingSpinChainDuration(n_samples=18))
        if noise_mode is None:
            noise_mode = "uniform"
            noise_args = {"bounds": J_rand_bounds}
        self.noise_mode = noise_mode
        self.noise_args = noise_args
        self._alpha = alpha
        self.p = p
        self._loadpath = Path("../../../../data/distance/runs/run5/data")
        self._ownloadpath = Path("../../../data/distance/runs/run5/data")
        self._savepath = Path("../../../../data/distance/collected/run5/")

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, val):
        self._alpha = val
        self.problem = init_spinglass_problem(self.L, self.J, self.g, self._alpha,
                                              self.noise_mode, self.noise_args, p=self.p)

    @property
    def p(self):
        return self._p

    @p.setter
    def p(self, val):
        self._p = val
        self.problem = init_spinglass_problem(self.L, self.J, self.g, self._alpha,
                                              self.noise_mode, self.noise_args, p=self.p)

    def get_load_files(self, **kwargs):
        return get_filenames(self._loadpath, base=self.filebase, L=self.L, J=self.J, g=self.g, p=self.p, **kwargs)


class SpinGlassProblem(SpinProblem):

    def __init__(self, alpha, p_range=None, L=6, J=1, g=1, J_rand_bounds=(0, 1.0), noise_mode=None, noise_args=None):
        super().__init__(L, J, g, filebase="spinglass",
                         spinchainduration=SparseSamplingSpinChainDuration(n_samples=18))
        if noise_mode is None:
            noise_mode = "uniform"
            noise_args = {"bounds": J_rand_bounds}
        self.noise_mode = noise_mode
        self.noise_args = noise_args
        self._alpha = alpha
        self.p_range = p_range
        self._loadpath = Path("../../../../data/distance/runs/run5/data")
        self._ownloadpath = Path("../../../data/distance/runs/run5/data")
        self._savepath = Path("../../../../data/distance/collected/run5/")

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, val):
        self._alpha = val

    def problem_fct(self):
       return lambda p: init_spinglass_problem(self.L, self.J, self.g, self._alpha,
                                              self.noise_mode, self.noise_args, p=p)

    def get_load_files(self, **kwargs):
        return get_filenames(self._loadpath, base=self.filebase, L=self.L, J=self.J, g=self.g, a=[self.alpha, ":01.1f"], **kwargs)


class SpinChainProblemScan:

    def __init__(self, alpha_range):
        self.alpha_range = alpha_range
        self.n_alpha = len(alpha_range)

    def __getitem__(self, alpha):
        return SpinGlassProblem(alpha)

    def __iter__(self):
        self.iterparams = self.alpha_range.copy().tolist()
        return self

    def __next__(self) -> SpinGlassProblem:
        if self.iterparams:
            return self[self.iterparams.pop(0)]
        else:
            raise StopIteration
