"""
Author: Andreas Springborg
Date: 2019-05-21
Goal:

"""
import warnings
from pathlib import Path

from pulseregression.framework.optim.optimization import get_optimizer_for_problem
from pulseregression.framework.procedure import get_filenames
from pulseregression.framework.qsl import lookup_qsl, UnknownQSLError
from pulseregression.framework.quantum import init_problem
from pulseregression.framework.utility import make_duration_range
from pulseregression.framework.problems.core import transfer_all_fields, QuantumProblem


class ScanProperties:

    def __init__(self, dt=0.1, T_step=None, T_offset=None):
        self.dt = dt
        if T_step is None:
            self.T_step = dt
        else:
            self.T_step = T_step
        if T_offset is None:
            self.T_offset = dt
        else:
            self.T_offset = T_offset


class RandomHamiltonianProblem():

    def __init__(self, n, p, b, scanproperties, n_seeds=100):

        self.n = n
        self.p = p
        self.b = b
        try:
            self.T_QSL = lookup_qsl(n, p, b)
            self.T_max = 1.6 * self.T_QSL
        except UnknownQSLError:
            warnings.warn("No known QSL for the selected problem - fields T_max and T_QSL will be none and certain "
                          "methods will cause errors")
            self.T_QSL = None
            self.T_max = None

        self.max_ampl = 1.0
        self.n_seeds = n_seeds
        transfer_all_fields(scanproperties, self)
        transfer_all_fields(QuantumProblem(), self)
        self._loadpath = Path("../../../../data/distance/runs/run2/data")
        self._savepath = Path("../../../../data/distance/collected/run2")
        self._ownloadpath = Path("../../../data/distance/runs/run2/data")
        self.problem = init_problem(n, p, b)
        self.filebase = "run"
        self.duration_range = make_duration_range(self.T_QSL, 0.0, 1.6, step=self.T_step)

    def bounds(self):
        return -self.max_ampl, self.max_ampl

    def range_width(self):
        return 2 * self.max_ampl

    def define(self):
        return [b for b in vars(self).values()]

    def define_names(self):
        return [b for b in vars(self).keys()]

    def load_path(self):
        return self._loadpath

    def save_path(self, sub_path=None):
        if sub_path is not None:
            return self._savepath / sub_path
        return self._savepath

    def generate_optim(self, T_sample):
        return get_optimizer_for_problem(self.problem, T_sample, dt=self.dt, amp_max=self.max_ampl)

    def get_load_files(self, source=None, **kwargs):
        if source is None:
            s = self._loadpath
        elif source == "own":
            s = self._ownloadpath
        elif isinstance(source, Path):
            s = source
        else:
            raise NotImplementedError("Unknown source: {}".format(source))
        return get_filenames(s, base=self.filebase, n=self.n, p=self.p, b=self.b, **kwargs)

    def get_T(self, Ti):
        return self.duration_range[Ti]

