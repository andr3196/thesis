
"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""
import numpy as np


def transfer_all_fields(from_obj, to_obj):
    for k, v, in vars(from_obj).items():
        setattr(to_obj, k, v)


class QuantumProblem(object):

    def __init__(self):
        self.fqsl = 0.99




