"""
Author: Andreas Springborg
Date: 2019-05-12
Goal:

"""
from pathlib import Path


def path_to_thesis_images():
    base = Path.home()
    return base / "Dropbox/AUni/Speciale/speciale/Resources/Images"


def save_figure(plt, figurename, dpi=None):
    if not figurename.endswith(".pdf"):
        figurename += ".pdf"
    plt.savefig(path_to_thesis_images() / figurename)
