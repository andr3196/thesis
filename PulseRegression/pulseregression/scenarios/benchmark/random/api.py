"""
Author: Andreas Springborg
Date: 28/02/2019
Goal:

"""
import sys
import warnings
from pathlib import Path

import numpy as np

from pulseregression.framework.models.modelconfigs import select_model
from pulseregression.framework.utility.error import ErrorCollector
from pulseregression.scenarios.benchmark.core import save_scan, run, prepare

""" Define """
data_filename_prefix = "modelscan_"
save_file_prefix = "scan_"
load_file_prefix = "coll_runs_"
load_path = Path("../../../../data/ml_training_data")
save_path = Path("../../../../data/model_errors")

n_values = np.arange(2, 8)
n_p = 10
p_values = np.arange(n_p)
b_values = [3.0]
fid_threshold = 0.8

dt = 0.1

max_num_datapoints = 100000
n_training_epochs = 50
batch_size = 128

training_pct = 0.8

new_data_input_path = Path("../../../../data/distance/runs/run1/data")
old_data_input_path = load_path
data_output_path = save_path

"""        """


def scan_models(model_func,
                data_wrapper,
                data_set_tags,
                max_num_datapoints,
                n_epochs,
                batch_size,
                new_data_path,
                old_data_path,
                data_save_path=None,
                field="ctrls",
                test_mode=False):

    if test_mode:
        warnings.warn("Running in test mode: Data is randomly generated and no data is loaded. Predictions will be "
                      "meaningless.")

    ec = ErrorCollector()
    for new_data_set_tag, old_data_set_tag in data_set_tags:
        # Initiate
        try:
            data = prepare(new_data_path, new_data_set_tag, old_data_path, old_data_set_tag, max_num_datapoints,
                           field=field, test_mode=test_mode)
        except IOError:
            print("Data file not found - skip!")
            continue
        input_dim = data[0].shape[1]
        print("Fitting data set:", new_data_set_tag)
        model = model_func(input_dim)

        savefile = data_save_path / (new_data_set_tag + "_" + model.name + ".npz")
        print(savefile)
        if savefile.exists():
            print("File already exists - skip!")
            continue
        else:
            print("Dataset not previously fitted")

        # Run the training sequence
        data = data_wrapper(data)
        loss_history, test_phase_data = run(data, model, n_epochs, batch_size)

        # Gather computed errors
        ec.collect(loss_history, test_phase_data)
        ec.print_last()

        save_scan(*ec.get_all_errors(), model.name, new_data_set_tag + "_", save_path=data_save_path)


def make_data_set_tag_pairs():
    """ Data (consisting of optimised and uniform data ) is stored in two places. We generate pairs of tags for a
    particular data set.

    :return:
    """
    tags = []
    for n in n_values:
        for p in p_values:
            for b in b_values:
                tags.append(("run_n_{}_p_{}_b_{}".format(n, p, b), "coll_runs_n_{}_p_{}".format(n, p)))

    return tags


if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Usage: api.py i, i not supplied")
    else:

        # On Grendel we need to map a single integer to different problems to run them in parallel. Here k is the
        # task index which we then map to  "i", the model to use, and "p" the particular realisations. One task loops
        # over all dimensions "n".
        k = int(args[1])
        i = k // n_p
        p = k % n_p

        data_set_tags = [x for x in make_data_set_tag_pairs() if "p_{}".format(p) in x[1]]

        # The model depends on the input dimension of the data, which in turn depends on the data. So rather than
        # building a model explicitly, we generate a function which builds a model when given the input dimension. The
        # input dimension is available after loading data.
        model_generator, wrapper = select_model(i)
        scan_models(model_generator,
                    wrapper,
                    data_set_tags,
                    max_num_datapoints,
                    n_training_epochs,
                    batch_size,
                    new_data_input_path,
                    old_data_input_path,
                    data_save_path=data_output_path,
                    field="ctrls",
                    test_mode=False)
