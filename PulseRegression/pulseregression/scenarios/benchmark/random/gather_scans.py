from pathlib import Path
import numpy as np
from scipy.io import savemat

""" Goal 
Running this file gathers the individual model scans to a single .mat file that can be loaded into MATLAB and plotted 
as in Figure 5.2


"""

""" Define """
load_path = Path("../../../../data/model_errors")
load_file_prefix = "scan_"
save_filename = "fitted_models_data.mat"
save_path = Path(".")

"""        """


def new_gather_scans():
    b = 3.0
    n_n = 6
    n_p = 10
    n_values = np.arange(2, 8)
    p_values = np.arange(n_p)
    model_names = ["Basic_MLP", "Deep_MLP", "Shallow_CNN", "Deep_CNN", "Shallow_LSTM", "Deep_LSTM", "Shallow_GRU",
                   "Deep_GRU", "GP_RBF", "GP_RQ", "XGB", "ADA", "SVR_RBF", "KNN", "Rand_For", "Random_Baseline"]
    fields = ["maes", "high_fid_maes", "min_maes"]

    data_out = {name: {field: np.zeros((n_p, n_n)) * np.nan for field in fields} for name in model_names}
    for model in model_names:
        for i, n in enumerate(n_values):
            for j, p in enumerate(p_values):
                tag = "run_n_{}_p_{}_b_{}_{name}".format(n, p, b, name=model)
                file = list(load_path.glob(tag + "*.npz"))
                print(file)
                if file:
                    f = np.load(file[0])
                    for field in fields:
                        data_out[model][field][j, i] = f[field][-1]
    fullfile = save_path / save_filename
    savemat(fullfile, data_out)


if __name__ == "__main__":
    new_gather_scans()
