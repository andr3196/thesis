"""
Author: Andreas Springborg
Date: 2019-07-02
Goal:

"""
import numpy as np
from hpsklearn import HyperoptEstimator
from sklearn.model_selection import train_test_split
from tensorflow.python import keras
from tensorflow.python.keras.callbacks import EarlyStopping
from pulseregression.framework.utility import stack, reshape, remove_empty_rows, shuffle, crop, fill_nan, \
    calc_control_length

# For testing mode
NT_TEST = 14
NTIMES_TEST = 80
N_SAMPLE_TEST = 50
###


def save_scan(test_maes, min_maes, high_fid_maes, model_name, data_filename_prefix, save_path=None, **kwargs):
    full_path = data_filename_prefix + model_name
    if save_path is not None:
        full_path = save_path / full_path
    np.savez(full_path, maes=test_maes, min_maes=min_maes, high_fid_maes=high_fid_maes, **kwargs)


def import_data(path_to_data_folder, data_tag, field='ctrls', test_mode=False):
    if test_mode:
        features = 2 * np.random.random((N_SAMPLE_TEST, NTIMES_TEST)) - 1
        fidelities = np.random.random((N_SAMPLE_TEST,))
        return features, fidelities

    file_gen = path_to_data_folder.glob(data_tag + "*.npz")

    features_list = []
    fidelities_list = []
    found_files = False
    for file in file_gen:
        found_files = True
        f = np.load(file)
        features_list.append(f[field])
        fidelities_list.append(f["fids"])

    if not found_files:
        raise IOError(
            "No files in the directory, {}, matched the specified tag: '{}'".format(path_to_data_folder, data_tag))

    return stack(features_list, fidelities_list)


def import_old_data(path_to_data_folder, data_tag, field='ctrls', test_mode=False):
    if test_mode:
        features = 2*np.random.random((N_SAMPLE_TEST, NTIMES_TEST, NT_TEST)) - 1
        fidelities = np.random.random((N_SAMPLE_TEST, NT_TEST))
        return features, fidelities

    file_gen = path_to_data_folder.glob(data_tag + "*.npz")
    files = list(file_gen)
    if not files:
        raise IOError(
            "No files in the directory, {}, matched the specified tag: '{}'".format(path_to_data_folder, data_tag))
    if not len(files) == 1:
        print("WARNING: Multiple files in forlder, {}, matched the specified tag: {}. Only import the first one: {}"
              .format(path_to_data_folder, data_tag, files[0]))
    f = np.load(files[0])
    features = f[field]
    fidelities = f["fids"]
    return features, fidelities


def prepare(new_data_path, new_data_tag, old_data_path, old_data_tag, max_num_datapoints, field="ctrls", test_mode=False):
    ctrls, fids = import_data(new_data_path, new_data_tag, field=field, test_mode=test_mode)
    print("new data: ", ctrls.shape, fids.shape)
    old_ctrls, old_fids = import_old_data(old_data_path, old_data_tag, field=field, test_mode=test_mode)
    old_ctrls, old_fids = reshape(old_ctrls, old_fids)
    old_ctrls, old_fids = remove_empty_rows(old_ctrls, old_fids)
    old_ctrls, old_fids = shuffle(old_ctrls, old_fids)
    old_ctrls, old_fids = crop(old_ctrls, old_fids, max_num_datapoints)
    old_ctrls = fill_nan(old_ctrls, 0.0)

    ctrls, fids = stack([ctrls, old_ctrls], [fids, old_fids])
    ctrls, fids = shuffle(ctrls, fids)
    if np.any(np.isnan(ctrls)):
        raise ValueError('Unexpected NaN encountered in controls matrix')
    X_train, X_rest, y_train, y_rest = train_test_split(ctrls, fids, test_size=0.3)
    X_valid, X_test, y_valid, y_test = train_test_split(X_rest, y_rest, test_size=0.5)
    return X_train, y_train, X_valid, y_valid, X_test, y_test


def run(data, model, n_training_epochs, batch_size):
    if isinstance(model, keras.Sequential):
        es = EarlyStopping(patience=2, verbose=True)
        print(model.summary())
        fit_res = model.fit(data[0], data[1],
                            epochs=n_training_epochs,
                            batch_size=batch_size,
                            validation_data=(data[2], data[3]),
                            callbacks=[es]
                            )
        loss_history = (fit_res.history['mean_absolute_error'], fit_res.history['val_mean_absolute_error'])
    elif isinstance(model, HyperoptEstimator):
        # Hypertopt model select the validation set itself, thus combine X_train, X_valid
        X_train_and_valid = np.concatenate((data[0], data[2]), axis=0)
        y_train_and_valid = np.concatenate((data[1], data[3]), axis=0)
        model.fit(X_train_and_valid, y_train_and_valid, valid_size=0.15)
        loss_history = None
    else:  # Must be an sklearn model
        model.fit(data[0], data[1])
        loss_history = None
    test_phase_data = (data[5], model.predict(data[4]), calc_control_length(data[4]))
    return loss_history, test_phase_data
