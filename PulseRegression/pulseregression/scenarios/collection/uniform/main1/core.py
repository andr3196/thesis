"""
Author: Andreas Springborg
Date: 02/12/2018
Goal:

"""
import time

import multiprocessing as mp

import numpy as np
from matplotlib import pyplot as plt


"""     SETUP      """

"""     CONTENT    """

should_plot_collected_F = True


def run_single_collection(config: CollectionConfiguration, time_spec: TimeSpec, i, max_time):
    n_time_steps = time_spec.n_time_steps
    n_ctrls = config.n_ctrls
    batch_size = config.batch_size

    dyn_tc = DynamicTerminationConditions(config)

    dc = DataCollector(config, n_time_steps, config.niter_selection_strategy)
    generator = GRAPEControlGenerator(config, time_spec, dyn_tc=dyn_tc)

    t_start = time.time()
    duration = 0

    collected_F = None

    while dc.should_continue() and duration < max_time:
        seed_controls, opt_controls, fidelities = generator.generate(batch_size, n_time_steps, n_ctrls)

        if collected_F is None:

            collected_F = fidelities.reshape((batch_size, 1))
        else:
            collected_F = np.concatenate((collected_F, fidelities.reshape((batch_size, 1))), axis=1)

        if should_plot_collected_F:
            plt.hist(collected_F, np.linspace(0, 1, 11), stacked=True)
            plt.show()

        dc.append_batch_history(dyn_tc.tc.max_iterations, fidelities)
        dc.collect(seed_controls, opt_controls, fidelities)

        n_it = dc.get_optimal_n_GRAPE_it(fidelities)
        dyn_tc.set_max_iter(n_it)

        duration = time.time() - t_start
    if duration > max_time:
        dc.termination_reason = "Ran out of time."

    #np.savez("collect_hist_n_3_p_0", data=collected_F)

    meta = {"duration": duration,
            "termination_reason": dc.termination_reason,
            "accessibility": dc.accessibility,
            "batch_history": dc.batch_history,
            "n_in_each_bin": dc.n_in_each_bin,
            "collect_it": dc.collect_it,
            "T_final": time_spec.T_final}

    print("Done - process: {}! Termination reason: {}".format(i, dc.termination_reason))
    return i, (dc.seeds, dc.ctrls, dc.fids), meta


def run_collection(config: CollectionConfiguration, max_time):
    p = mp.Pool()
    if config.should_run_parallel:
        async_output = [p.apply_async(run_single_collection, args=(config, time_spec, i, max_time)) for (i, time_spec)
                        in
                        enumerate(config.time_spec_range())]
        p.close()
        p.join()
        result_tuples = [res.get() for res in async_output]
        all_results = sorted(result_tuples)
        results = [result[1] for result in all_results]
        meta = [result[2] for result in all_results]
    else:
        results = [p.apply(run_single_collection, args=(config, time_spec, i, max_time)) for (i, time_spec)
                   in
                   enumerate(config.time_spec_range())]
        p.terminate()
        p.join()

    return DataCollector.gather_sheets(results), meta


"""     OUTPUT     """
