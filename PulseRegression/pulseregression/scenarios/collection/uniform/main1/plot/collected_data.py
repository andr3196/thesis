"""
Author: Andreas Springborg
Date: 23/01/2019
Goal:

"""
import numpy as np

from pulseregression.framework.io.filemanager import LocalFileManager
import matplotlib.pyplot as plt


i_T_selected = -1

labels = ["p={}".format(i) for i in range(10)]

x = np.linspace(0.0, 1.0, 11)
z = np.arange(10)


def make_data_plot():
    gfm = LocalFileManager(path_section="runcollection")

    fig, axes = plt.subplots(nrows=2, ncols=4)

    empty_axes = axes.flatten()[[3, 7]]
    axes = axes.flatten()[[0, 1, 2, 4, 5, 6, ]]

    for i_n in range(6):
        n = i_n + 2
        ax = axes[i_n]
        data_sets = []
        for i_p in range(10):
            p = i_p
            file_tag = gfm.get_collected_filepath() / "coll_runs_n_{}_p_{}.npz".format(n, p)
            f = np.load(file_tag)
            fids = f["fids"]  #[:, i_T_selected]
            fids = fids[fids != 0]
            freqs, _ = np.histogram(fids, x)
            print("For n={}, p={} the frequency distribution is:".format(n,p))
            print(freqs)
            data_sets.append(freqs)
        # total_num_obs = [np.sum(obs) for obs in data_sets]
        # print("best p for n={} is {}".format(n, np.argmax(total_num_obs)))
        data = np.stack(data_sets, axis=0)
        im = ax.imshow(data, extent=[0.0, 1.0, 9.5, -0.5], aspect='auto')
        if i_n == 0:
            ax.set_ylabel("Problem number, p")
        if i_n == 3:
            ax.set_xlabel("Fidelity")
        ax.set_title("n = {}".format(n))
        ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(10)))

    for eax in empty_axes:
        eax.set_axis_off()

    fig.subplots_adjust(hspace=0.3, right=1.0)
    cbar_ax = fig.add_axes([0.80, 0.15, 0.05, 0.7])
    cbar = fig.colorbar(im, cax=cbar_ax)
    cbar.set_label("Total number of data points")
    plt.savefig("Collected_data_sets.pdf", format="pdf", dpi=fig.dpi*2)
    plt.show()


if __name__ == "__main__":
    make_data_plot()
