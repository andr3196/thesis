"""
Author: Andreas Springborg
Date: 02/12/2018
Goal:

"""
import sys
import json
import numpy as np

from pulseregression.framework.grendel.collection import CollectionConfiguration
from pulseregression.scenarios.collection.uniform.main1.core import run_collection

"""     SETUP      """

"""     CONTENT    """


def make_configuration(config_file: dict, i):
    cf_collect = config_file["collection"]
    cf_problem = config_file["problem"]
    cf_control = config_file["control"]
    cf_term = config_file["termination"]

    n = cf_collect["n_min"] + i // cf_collect["n_problems"]
    p = i % cf_collect["n_problems"]

    print("Collecting data for: ", n, p)

    return CollectionConfiguration(
        n=n,
        p=p,
        b=cf_problem["ctrl_ampl"],
        batch_size=cf_collect["batch_size"],
        dt=cf_control["dt"],
        #t_before=cf_control["t_before"],
        #t_after=cf_control["t_after"],
        time_scale=cf_control["time_scale"],
        time_range_low=cf_control["time_range_low"],
        time_range_high=cf_control["time_range_high"],
        bin_max=cf_collect["bin_max"],
        pulse_type=cf_control["pulse_type"],
        save_folder=config_file["saving"]["folder"],
        seed_offset=cf_collect["seed_offset"],
        fid_err_target=cf_term["fid_err_target"],
        max_wall_time=cf_term["max_wall_time"],
        min_grad=cf_term["min_grad"],
        min_progress=cf_collect["min_progress"],
        lives=cf_collect["lives"],
        MAX_ITER=cf_collect["MAX_ITER"],
        niter_selection_strategy=cf_collect["strategy"],
        should_run_parallel= cf_collect["run_parallel"]
    ), cf_collect["max_time"]


def __run__(config_fullpath, i):
    with open(config_fullpath, "r") as f:
        config_file = json.loads(f.read())
    configuration, max_time = make_configuration(config_file, i)
    data, meta = run_collection(configuration, max_time)
    seed_ctrls, opt_ctrls, fids = data
    np.savez(configuration.get_save_path(), **{"seeds": seed_ctrls, "ctrls": opt_ctrls, "fids": fids, "meta": meta})


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 3:
        print("usage: api config_fullpath i")
        sys.exit(1)
    else:
        __run__(args[1], int(args[2]))
