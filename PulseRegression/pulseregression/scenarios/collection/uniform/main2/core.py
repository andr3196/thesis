"""
Author: Andreas Springborg
Date: 02/12/2018
Goal: To determine the functional form of the relationship between the fidelity and the number of GRAPE iterations

"""
from qutip.control.termcond import TerminationConditions
from matplotlib import pyplot as plt

from pulseregression.framework.grendel.utility.quantum import get_fidelity, continue_control_optimization, \
    get_optimizer_generator, init_problem


def increments():
    for i in range(10):
        yield 1
    for i in range(5):
        yield 10
    for i in range(8):
        yield 20


def scan_grape_iterations(optim, tc):
    nit_history = [0]
    fidelity_history = [get_fidelity(optim, tc)]

    for inc in increments():
        nit_history.append(nit_history[-1] + inc)
        tc.max_iterations += inc
        print("Max it: ", tc.max_iterations)

        F = continue_control_optimization(optim, tc)
        fidelity_history.append(F)
        if F > 0.999:
            break
    return nit_history, fidelity_history

#def scan_grape_iterations_batch(N):

#    nits = []
#    for i in range(N):



def init_termconds():
    tc = TerminationConditions()
    tc.max_iterations = 0
    tc.min_gradient_norm = 0
    tc.max_wall_time = 1000
    tc.fid_err_targ = 1e-6
    return tc


if __name__ == "__main__":
    opt_gen = get_optimizer_generator(*init_problem(n=2, p=0, b=1.0), dt=0.1)
    optim = opt_gen(T_final=15.0)
    tc = init_termconds()
    nit, fids = scan_grape_iterations(optim, tc)
    print(nit)
    print(fids)

    plt.plot(nit, fids)
    plt.show()
