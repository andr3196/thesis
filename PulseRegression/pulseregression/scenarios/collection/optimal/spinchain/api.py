"""
Author: Andreas Springborg
Date: 22/12/2018
Goal:

"""

import sys
import time
from pathlib import Path
from plotguard import pyplot
import numpy as np

from pulseregression.framework.optim.optimization import perform_optimization
from pulseregression.framework.termination.termination import get_default_termination_conditions

####  Define  ####
from pulseregression.scenarios.collection.optimal.spinchain.core import init

n_seeds = 100
L = 6
J = 1
g = 1
amp_max = 4.0

# duration range
n_T = 200
n_ts = 200
T_max = 4.0
T_step = T_max / n_T
#duration_range = T_step * np.arange(1, n_T + 1)


# Fidelity convergence
F_threshold = 0.999

tc = get_default_termination_conditions()

# IO
path = Path("../../../../../data/distance/runs/run6/data")


#################


def __run__(T_i):
    optim, filename = init(path,  T_step, T_i, L, J, g, n_ts, amp_max)
    print("Starting run: " + filename)
    t_start = time.time()
    seeds, ctrls, fids = perform_optimization(optim, n_seeds, tc, verbose=True)

    run_time = (time.time()-t_start)/60
    max_fid = np.max(fids)
    print("Finished optimization. Time: {} min. Best fid: {}".format(run_time, max_fid))

    data = {"seeds": seeds, "ctrls": ctrls, "fids": fids}
    np.savez(path / filename, **data)
    return max_fid > F_threshold


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("usage: api [Ti]")
        sys.exit(1)
    else:
        i = int(args[1])
        T_i = int(args[1])
        __run__(T_i)