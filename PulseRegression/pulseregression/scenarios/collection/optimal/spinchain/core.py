"""
Author: Andreas Springborg
Date: 20/01/2019
Goal:

"""
from pulseregression.framework.procedure import make_file_identifier, repetition_guard
from pulseregression.framework.quantum import init_spinchain_problem
from pulseregression.framework.optim.optimization import get_optimizer_function


def init(path, T_step, T_i, L, J, g, n_ts, amp_max):

    T = T_step*(T_i+1)

    file_identifier = make_file_identifier(base="spinchain", L=L, J=J, g=g, Ti=T_i)
    filename = file_identifier + ".npz"

    repetition_guard(path / filename)

    H_d, H_c, psi_0, psi_targ = init_spinchain_problem(L, J, g)
    optim_gen = get_optimizer_function(H_d, H_c, psi_0, psi_targ, N_ts=n_ts, amp_lbound=-amp_max, amp_ubound=amp_max)
    optim = optim_gen(T)
    return optim, file_identifier


