"""
Author: Andreas Springborg
Date: 2019-04-14
Goal:

"""
import sys
from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

### Define ###

# Problem
L = 6
J = 1
g = 1

# IO
from pulseregression.framework.procedure import get_filenames, field_loader

path = Path("../../../../../data/distance/runs/run4/data")


###    ###


def __run__():
    files = get_filenames(path, base="spinchain", L=L, J=J, g=g)

    plt.figure()
    for Ti, ctrls, fids in field_loader(files, ["ctrls", "fids"]):
        plt.scatter(Ti * np.ones(fids.shape), fids)
    plt.show()


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        print("usage: api")
        sys.exit(1)
    else:
        __run__()
