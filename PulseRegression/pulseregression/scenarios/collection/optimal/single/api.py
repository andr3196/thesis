"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Gather optimal controls for

"""

import sys
from pathlib import Path
import numpy as np
from qutip.control.termcond import TerminationConditions

####  Define  ####
from pulseregression.framework.optim.optimization import perform_optimization
from pulseregression.framework.procedure import RepeatedRunError
from pulseregression.framework.qsl import UnknownQSLError
from pulseregression.scenarios.collection.optimal.single.core import init

dt = 0.1
n_seeds = 100
n = 4
p = 1
b = 3.0

# duration range
low = 0.0
high = 1.6
step = 2*dt

tc = TerminationConditions()
tc.max_iterations = 200
tc.fid_err_targ = 1e-6
tc.max_wall_time = 600
tc.min_gradient_norm = 1e-22

# IO
path = Path("../../../../data/distance/runs/run2/data")


#################


def __run__(i):
    optim, n_time_steps, filename = init(i, path, n, p, b, low, high, step, dt)
    print("Starting run: " + filename)
    seeds, ctrls, fids = perform_optimization(optim, n_seeds, n_time_steps, tc)

    data = {"seeds": seeds, "ctrls": ctrls, "fids": fids}
    np.savez(path / filename, **data)


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("usage: api [i] ")
        sys.exit(1)
    else:
        try:
            i = int(args[1])
            __run__(i)
        except RepeatedRunError:
            print("Skipping run i={} as the result already exists".format(i))
        except UnknownQSLError:
            print("Skipping i={} as the run is based on an unknown QSL".format(i))
