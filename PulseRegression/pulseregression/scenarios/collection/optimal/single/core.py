"""
Author: Andreas Springborg
Date: 20/01/2019
Goal:

"""
from pulseregression.framework.optim.optimization import get_optimizer_function
from pulseregression.framework.procedure import make_file_identifier, repetition_guard
from pulseregression.framework.qsl import lookup_qsl
from pulseregression.framework.quantum import init_problem
from pulseregression.framework.utility import make_duration_range, get_n_time_steps


def init(Ti, path, n, p, b, low, high, step, dt):

    file_identifier = make_file_identifier(n=n, p=p, b=b, Ti=Ti)
    filename = file_identifier + ".npz"

    repetition_guard(path / filename)

    T_QSL = lookup_qsl(n, p, b)

    duration_range = make_duration_range(T_QSL, low, high, step=step)
    T = duration_range[Ti]

    H_d, H_c, U_0, U_targ = init_problem(n, p, b)
    optim_gen = get_optimizer_function(H_d, H_c, U_0, U_targ, dt)
    optim = optim_gen(T)
    return optim, get_n_time_steps(T, dt), file_identifier
