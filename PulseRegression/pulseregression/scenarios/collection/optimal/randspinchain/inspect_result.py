"""
Author: Andreas Springborg
Date: 2019-05-02
Goal: To see if calculated optimal controls are okay

"""
import sys
from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

### Define ###

# Problem
L = 6
J = 1
g = 1

T_step = 0.4
# IO
from pulseregression.framework.procedure import get_filenames, field_loader

path = Path("../../../../../data/distance/runs/run7/data")


###    ###


def __run__():
    files = get_filenames(path, base="randspinchain", L=L, J0=J, g=g)

    plt.figure()
    for Ti, ctrls, fids in field_loader(files, ["ctrls", "fids"]):

        x = input("Plot? T: {}".format(T_step * (Ti + 1)))
        if x.lower() == "y":
            plt.plot(ctrls.T)
            print(np.mean(fids))
            plt.show()


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        print("usage: api")
        sys.exit(1)
    else:
        __run__()
