"""
Author: Andreas Springborg
Date: 20/01/2019
Goal:

"""

import itertools

from pulseregression.framework.optim.optimization import get_optimizer_function
from pulseregression.framework.procedure import make_file_identifier, repetition_guard
from pulseregression.framework.quantum import init_rand_spinchain_problem


def init(i, path, T_step, T_i, alpha_range, p_range, L, J0, g, n_ts, amp_max, J_rand_bounds, filebase):
    alpha, p = [x for x in itertools.product(alpha_range, p_range)][i]

    T = T_step * (T_i + 1)

    file_identifier = make_file_identifier(base=filebase, L=L, J0=J0, g=g, a=[alpha, ":1.1f"], p=p, Ti=T_i)
    filename = file_identifier + ".npz"

    repetition_guard(path / filename)

    H_d, H_c, psi_0, psi_targ = init_rand_spinchain_problem(L, J0, g, alpha, noise_mode="uniform",
                                                            noise_args={"bounds": J_rand_bounds}, p=p)
    optim_gen = get_optimizer_function(H_d, H_c, psi_0, psi_targ, N_ts=n_ts, amp_lbound=-amp_max, amp_ubound=amp_max)
    optim = optim_gen(T)
    return optim, file_identifier
