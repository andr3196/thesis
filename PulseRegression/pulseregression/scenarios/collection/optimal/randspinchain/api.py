"""
Author: Andreas Springborg
Date: 22/12/2018
Goal:

"""

import sys
import time
from pathlib import Path
from plotguard import pyplot
import numpy as np

from pulseregression.framework.optim.optimization import perform_optimization
from pulseregression.framework.procedure import RepeatedRunError
from pulseregression.framework.termination.termination import get_moderate_termination_conditions
####  Define  ####
from pulseregression.scenarios.collection.optimal.randspinchain.core import init

n_seeds = 100
L = 6
J0 = 1
g = 1
amp_max = 4.0

# duration range
n_T = 10
n_ts = 200
T_max = 4.0
T_step = T_max / n_T
# duration_range = T_step * np.arange(1, n_T + 1)


# Fidelity convergence
F_threshold = 0.999

# Coupling level
alpha_min = 0.0
alpha_max = 2.0
n_alpha = 6
alpha_range = np.linspace(alpha_min, alpha_max, n_alpha)

J_rand_bounds = (-1.0, 1.0)

# Random couplings
n_p = 5
p_range = range(n_p)

tc = get_moderate_termination_conditions()
tc.min_gradient_norm = 1e-8

# IO
path = Path("../../../../../data/distance/runs/run7/data")
filebase = "randspinchain"


#################


def __run__(i, T_i):
    optim, filename = init(i, path, T_step, T_i, alpha_range, p_range, L, J0, g, n_ts, amp_max, J_rand_bounds, filebase)
    print("Starting run: " + filename)
    t_start = time.time()
    seeds, ctrls, fids = perform_optimization(optim, n_seeds, tc)

    run_time = (time.time() - t_start) / 60
    max_fid = np.max(fids)
    print("Finished optimization. Time: {} min. Best fid: {}".format(run_time, max_fid))

    data = {"seeds": seeds, "ctrls": ctrls, "fids": fids}
    np.savez(path / filename, **data)
    return max_fid > F_threshold


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("usage: api [i] [Ti]")
        sys.exit(1)
    elif len(args) == 3:
        i = int(args[1])
        T_i = int(args[2])
        __run__(i, T_i)
    else:

        # i map to alpha, p

        i = int(args[1])
        T_i = 0
        has_converged = False
        while not has_converged:
            try:
                has_converged = __run__(i, T_i)
            except RepeatedRunError:
                print("Already completed this run!")
            T_i += 1
        __run__(i, T_i)
