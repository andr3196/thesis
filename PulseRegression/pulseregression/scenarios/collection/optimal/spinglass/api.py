"""
Author: Andreas Springborg
Date: 22/12/2018
Goal:

"""

import sys
import time
from pathlib import Path
from plotguard import pyplot
import numpy as np

from pulseregression.framework.optim.optimization import perform_optimization
from pulseregression.framework.procedure import RepeatedRunError
from pulseregression.framework.termination.termination import get_default_termination_conditions, \
    get_moderate_termination_conditions
from pulseregression.scenarios.collection.optimal.spinglass.core import init

####  Define  ####
from pulseregression.scenarios.controls.check.api import match_controls

n_seeds = 1
L = 6
J = 1
g = 1
amp_max = 4.0

# duration range
n_T = 10
n_ts = 200
T_max = 4.0
T_step = T_max / n_T
#duration_range = T_step * np.arange(1, n_T + 1)


# Fidelity convergence
F_threshold = 0.999

# Coupling level
alpha_min = 0.0
alpha_max = 1.0
n_alpha = 6
alpha_range = np.linspace(alpha_min, alpha_max, n_alpha)

# Random couplings
n_p = 5
p_range = range(n_p)

tc = get_moderate_termination_conditions()

# IO
path = Path("../../../../../data/distance/runs/run5/data")


#################

def __run__(i, T_i):
    optim, filename = init(i, path,  T_step, T_i, alpha_range, p_range, L, J, g, n_ts, amp_max)
    print("Starting run: " + filename)
    t_start = time.time()
    seeds, ctrls, fids = perform_optimization(optim, n_seeds, tc)

    run_time = (time.time()-t_start)/60
    max_fid = np.max(fids)
    print("Finished optimization. Time: {} min. Best fid: {}".format(run_time, max_fid))

    data = {"seeds": seeds, "ctrls": ctrls, "fids": fids}
    np.savez(path / filename, **data)
    return max_fid > F_threshold


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("usage: api [i] {Ti, optional}")
        sys.exit(1)
    elif len(args) == 3:
        i = int(args[1])
        T_i = int(args[2])
        __run__(i, T_i)

    else:

        # i map to alpha, p

        i = int(args[1])
        T_i = 0
        T_i_max = 16
        has_converged = False
        while not has_converged and T_i < T_i_max:
            try:
                has_converged = __run__(i, T_i)
            except RepeatedRunError:
                print("Already completed this run!")
            T_i += 1
