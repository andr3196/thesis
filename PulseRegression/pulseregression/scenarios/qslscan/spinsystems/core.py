"""
Author: Andreas Springborg
Date: 2019-04-25
Goal:

"""
import numpy as np

from pulseregression.framework.procedure import field_loader


def find_qsls(files, T_map,  n_p, n_alpha, alphas, F_QSL=0.99):
    qsls = np.zeros((n_p, n_alpha))

    for p in range(n_p):
        for j, alpha in enumerate(alphas):

            match_files = [f for f in files if "a_{:01.1f}_p_{}".format(alpha, p) in str(f)]

            data_gen = field_loader(match_files, "fids")

            Ti, fids = next(data_gen)

            F_cur = np.max(fids)
            T_cur = T_map(Ti)

            for Ti, fids in data_gen:

                T_last = T_cur
                F_last = F_cur

                T_cur = T_map(Ti)
                F_cur = np.max(fids)

                if F_cur > F_QSL:
                    break
            else:
                F_cur = np.nan
                #raise ValueError("No datapoint above QSL fidelity = {}. Final was {}".format(F_QSL, F_cur))

            # Perform linear interpolation to get QSL
            qsls[p, j] = (F_QSL - F_last)/(F_cur - F_last)*(T_cur-T_last) + T_last

    return qsls









