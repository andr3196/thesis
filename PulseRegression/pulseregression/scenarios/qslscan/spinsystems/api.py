"""
Author: Andreas Springborg
Date: 2019-04-25
Goal:

"""
import sys
from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

### Define ###

# Problem
from pulseregression import systems
from pulseregression.scenarios.qslscan.spinsystems.core import find_qsls
from pulseregression.styling import defaults

L = 6
J = 1
g = 1

# Realisations
n_p = 5

# coupling strengths (spinglass)
n_alpha = 6
alpha_min = 0.0
alpha_max = 1.0
spinglass_alphas = np.linspace(alpha_min, alpha_max, n_alpha)

# coupling stenghths (randspinchain)
alpha_min = 0.0
alpha_max = 2.0
randspinchain_alphas = np.linspace(alpha_min, alpha_max, n_alpha)

# Time mapping

n_T0 = 10
T_max = 4.0
T_step = T_max / n_T0

# Map from Ti to T
T_map = lambda Ti: T_step * (Ti + 1)

# IO
from pulseregression.framework.procedure import get_filenames, field_loader

path_spinglass = Path("../../../../data/distance/runs/run5/data")
path_randspinchain = Path("../../../../data/distance/runs/run7/data")


###    ###


def __run__():
    spinglass_files = get_filenames(path_spinglass, base="spinglass", L=L, J=J, g=g)
    spinglass_QSLs = find_qsls(spinglass_files, T_map, n_p, n_alpha, spinglass_alphas)

    randspinchain_files = get_filenames(path_randspinchain, base="randspinchain", L=L, J0=J, g=g)
    randspinchain_QSLs = find_qsls(randspinchain_files, T_map, n_p, n_alpha, randspinchain_alphas)

    sg_mean_QSL = np.mean(spinglass_QSLs, axis=0)
    rsc_QSL = np.nanmean(randspinchain_QSLs, axis=0)

    plt.subplot(211)
    ps = plt.plot(spinglass_alphas, spinglass_QSLs.T, 'rx')
    ps[0].set_label("Spin glass")
    plt.plot(spinglass_alphas, sg_mean_QSL, 'r-', alpha=0.4)
    plt.ylabel("$T_{min}^{F=0.99}$", fontsize=defaults.fontsize)
    plt.xlabel("$\\gamma$", fontsize=defaults.fontsize)
    plt.legend()
    plt.subplot(212)

    ps = plt.plot(randspinchain_alphas, randspinchain_QSLs.T, 'b+')
    ps[0].set_label("Anis. Spin chain")
    plt.plot(randspinchain_alphas, rsc_QSL, 'b-', alpha=0.4)
    plt.legend()
    plt.xlabel("$\\alpha$", fontsize=defaults.fontsize)
    plt.ylabel("$T_{min}^{F=0.99}$", fontsize=defaults.fontsize)
    plt.subplots_adjust(hspace=0.4)
    systems.save_figure(plt, "spinsystems_minimum_transfer_times")

    plt.show()


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        print("usage: api")
        sys.exit(1)
    else:
        __run__()
