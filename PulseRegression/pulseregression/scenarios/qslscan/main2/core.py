"""
Author: Andreas Springborg
Date: 25/01/2019
Goal: Reads the T_QSL from all_controls files in path_to_single_files and gathers them in a ndarray saved to path_to_gathered_file

"""

import numpy as np
from pulseregression.framework.grendel.qsl import map_to_indices, n_n, n_p, n_b, qsl_data_filename,\
    path_to_gathered_file, path_to_single_files
from pulseregression.framework.procedure.procedure import get_parameters_from_filename

""" Define """


"""        """



def gather_qsl_files():
    qsl_cube = np.zeros((n_n, n_p, n_b))

    file_gen = path_to_single_files.glob("run_*.txt")
    for file in file_gen:
        n, p, b = get_parameters_from_filename(file)
        ni, pi, bi = map_to_indices(n, p, b)
        with open(file, 'r') as f:
            T_QSL = float(f.read())
        qsl_cube[ni, pi, bi] = T_QSL
    np.savez(path_to_gathered_file / qsl_data_filename, qsl=qsl_cube)
