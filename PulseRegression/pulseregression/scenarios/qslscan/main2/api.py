"""
Author: Andreas Springborg
Date: 13/02/2019
Goal: This script calculates the approximate QSL for a range of problems by guessing the QSL, generating a batch of controls, optimizing until the fidelity of one exceeds the QSL, then decreasing the duration and repeating until no optimal control is found.

"""
from pulseregression.scenarios.qslscan.main2.core import gather_qsl_files

####  Define  ####

#################


if __name__ == "__main__":
    gather_qsl_files()
