"""
Author: Andreas Springborg
Date: 25/01/2019
Goal: Uses a simple line search algorithm to find the

"""

import numpy as np
from queue import Empty
import multiprocessing as mp

from pulseregression.framework.grendel.utility.quantum import get_fidelity
from pulseregression.framework.grendel.utility.time import get_n_time_steps

""" Define """
b_index_map = {0.5: 0, 1.0: 1, 3.0: 2}
slopes = [7.447, 6.333, 5.235]
offsets = [-3.311, -6.428, -8.539]
shifts = [40.0, 25.0, 25.0]

"""        """


def make_filename(n, p, b, ext=".npz"):
    return "run_n_{}_p_{}_b_{}{}".format(n, p, b, ext)


def guess_T_init(n, b):
    """ From the previous step_through of the QSL, we can make qualified guesses of the QSLs """
    try:
        bi = b_index_map[b]
        return np.round(slopes[bi] * n + offsets[bi] + shifts[bi], 1)
    except KeyError:
        raise NotImplementedError('Unknown b key: {}'.format(b))


def get_best_fidelity(optim, T, n_tries_init, dt, F_QSL, tc):
    """ Try n_tries times to generate a control above the QSL, if it fails return the best seen """
    n_tries = n_tries_init
    n_T = get_n_time_steps(T, dt)
    while n_tries > 0:
        F = get_fidelity(optim, tc)
        if F >= F_QSL:
            return F
        else:
            n_tries -= 1
    return np.nan


def get_best_fidelity_wrapper(queue, optim, T, n_tries, dt, F_QSL, tc):
    np.random.seed()
    F = get_best_fidelity(optim, T, n_tries, dt, F_QSL, tc)
    print("Adding F={} to queue".format(F))
    queue.put(F)


def get_best_fidelity_parallel(generator, T, n_processes, n_tries_per_proc, dt, F_QSL, tc, max_queue_time):
    """ """
    queue = mp.Queue()
    my_workers = []
    for _ in range(n_processes):
        optim = generator(T)
        worker = mp.Process(target=get_best_fidelity_wrapper, args=(queue, optim, T, n_tries_per_proc, dt, F_QSL, tc))
        worker.start()
        my_workers.append(worker)

    n = n_processes
    while n > 0:
        try:
            F = queue.get(timeout=max_queue_time)
            if np.isnan(F):
                n -= 1
            else:
                for worker in my_workers:
                    worker.terminate()
                return F
        except Empty:
            print("Optimizing too slow")
            break
    for worker in my_workers:
        worker.terminate()
    return np.nan


def find_QSL(generator, T_init, n_tries, dt, F_QSL, tc, step_increase=5.0):
    print("Running line search")
    steps = [1.0, 0.5, 0.2, 0.1]
    si = 0
    T_last = T_init
    T_cur = T_init
    last_was_successful = True
    while True:
        F_max = get_best_fidelity(generator, T_cur, n_tries, dt, F_QSL, tc)
        print(F_max)
        if F_max < F_QSL:
            if T_cur == T_last - 0.1:
                return T_last
            elif T_cur == T_last:
                return find_QSL(generator, T_init + step_increase, n_tries, dt, F_QSL, tc, step_increase)
            si += 1
            last_was_successful = False
        else:
            T_last = T_cur
            if not last_was_successful:
                si += 1
            last_was_successful = True
        T_cur = T_last - steps[min(si, len(steps) - 1)]
        print(T_cur)


def find_QSL_simple(generator,
                    T_init,
                    n_processes,
                    n_tries_per_proc,
                    dt,
                    F_QSL,
                    tc,
                    step_increase=2.0,
                    max_queue_time=60 * 60):
    print("running simple")
    step = 0.1
    T_cur = T_init
    while True:
        F = get_best_fidelity_parallel(generator, T_cur, n_processes, n_tries_per_proc, dt, F_QSL, tc, max_queue_time)
        if np.isnan(F):
            if T_cur == T_init:
                return find_QSL_simple(generator, T_init + step_increase, n_processes, n_tries_per_proc, dt, F_QSL, tc,
                                       step_increase, max_queue_time)
            return T_cur + step
        else:
            T_cur -= step
            print(T_cur)
