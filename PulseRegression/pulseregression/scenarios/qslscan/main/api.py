"""
Author: Andreas Springborg
Date: 13/02/2019
Goal: This script calculates the approximate QSL for a range of problems by guessing the QSL, generating a batch of controls, optimizing until the fidelity of one exceeds the QSL, then decreasing the duration and repeating until no optimal control is found.

"""
import sys
from pathlib import Path

from qutip.control.termcond import TerminationConditions

from pulseregression.framework.procedure.procedure import repetition_guard
from pulseregression.framework.grendel.utility.quantum import get_optimizer_generator, \
    init_problem
from pulseregression.scenarios.qslscan.main.core import find_QSL_simple, guess_T_init, make_filename
from pulseregression.scenarios.utility.mapping import map_j_to_npb

####  Define  ####
dt = 0.1
F_QSL = 0.999
n_tries_per_proc = 250
n_processes = 8
n_p = 40
n_b = 3
b_values = [0.5, 1.0, 3.0]

tc = TerminationConditions()
tc.max_iterations = 200
tc.fid_err_targ = 1e-6
tc.max_wall_time = 600
tc.min_gradient_norm = 1e-22

# Queue
max_queue_time = 20 * 60

# IO
path = Path("../../../../data/qslscan/runs/run1/data")

#################


if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Usage: find_qsl j")
    else:
        j = int(args[1])
        n, p, b = map_j_to_npb(j, n_p, n_b, b_values)
        filename = make_filename(n, p, b, ext=".txt")
        full_path = path / filename

        repetition_guard(full_path)

        H_d, H_c, U_0, U_targ = init_problem(n, p, b)

        generator = get_optimizer_generator(H_d, H_c, U_0, U_targ, dt)
        T_init = guess_T_init(n, b)
        T_QSL = find_QSL_simple(generator, T_init, n_processes, n_tries_per_proc, dt, F_QSL, tc, step_increase=2.0,
                                max_queue_time=max_queue_time)
        print(T_QSL)
        with open(full_path, 'w') as f:
            f.write('{:.1f}'.format(T_QSL))
