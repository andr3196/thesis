"""
Author: Andreas Springborg
Date: 19/04/2019
Goal: Save the problem Hamiltonians to a mat file

"""
import sys
from pathlib import Path

import numpy as np


####  Define  ####
from scipy.io import savemat

from pulseregression.framework.quantum import init_problem

n_n = 10
n_p = 40
n_b = 3
n_comp = 3  # H_d, H_c, U_targ
n_values = np.arange(2, 2+n_n)
p_values = np.arange(n_p)

b_values = [0.5, 1.0, 3.0]


# IO
path = Path("../../../../data/qslscan/collected/run1/")

#################


if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        print("Usage: problems")
    else:

        out = np.zeros((n_values[-1], n_values[-1], n_n, n_p, n_b, n_comp), dtype=np.complex)
        out[:] = np.nan

        for i, b in enumerate(b_values):
            for j, n in enumerate(n_values):
                for k, p in enumerate(p_values):
                    H_d, H_c, U_0, U_targ = init_problem(n, p, b)
                    out[:n, :n, j, k, i, 0] = H_d.full()
                    out[:n, :n, j, k, i, 1] = H_c.full()
                    out[:n, :n, j, k, i, 2] = U_targ.full()
        savemat(path / "problems.mat", {"problems": out})
        print(out)

