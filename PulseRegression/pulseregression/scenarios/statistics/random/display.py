
"""
Author: Andreas Springborg
Date: 2019-05-30
Goal:

"""
from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

from pulseregression import systems
from pulseregression.framework.procedure import get_filenames, field_loader, extract_field
from pulseregression.styling import defaults

load_path = Path("../../../../data/distance/collected/run1/statistic/")


def display():

    files = get_filenames(load_path, base="hopkins", sort_tag=None)
    n_values = np.array(extract_field(files, "n_"))
    arg_ind= np.argsort(n_values)
    n_values = n_values[arg_ind]
    files = [files[i] for i in arg_ind]

    n_n = 12
    bi = 2
    n_T = 14
    Tis = np.arange(14)
    Ts = 0.1*Tis + 0.2
    #Ts = np.linspace(0.2, 1.6, n_T)
    #n_values = np.arange(2, 12)

    cmap = plt.get_cmap("viridis")
    for n, statistic in zip(n_values, field_loader(files, "statistic", prepend_Ti=False)):

        mean_stat = np.nanmean(statistic[:, bi, :], axis=0)

        plt.plot(Ts, mean_stat, color=cmap((n+1)/n_n), label="n = {}".format(n))
    plt.legend()
    plt.xlabel("Duration, $T/T_{min}$", fontsize=defaults.latexfontsize)
    plt.ylabel("Hopkins statistic, $h$", fontsize=defaults.latexfontsize)
    defaults.set_ticksize(plt.gca())
    systems.save_figure(plt, "hopkins_statistics_all")
    plt.show()





if __name__ == "__main__":
    display()