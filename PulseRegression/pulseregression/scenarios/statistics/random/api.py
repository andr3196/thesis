"""
Author: Andreas Springborg
Date: 2019-05-30
Goal:

"""
import sys
from pathlib import Path

import numpy as np
from plotguard import pyplot as plt
from pulseregression.framework.clustering.hopkins import hopkins
from pulseregression.framework.procedure import field_loader, get_filenames
from pulseregression.framework.problems import ScanProperties, RandomHamiltonianProblem

sp = ScanProperties(T_step=0.2)
rhp = RandomHamiltonianProblem(n=4, p=1, b=3.0, scanproperties=sp)

# data shape

n_T_dict = {0: 121, 1: 94}

n_T = n_T_dict[rhp.p]


def hopkins_scan():
    metrics = []
    Tis = []

    for Ti, ctrls in field_loader(rhp.get_load_files(), "ctrls"):
        metrics.append(hopkins(ctrls))
        Tis.append(Ti)

    plt.plot(Tis, metrics)
    plt.show()


def hopkins_scan_all(n):
    n_p = 20
    n_T = 14
    n_b = 3
    b_values = [0.5, 1.0, 3.0]
    data_out = np.zeros((n_p, n_b, n_T)) * np.nan

    for p in range(n_p):
        for j, b in enumerate(b_values):
            files = get_filenames(Path("../../../../data/distance/runs/run1/data"), n=n, p=p, b=b)
            print(files)
            if files:
                for Ti, ctrls in field_loader(files, "ctrls"):
                    h = hopkins(ctrls)
                    data_out[p, j, Ti] = h

    np.savez("../../../../data/distance/collected/run1/statistic/" + "hopkins_n_{}.npz".format(n), statistic=data_out)


if __name__ == "__main__":

    mode = "all"

    if mode == "single":
        hopkins_scan()
    elif mode == "all":
        args = sys.argv
        if len(args) > 1:
            n = int(args[1])
            hopkins_scan_all(n)
        else:
            print("Not enough input arguments")
