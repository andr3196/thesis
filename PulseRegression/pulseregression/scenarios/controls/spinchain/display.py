"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot the control for the specified problem

"""

from pathlib import Path

import numpy as np

from plotguard import pyplot as plt
from plotguard.matplotlib_mod import make_color_list
from pulseregression import systems
from pulseregression.framework.clustering.kmeans import get_cluster_labels, get_cluster_centres
from pulseregression.framework.control import apply_smoothing
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.procedure.procedure import field_loader, get_filenames

####  Define  ####
from pulseregression.framework.utility import calculate_symmetric_fraction
from pulseregression.scenarios.landscape.spinchain.common import data_iterator
from pulseregression.framework.problems import SpinChainProblem
from pulseregression.scenarios.durationcorrelation.spinchain.api import get_trajectories

scp = SpinChainProblem()


# duration range
n_T = 200
n_ts = 200
T_max = 4.0
dt = T_max / n_ts
T_step = T_max / n_T
# Define time
T_range = T_step * np.arange(n_T) + dt

# Ti_filter = lambda x: x in [0, 7, 8, 9, 13]

# IO
load_path = Path("../../../../data/distance/runs/run6/data")
save_path = Path("../../../../data/distance/collected/run6/")
swap_path = Path("../../../../data/distance/collected/run6/swapping")

# Procedure
show_diff = False
smoothing = True

colors = 'bgrcmyk'

#################


def step_through(file_list):
    found = False
    for Ti1, seeds1, ctrls1, fids1 in data_iterator():

        print("Ti: {} - N_ts: {}".format(Ti1, ctrls1.shape[1]))

        if not found and np.max(fids1) > 0.999:
            found = True
            print("Ti: {} - QSL!")

        x = input("Plot? T: {}".format(T_range[Ti1]))
        if x.lower() == "y":

            ctrls1 = apply_default_bessel_filter(ctrls1, scp.bounds())
            ctrls1, _, _ = get_cluster_centres(ctrls1, fids1, scp.max_ampl)

            sf = calculate_symmetric_fraction(ctrls1)
            print(sf)

            plt.figure()

            times = np.linspace(0, T_range[Ti1], ctrls1.shape[1])



            plt.plot(times, ctrls1.T)
            plt.xlabel("Time")
            plt.ylabel("Control strength")
            plt.show()


def plot4_clusters(files, Tis):
    # Grid
    n_rows, n_cols = 2, 3
    fig, ax_array = plt.subplots(n_rows, n_cols, sharey=True)

    ax_numbers = np.arange(ax_array.size)

    for ax, file, Ti, n in zip(ax_array.flatten(), files, Tis, ax_numbers):

        Ti, ctrls, fids = next(field_loader(file, ["ctrls", "fids"]))

        T = T_range[Ti]
        t = np.linspace(0, T, n_ts)
        if n == 2 or n == 5:
            centers = get_cluster_centres(ctrls, fids, scp.max_ampl)
            for i, clust in enumerate(zip(*centers)):
                c,f,ind = clust
                ax.plot(t, c, linewidth=1, color=colors[i])
        else:
            ax.plot(t, ctrls.T)

        ax.set_title("T = {:1.2f}".format(T))
        # ax.set_ylim([-4.1, 4])
        if n > 2:
            ax.set_xlabel("Time")
        if n % 3 == 0:
            ax.set_ylabel("Control amplitude")

    plt.subplots_adjust(hspace=0.4)
    systems.save_figure(plt,"ctrl_clusters_spinchain_L6J1g1.pdf")
    #plt.savefig("ctrl_clusters_spinchain_L6J1g1.pdf", format="pdf")
    plt.show()


def plot4(files, Tis):
    fig, ax_array = plt.subplots(2, 2)

    ax_numbers = np.arange(ax_array.size)

    all_ctrls = []
    for Ti, ctrls in field_loader(files, "ctrls"):
        all_ctrls.append(ctrls)

    for ax, ctrls, Ti, n in zip(ax_array.flatten(), all_ctrls, Tis, ax_numbers):
        T = T_range[Ti]
        t = np.linspace(0, T, n_ts)
        ax.plot(t, ctrls.T, linewidth=1)
        ax.set_title("T = {:1.2f}".format(T))
        if n > 1:
            ax.set_xlabel("Time")
        if n % 2 == 0:
            ax.set_ylabel("Control field")

    plt.subplots_adjust(hspace=0.4)
    #systems.save_figure(plt,"ctrl_examples_spinchain_L6J1g1.pdf" )
    plt.show()

"""     OUTPUT     """

if __name__ == "__main__":
    # Get files
    file_list = get_filenames(load_path, base="spinchain", L=scp.L, J=scp.J, g=scp.g)

    mode = "plot4"
    if mode == "plot4_clusters":
        # Plot examples of controls for a few durations

        # Define
        Tis = [12, 28, 28, 120, 148, 148]

        files = [f for f in file_list for Ti in Tis if "Ti_{}.".format(Ti) in str(f)]

        plot4_clusters(files, Tis)
    elif mode == "plot4":
        # Define
        Tis = [12, 28, 120,  148]

        files = [f for f in file_list for Ti in Tis if "Ti_{}.".format(Ti) in str(f)]

        plot4(files, Tis)
    else:
        step_through(file_list)
