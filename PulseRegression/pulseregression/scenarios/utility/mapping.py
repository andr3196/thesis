"""
Author: Andreas Springborg
Date: 14/02/2019
Goal:

"""

def map_j_to_npb(j, n_p, n_b, b_values):
    n = 2 + j // (n_p * n_b)
    p = (j // n_b) % n_p
    bi = j % n_b
    b = b_values[bi]

    return n, p, b


def map_j_to_npbT(j, n_p, n_b, n_T, b_values, n_offset=2, p_offset=0):
    """ Turns slurm index into problem parameters"""

    n3 = n_p * n_b * n_T
    n2 = n3 // n_p

    n = n_offset + j // n3
    p = p_offset + (j // n2) % n_p
    bi = (j // n_T) % n_b
    b = b_values[bi]

    Ti = j % n_T
    return n, p, b, Ti
