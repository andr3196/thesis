"""
Author: Andreas Springborg
Date: 2019-05-26
Goal:

"""
from scipy.spatial.distance import pdist, squareform

from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

from pulseregression import systems
from pulseregression.framework.procedure import field_loader, get_filenames
from pulseregression.styling import defaults

length_scale_out_file = Path("../../../../data/distance/collected/run1/run_lengthscale_th_001.npz")


def all_controls3d():
    n_seeds = 500

    load_path = Path("../../../../data/distance/runs/run1/data")

    max_ampl = 1
    # Get files
    file_list = get_filenames(load_path, n=4, p=0, b=[3.0, ":1.1f"])
    plt.figure()

    all_data = []
    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):
        # ctrls = ctrls[:100,:]
        # fids = fids[:100]
        print("calc: {}".format(Ti))
        all_dists = pdist(ctrls)
        max_dist = np.sqrt(ctrls.shape[1] * (2 * max_ampl) ** 2)
        all_dists = all_dists / max_dist
        sq_all_dists = squareform(all_dists)
        max_dists_ind = np.argmax(sq_all_dists, axis=0)
        max_dists = sq_all_dists[np.arange(n_seeds), max_dists_ind]

        fids = np.reshape(fids, (n_seeds, 1))
        df = pdist(fids)
        sq_df = squareform(df)
        df = sq_df[np.arange(n_seeds), max_dists_ind]

        all_data.append((max_dists, df))

        # plt.scatter(all_dists, df, s=1.0)
        # plt.title("T/T_QSL= {:03.2f}".format(T))
        # plt.xlabel("$\\|\\|c_i^\\ast- c_j^\\ast\\|\\|/D_{max}$")
        # plt.ylabel("$|F(c_i^\\ast)-F(c_j^\\ast)|$")
        # plt.xlim([0, 1])
        # plt.savefig("spinchain_dist_df_L{}J{}g{}Ti{}.png".format(L, J, g, Ti), format="pdf")
        # plt.draw()
        # plt.pause(5)

    n_T = 14
    Ti = np.arange(14)
    Ts = (1.6 - 0.2) * Ti / n_T + 0.2

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for T, d in zip(Ts, all_data):
        dists, df = d
        times = np.ones(df.shape) * T
        ax.scatter(dists, times, df)
        print("Done, T = {}".format(T))
    plt.show()


def all_controls():
    n_seeds = 500
    Ti = 8
    load_path = Path("../../../../data/distance/runs/run1/data")

    max_ampl = 1
    # Get files
    file_list = get_filenames(load_path, n=4, p=0,b=3.0,Ti=Ti)
    plt.figure()

    k = 0
    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):

        #k += 1
        #if Ti > 2:
        #    continue
        #plt.clf()
        #print(str(file_list[k]))
        all_dists = pdist(ctrls)
        max_dist = np.sqrt(ctrls.shape[1] * (2 * max_ampl) ** 2)
        all_dists = all_dists / max_dist

        fids = np.reshape(fids, (n_seeds, 1))
        df = pdist(fids)
        #plt.scatter(all_dists, df, s=1.0, c='b')
        #plt.draw()
        #plt.waitforbuttonpress()

    n_T = 14
    T = (1.6 - 0.2) * Ti / n_T + 0.2

    ind = np.argsort(df)
    all_dists = all_dists[ind]
    df = df[ind]
    filt = df > 0.01
    k = np.flatnonzero(df[filt])[0]


    plt.scatter(all_dists, df, s=1.0, c='b')
    plt.title("$T/T_{min}$ " + "= {:03.2f}".format(T))
    plt.xlabel("$\\|c_i^\\ast- c_j^\\ast\\|/d_{max}$", fontsize=defaults.latexfontsize)
    plt.ylabel("$|F(c_i^\\ast)-F(c_j^\\ast)|$", fontsize=defaults.latexfontsize)
    plt.axhline(0.01, 0, all_dists[k], color='r', linestyle='--')
    plt.axvline(all_dists[k], 0, 0.04, color='g', linestyle='-')
    plt.annotate("$l$", (all_dists[k], -0.08), annotation_clip=False, fontsize=defaults.largefontsize)
    plt.xlim([0, 1])
    defaults.set_ticksize(plt.gca())
    plt.subplots_adjust(bottom=0.15)
    systems.save_figure(plt, "run_dist_vs_df_n4p0b3")
    plt.show()



def length_scale_scan():
    # data dimensions:(n,p,b,Ti), shape: (10, 20, 3, 14)
    lengths = next(field_loader(length_scale_out_file, "lengths", prepend_Ti=False))

    n_T = 14
    Ti = np.arange(14)
    Ts = (1.6 - 0.2) * Ti / n_T + 0.2

    l = lengths[:, :, 2, :]
    l_mean = np.nanmean(l, axis=1)
    l_mean[np.isnan(l_mean)] = 1.0

    print(l_mean)

    plt.imshow(np.flipud(l_mean.T), cmap="viridis", vmin=0.15, vmax=0.65, extent=[2 - 0.5, 11 + 0.5, Ts[0] - 0.05, Ts[-1] + 0.05],
               aspect="auto")
    plt.xlabel("Dimension, $n$", fontsize=defaults.latexfontsize)
    plt.ylabel("Duration, $T/T_{min}$", fontsize=defaults.latexfontsize)
    defaults.set_ticksize(plt.gca())
    bar = plt.colorbar()
    bar.set_label("Length scale, $l$", fontsize=defaults.latexfontsize)
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # [X, Y] = np.meshgrid(Ts, np.arange(2, 12))
    # surf = ax.plot_surface(X, Y, l_mean, cmap="jet", vmin=0.1, vmax=0.5,
    #                       linewidth=0, antialiased=False)

    # labels = ["n={}".format(n) for n in range(2, 12)]
    # for i, li in enumerate(l_mean):
    #    plt.plot(Ts, li, label=labels[i])

    # plt.xlabel("$\\|\\|c_i^\\ast- c_j^\\ast\\|\\|/D_{max}$")
    # plt.ylabel("$|F(c_i^\\ast)-F(c_j^\\ast)|$")
    # plt.legend()
    systems.save_figure(plt, "run_superlandscape_lengthscale")
    plt.show()


if __name__ == "__main__":

    mode = "length_scale"

    if mode == "all_controls":
        all_controls()
    else:
        length_scale_scan()
