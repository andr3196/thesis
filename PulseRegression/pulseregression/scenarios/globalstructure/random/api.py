"""
Author: Andreas Springborg
Date: 2019-05-06
Goal:

"""
from pathlib import Path

from scipy.spatial.distance import pdist

from plotguard import pyplot as plt
import numpy as np

from pulseregression.framework.procedure import get_filenames, field_loader
from pulseregression.framework.problems import RandomHamiltonianProblem, ScanProperties

props = ScanProperties(T_step=0.2)
rhp = RandomHamiltonianProblem(4, 1, 3.0, props)

# IO
load_path = Path("../../../../data/distance/runs/run2/data")

data_path = Path("../../../../data/distance/runs/run1/data")
length_scale_out_file = Path("../../../../data/distance/collected/run1/run_lengthscale")


#################

def get_lengthscale(ctrls, fids, threshold=0.05):
    all_dists = pdist(ctrls)
    max_dist = np.sqrt(ctrls.shape[1] * (2 * rhp.max_ampl) ** 2)
    all_dists = all_dists / max_dist
    fids = np.reshape(fids, (ctrls.shape[0], 1))
    df = pdist(fids)
    indices = np.argsort(all_dists)
    df = df[indices]
    above_th = np.flatnonzero(df > threshold)
    if not np.any(above_th):
        return np.nan
    else:
        all_dists = all_dists[indices]
        return all_dists[above_th[0]]


def length_scale():
    # Get files
    file_list = rhp.get_load_files()

    Tis = []
    Ls = []
    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):
        Tis.append(Ti)
        print(Ti)
        Ls.append(get_lengthscale(ctrls, fids, threshold=0.02))

    Ls = np.array(Ls)
    Ts = rhp.get_T(Tis)
    norm_Ts = Ts / rhp.T_QSL
    plt.plot(norm_Ts, Ls)

    filt = np.logical_not(np.isnan(Ls))
    Ls = Ls[filt]
    norm_Ts = norm_Ts[filt]

    p_opt = np.polyfit(norm_Ts, Ls, 2)
    print(p_opt)
    x = np.linspace(0.0, 1.6, 200)
    y = np.polyval(p_opt, x)
    plt.plot(x, y, 'r--')
    plt.show()


def length_scale_all_n():
    n_n = 10
    n_p = 20
    n_b = 3
    n_ti = 14
    n_values = np.arange(2, 12)
    p_values = np.arange(20)
    b_values = [0.5, 1.0, 3.0]
    Ti_values = np.arange(n_ti)

    # Get files
    lengths = np.zeros((n_n, n_p, n_b, n_ti))
    lengths[:] = np.nan
    for i, n in enumerate(n_values):
        for j, p in enumerate(p_values):
            for k, b in enumerate(b_values):
                for q, ti in enumerate(Ti_values):
                    file = get_filenames(data_path, n=n, p=p, b=b, Ti=ti)
                    if file:
                        print("Found!")
                        ctrls, fids = next(field_loader(file, ["ctrls", "fids"], prepend_Ti=None))
                        lengths[i, j, k, q] = get_lengthscale(ctrls, fids)

    np.savez(length_scale_out_file, lengths=lengths)


def __run__():
    mode = "length_scale"
    if mode == "length_scale":
        length_scale()
    elif mode == "length_scale_all_n":
        length_scale_all_n()


"""     OUTPUT     """

if __name__ == "__main__":
    __run__()
