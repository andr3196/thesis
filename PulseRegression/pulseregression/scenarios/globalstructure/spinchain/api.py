"""
Author: Andreas Springborg
Date: 2019-04-29
Goal: Scan

"""
####  Define  ####
from plotguard import pyplot as plt
from pathlib import Path
import numpy as np
from scipy.spatial.distance import pdist

from pulseregression.framework.control import get_cluster_centres
from pulseregression.framework.procedure import get_filenames, field_loader
from pulseregression.scenarios.smoothing.core import lookup_cluster_fidelities

L = 6
J = 1
g = 1

max_ampl = 4

range_width = 2 * max_ampl

n_seeds = 100
n_p = 5

# duration range
n_T0 = 200
n_ts = 200
T_max = 4.0
dt = T_max / n_ts
T_step = T_max / n_T0

# IO
load_path = Path("../../../../data/distance/runs/run6/data")
save_path = Path("../../../../data/distance/collected/run6")


#################

def clusters():
    # Get files
    file_list = get_filenames(load_path, base="spinchain", L=L, J=J, g=g, Ti=148)

    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):
        centers, ind_list = get_cluster_centres(ctrls, fids, max_ampl, window_length=3)
        n_clust = len(ind_list)
        save_file = save_path / file_list[0].name.replace(".", "_mean_clust.")
        fidelities = lookup_cluster_fidelities(save_file)

        center_fidelities = [np.mean(fidelities[ind_list[i]]) for i in range(n_clust)]

        all_dists = pdist(centers)
        max_dist = np.sqrt(ctrls.shape[1] * range_width ** 2)
        all_dists = all_dists / max_dist
        fids = np.reshape(center_fidelities, (n_clust, 1))
        df = pdist(fids)

        T = T_step * (Ti + 1)
        plt.figure()
        plt.scatter(all_dists, df, s=1.0)
        # plt.title("T/T_QSL= {:03.2f}".format(T))
        plt.xlabel("dist(c_i, cj)/d_max")
        plt.ylabel("abs(F_i-F_j)")
        # plt.xlim([0, 1])
        # plt.savefig("dist_df_n{}p{}b{}Ti{}.png".format(n, p, b, Ti), format="png")
        plt.show()


def all_controls():
    # Get files
    file_list = get_filenames(load_path, base="spinchain", L=L, J=J, g=g, Ti=148)

    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):

        all_dists = pdist(ctrls)
        max_dist = np.sqrt(ctrls.shape[1] * range_width ** 2)
        all_dists = all_dists / max_dist
        fids = np.reshape(fids, (n_seeds, 1))
        df = pdist(fids)

        T = T_step * (Ti + 1)
        plt.figure()
        plt.scatter(all_dists, df, s=1.0)
        # plt.title("T/T_QSL= {:03.2f}".format(T))
        plt.xlabel("$\\|\\|c_i^\\ast- c_j^\\ast\\|\\|/D_{max}$")
        plt.ylabel("$|F(c_i^\\ast)-F(c_j^\\ast)|$")
        plt.xlim([0, 1])
        plt.savefig("spinchain_dist_df_L{}J{}g{}Ti{}.png".format(L, J, g, Ti), format="pdf")
        plt.show()


def __run__():
    mode = "all_controls"
    if mode == "clusters":
        clusters()
    else:
        all_controls()


"""     OUTPUT     """

if __name__ == "__main__":
    __run__()
