"""
Author: Andreas Springborg
Date: 21/04/2019
Goal: Plot fidelity distance as a function of control distance

"""

from pathlib import Path

import numpy as np
from scipy.spatial.distance import pdist

from plotguard import pyplot as plt
from pulseregression.framework.procedure.procedure import field_loader, \
    get_filenames

####  Define  ####

L = 6
J = 1
g = 1

max_ampl = 4

range_width = 2 * max_ampl

n_seeds = 100
n_p = 5

# duration range
n_T0 = 10
n_ts = 200
T_max = 4.0
dt = T_max / n_ts
T_step = T_max / n_T0

# IO
load_path = Path("../../../../data/distance/runs/run4/data")


#################


def __run__():

    alpha = 0.2
    p = 1

    # Get files
    file_list = get_filenames(load_path, base="spinsystems", L=L, J=J, g=g, a=alpha, p=p)

    for (Ti, ctrls, fids) in field_loader(file_list, ["ctrls", "fids"]):

        centers = get_cluster_centres(ctrls, fids, max_ampl, window_length=3)



        all_dists = pdist(ctrls)
        max_dist = np.sqrt(ctrls.shape[1] * range_width ** 2)
        all_dists = all_dists / max_dist
        fids = np.reshape(fids, (len(fids), 1))
        df = pdist(fids)

        T = T_step*(Ti + 1)
        plt.figure()
        plt.scatter(all_dists, df, s=1.0)
        plt.title("T/T_QSL= {:03.2f}".format(T))
        plt.xlabel("dist(c_i, cj)/d_max")
        plt.ylabel("abs(F_i-F_j)")
        #plt.xlim([0, 1])
        #plt.savefig("dist_df_n{}p{}b{}Ti{}.png".format(n, p, b, Ti), format="png")
        plt.show()


"""     OUTPUT     """

if __name__ == "__main__":
    __run__()
