"""
Author: Andreas Springborg
Date: 20/01/2019
Goal: Calculate metrics

"""
from logging import warning
import numpy as np
from qutip import Qobj
from scipy.spatial.distance import pdist
import scipy.linalg as la

from pulseregression.framework.clustering.kmeans import get_cluster_centres
from pulseregression.framework.control import clean_controls, stretch
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.fidelity import interpolate_for_tmin
from pulseregression.framework.procedure import field_loader, select_files
from pulseregression.framework.optim.optimization import calculate_drift_fidelity, calculate_fidelity, \
    get_optimizer_function, run_grape
from pulseregression.framework.termination.termination import get_default_termination_conditions
from pulseregression.framework.utility import calculate_order_metric, count_num_unique, \
    calculate_bangbang_fraction, calculate_bangbangness, calculate_jitter, calculate_mean_magnetization, stack
from pulseregression.framework.visual import display_distance_distribution


def get_optimal_fidelities(file_iterator, n_T):
    """ Extract a vector of optimal fidelities """

    opt_fidelities = np.zeros((n_T,))
    for (Ti, fids) in field_loader(file_iterator, "fids"):
        opt_fidelities[Ti] = np.max(fids)
    return opt_fidelities


def get_drift_fidelities(H_d: Qobj, U_0: Qobj, U_targ: Qobj, dt: float, T_max: float):
    """
    Calculate the fidelity at each timestep from 0 to T_max
    :param H_d: Drift Hamiltonian
    :param U_0: The intial unitary
    :param U_targ: The target unitary
    :param dt: the timestep
    :param T_max: The final time
    :return: Array of fidelities for each timestep
    """
    is_ket = U_targ.isket

    U = U_0.full().copy()
    n_t = int(np.rint(T_max / dt))
    H = H_d.full().copy()
    U_t = U_targ.full().copy()
    fidelities = np.zeros((n_t,))

    for i in range(n_t):
        fidelities[i] = calculate_fidelity(U, U_t, sts=is_ket)
        prop = la.expm(-1j * H * dt)
        U = np.dot(prop, U)
    return fidelities


def get_analytical_drift_fidelities(times, H_d, U_targ):
    """ Calculates the unitary drift evolution of 2x2 problems

        Assumes U_0 = eye

    """
    drift_fidelities = np.zeros(times.shape)
    for i in range(len(drift_fidelities)):
        U = np.cos(times[i]) * np.eye(H_d.shape[0]) - 1j * np.sin(times[i]) * H_d.full()
        drift_fidelities[i] = calculate_fidelity(U, U_targ.full())
    return drift_fidelities


def get_optim_drift_fidelities(H_d, U_0, U_targ, dt, T_max):
    """
    Calculate the fidelity at each timestep from 0 to T_max
    :param H_d: Drift Hamiltonian
    :param U_0: The intial unitary
    :param U_targ: The target unitary
    :param dt: the timestep
    :param T_max: The final time
    :return: Array of fidelities for each timestep
    """
    global drift1
    n_t = int(T_max // dt)
    fidelities = np.zeros((n_t,))
    optim_gen = get_optimizer_function(H_d, 0 * H_d, U_0, U_targ, dt)

    for i in range(n_t):
        optim = optim_gen((i + 1) * dt)
        fidelities[i] = calculate_drift_fidelity(optim)
        drift1.append(optim.dynamics.fwd_evo[-1].full().copy())

    return fidelities


def get_order_metric(file_iterator, n_T, max_amp=1.0, clean=True):
    q = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, "ctrls"):
        if clean:
            ctrls = clean_controls(ctrls)
        q[Ti] = calculate_order_metric(ctrls, max_amp=max_amp)
    return q


def get_mean_abs_magnetization(file_iterator, n_T, max_amp=1.0, clean=True):
    q = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, "ctrls"):
        if clean:
            ctrls = clean_controls(ctrls)
        q[Ti] = calculate_mean_magnetization(ctrls, max_amp=max_amp)
    return q


def get_uniqueness_fraction(file_iterator, n_T, n_seeds, threshold=1.0):
    q = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, "ctrls"):
        q[Ti] = count_num_unique(ctrls, threshold) / n_seeds
    return q


def plot_distance_distribution(file_iterator, n_T, T_min, n, p, b, field='ctrls', normalize=False, range_width=2.0,
                               close_after_plotting=False, filter=None):
    for (Ti, ctrls) in field_loader(file_iterator, field):
        # ctrls = clean_controls(ctrls)
        if ctrls.shape[0] == 1:
            continue
        all_dists = pdist(ctrls)
        if normalize:
            norm = np.sqrt(ctrls.shape[1] * range_width ** 2)
        else:
            norm = 1.0
        display_distance_distribution(Ti, all_dists, n_step=n_T, T_min=T_min, norm=norm, n=n, p=p, b=b,
                                      close_after_plotting=close_after_plotting, filter=filter)


def get_mean_distance(file_iterator, n_T, field="ctrls", metric="euclidean", normalize=False, max_amp=1.0, clean=True):
    d = np.zeros((n_T,))
    max_dist = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, field):
        if clean:
            ctrls = clean_controls(ctrls)
        if ctrls.shape[0] == 1:
            d[Ti] = 0
            continue
        max_dist[Ti] = np.sqrt(ctrls.shape[1] * (2 * max_amp) ** 2)
        if metric != "euclidean":
            warning("Normalization only works for Euclidean distance")
        all_dists = pdist(ctrls, metric)

        d[Ti] = np.mean(all_dists)
    if normalize:
        d = d / max_dist
    return d


def get_bangbang_fraction(file_iterator, n_T, threshold=0, field="ctrls", max_amp=1.0, clean=True):
    d = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, field):
        if clean:
            ctrls = clean_controls(ctrls)
        d[Ti] = calculate_bangbang_fraction(ctrls, threshold=threshold, max_amp=max_amp)
    return d


def get_mean_bangbangness(file_iterator, n_T, field="ctrls", max_amp=1.0, clean=True):
    B = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, field):
        if clean:
            ctrls = clean_controls(ctrls)
        B[Ti] = np.mean(calculate_bangbangness(ctrls, max_amp=max_amp))
    return B


def get_mean_jitter(file_iterator, n_T, field="ctrls", max_amp=1.0, clean=True):
    J = np.zeros((n_T,))
    for (Ti, ctrls) in field_loader(file_iterator, field):
        if clean:
            ctrls = clean_controls(ctrls)
        J[Ti] = np.mean(calculate_jitter(ctrls, max_amp=max_amp))
    return J


def get_metrics(file_list, n_T, metrics, modify_ctrls_fct=None, uniquefy=False, prefunc_kwargs=None, **kwargs):
    """ Loop through files and calculate each metric on each data"""
    return get_metrics_from_input_iterator(field_loader(file_list, ["seeds", "ctrls", "fids"]),
                                           n_T,
                                           metrics,
                                           modify_ctrls_fct,
                                           uniquefy,
                                           prefunc_kwargs,
                                           **kwargs)


def get_metrics_from_input_iterator(input_iterator, n_T, metrics, modify_ctrls_fct=None, uniquefy=False,
                                    prefunc_kwargs=None, **kwargs):
    """ Loop through files and calculate each metric on each data"""
    n_metrics = len(metrics)
    out = np.zeros((n_metrics, n_T))
    out[:] = np.nan

    if prefunc_kwargs is None:
        prefunc_kwargs = {}

    for (Ti, seeds, ctrls, fids) in input_iterator:
        if modify_ctrls_fct is not None:
            vargout = modify_ctrls_fct(ctrls, fids, **prefunc_kwargs)
            if isinstance(vargout, tuple):
                ctrls = vargout[0]
            else:
                ctrls = vargout

        for i, metric in enumerate(metrics):
            if uniquefy:
                ctrls, fids = clean_controls(ctrls, fids=fids)
            out[i, Ti] = metric(seeds, ctrls, fids, kwargs)
    return out


def gather_fidelities(file_list, n_T, n_seeds, continuous_index=False, duration_tags=None, return_Ti=False):
    all_fids = np.zeros((n_T, n_seeds))
    indices = np.zeros((n_T,))
    for i, d in zip(range(n_T), field_loader(file_list, "fids", duration_tags=duration_tags)):
        Ti, fids = d
        indices[i] = Ti
        if continuous_index:
            all_fids[i, :] = fids
        else:
            all_fids[Ti, :] = fids
    if return_Ti:
        return all_fids, indices
    return all_fids


def compute_mean_data(file_list,
                      n_p,
                      T_step,
                      max_ampl,
                      n_seeds,
                      F_QSL,
                      metric_fcts,
                      modify_ctrls_fct,
                      save_file,
                      problem_fct,
                      dt,
                      T_last):
    all_metrics = []
    all_fidelities = []
    all_drift_fidelities = []
    all_tqsls = []
    for p in range(n_p):
        p_tag = "p_{}".format(p)
        files = [file for file in file_list if p_tag in str(file)]
        n_T = len(files)
        T_range = T_step * np.arange(1, n_T + 1)
        metrics = get_metrics(files,
                              n_T,
                              metric_fcts,
                              modify_ctrls_fct=modify_ctrls_fct,
                              max_amp=max_ampl,
                              normalize_distance=True,
                              bb_threshold=0,
                              prefunc_kwargs={"amp_max": max_ampl}
                              )
        fidelities = gather_fidelities(files, n_T, n_seeds)
        all_tqsls.append(interpolate_for_tmin(T_range, fidelities, F_QSL))

        H_d, H_c, psi_0, psi_targ = problem_fct(p)
        drift_fidelities = get_drift_fidelities(H_d, psi_0, psi_targ, dt, T_last)

        all_drift_fidelities.append(drift_fidelities)
        all_fidelities.append(fidelities)
        all_metrics.append(metrics)

    metrics = stack(all_metrics, fill_val=np.nan, concatenate=False)
    drift_matrix = np.stack(all_drift_fidelities, axis=0)
    drift_fidelities = np.mean(drift_matrix, axis=0)
    mean_metrics = np.nanmean(metrics, axis=2)
    mean_metrics[np.isnan(mean_metrics)] = 0
    mean_qsl = np.nanmean(all_tqsls)
    np.savez(save_file, mean_metrics=mean_metrics, mean_qsl=mean_qsl, drift_fidelities=drift_fidelities,
             drift_matrix=drift_matrix)
    return mean_metrics, mean_qsl, drift_fidelities, drift_matrix


def load_mean_data(save_file):
    f = np.load(save_file)
    return f["mean_metrics"], f["mean_qsl"], f["drift_fidelities"], f["drift_matrix"]


def get_mean_data(save_file, *compute_args, force_renew=False):
    try:
        if force_renew:
            raise IOError("Forced to renew data source")
        return load_mean_data(save_file)
    except IOError:
        return compute_mean_data(*compute_args)


def cluster_controls(ctrls, fids, amp_max, filt_val=0.4):
    ctrls = apply_default_bessel_filter(ctrls, (-amp_max, amp_max), filt_val=filt_val)
    ctrls, _, _ = get_cluster_centres(ctrls, fids, amp_max)
    return ctrls


def get_nearest_ctrl(point, times, file_list, Ts, Fs, ctrls2):
    t0 = point[0]
    print("Clicked time: {}".format(t0))
    i_min = np.argmin(np.abs(times - t0))
    T = times[i_min]
    print("Closest duration is: Ti: {} ~ T={}".format(i_min, T))
    file = select_files(file_list, "Ti_{}".format(i_min))
    print("Selected file: {}".format(file))
    if file:
        ctrls, fids = next(field_loader(file, ["ctrls", "fids"], prepend_Ti=False))
        j = np.argmin(np.abs(fids - point[1]))
        nearest_dist = np.abs(fids[j] - point[1])
        cur_ctrl = ctrls[j, :]
        cur_f = fids[j]
        print("Closest control is number, {}, with fidelity {} at a distance {}".format(j, fids[j], nearest_dist))
    else:
        print("No original file at that point!")
        nearest_dist = 2.0

    new_points = [(t, f, c) for t, f, c, in zip(Ts, Fs, ctrls2) if np.isclose(t, T)]
    if not new_points:
        return cur_ctrl, cur_f, T
    k = np.argmin([np.abs(e[1] - point[1]) for e in new_points])
    p = new_points[k]
    print("The closest new point is ({}, {})".format(p[0], p[1]))
    if np.abs(p[1] - point[1]) < nearest_dist:
        nearest_f = p[1]
        nearest_c = p[2]
        print("Selecting new point")
    else:
        nearest_f = cur_f
        nearest_c = cur_ctrl
        print("Sticking to old point")
    return nearest_c, nearest_f, T


def default_Ti_to_n_map(Ti):
    return 2 * Ti + 1


def get_optimised_point(event, cur_ctrl, problem, Ti_map=default_Ti_to_n_map):
    n_cur = cur_ctrl.shape[-1]
    ti = np.argmin(np.abs(problem.duration_range - event.xdata))
    print("Optimising for Ti = {}".format(ti))
    T = problem.duration_range[ti]
    optim = problem.generate_optim(T)
    n_next = Ti_map(ti)
    delta_n = n_next - n_cur
    cur_ctrl = stretch(cur_ctrl, delta_n)
    opt_ctrl, opt_fid = run_grape(optim, get_default_termination_conditions(), seed=cur_ctrl)
    print("Got optimised point: ({}, {})".format(T, opt_fid))
    return ti, opt_ctrl, opt_fid, T


def get_fidelity_points(file_list):
    points = [point for point_list in field_loader(file_list, "points", prepend_Ti=False) for point in point_list]
    Tis = [point[0] for point in points]
    Fs = [point[2] for point in points]
    ctrls = [point[1] for point in points]
    return Tis, Fs, ctrls
