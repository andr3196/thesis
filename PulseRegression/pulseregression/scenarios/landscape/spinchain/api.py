"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot phase metrics for a the Bukov spin chain problem

"""

import numpy as np

####  Define  ####
from pulseregression.framework.problems import SpinChainProblem
from pulseregression.framework.quantum import init_spinchain_problem
from pulseregression.framework.utility import calculate_max_fidelity, calculate_order_metric, calculate_mean_distance, \
    calculate_bangbang_fraction, calculate_mean_magnetization
from pulseregression.scenarios.landscape.core import get_metrics, get_drift_fidelities, gather_fidelities

# problem def

scp = SpinChainProblem()

#################


def compute_metrics():
    # Define problem
    H_d, H_c, psi_0, psi_targ = init_spinchain_problem(scp.L, scp.J, scp.g)

    # Define time
    T_range = scp.T_step * np.arange(scp.n_T) + scp.dt

    # Get files
    file_list =  scp.get_load_files()

    # Calculate metrics
    metric_fcts = [
        lambda s, c, f, args: calculate_max_fidelity(f),
        lambda s, c, f, args: calculate_order_metric(c, max_amp=args["max_amp"]),
        lambda s, c, f, args: calculate_mean_distance(c,
                                                      normalize=args["normalize_distance"], max_amp=args["max_amp"]),
        lambda s, c, f, args: calculate_bangbang_fraction(c, violations_th=args["bb_threshold"],
                                                          max_amp=args["max_amp"]),
        lambda s, c, f, args: calculate_mean_magnetization(c, max_amp=args["max_amp"])

    ]

    metrics = get_metrics(file_list,
                          scp.n_T,
                          metric_fcts,
                          max_amp=scp.max_ampl,
                          normalize_distance=True,
                          bb_threshold=0,
                          prefunc_kwargs={"amp_max": scp.max_ampl})

    drift_fidelities = get_drift_fidelities(H_d, psi_0, psi_targ, scp.dt, scp.T_max)
    drift_times = scp.dt * np.arange(drift_fidelities.shape[0])
    fidelities = gather_fidelities(file_list, scp.n_T, scp.n_seeds)

    metrics = metrics[:, ::4]
    T_range = T_range[::4]
    fidelities = fidelities[::4, :]

    np.savez(scp.save_path("metrics")/"spinchain_L_6_J_1_g_1_metrics.npz", metrics=metrics, T_range=T_range, fidelities=fidelities, drift=(drift_times, drift_fidelities))



"""     OUTPUT     """

if __name__ == "__main__":
    mode = "display_metrics"
    if mode == "display_metrics":
        compute_metrics()
