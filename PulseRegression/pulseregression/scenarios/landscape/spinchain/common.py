"""
Author: Andreas Springborg
Date: 2019-05-27
Goal:

"""
import numpy as np

from pulseregression.framework.procedure import get_filenames, field_loader, select_files
from pulseregression.framework.problems import SpinChainProblem

scp = SpinChainProblem()


def load_flip_files():
    return get_filenames(scp.save_path("swapping"), base="spinchain_L_6_J_1_g_1_flipping", sort_tag=None)


def load_point_files():
    return get_filenames(scp.save_path("swapping"), base="interactive_swapping", sort_tag=None)


def load_all_files():
    # Load all files (spinchain, swapping, 2nd swapping)
    file_list = get_filenames(scp.load_path(), base="spinchain")
    file_list1 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, ignore="nnTi")
    file_list2 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, L=scp.L, J=scp.J, g=scp.g,
                               nTi=92)
    file_list3 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, L=scp.L, J=scp.J, g=scp.g,
                               nTi=141)
    return file_list, file_list1, file_list2, file_list3


def data_iterator():
    """ Iterator for spin chain data.

        As the data files have slightly different formats, it is awkward to load optimal controls from different files.
        This iterator yields all data for different time indices. Note that some Ti may be skipped if no data is
        available. Thus, in case no data is available, no exception will be raised.

    :return:
    """
    file_list, file_list1, file_list2, file_list3 = load_all_files()

    all_points = []
    for points in field_loader(load_point_files(), "points", prepend_Ti=False):
        if len(points[0]) == 4:
            points = [(p[0], p[2], p[3]) for p in points]
        all_points.extend(points)
    for points in field_loader(load_flip_files(), "points", prepend_Ti=False):
        if len(points[0]) == 4:
            points = [(p[0], p[2], p[3]) for p in points]
        all_points.extend(points)

    for Ti in np.arange(200):
        files = select_files(file_list, "_Ti_{}.npz".format(Ti))
        files.extend(select_files(file_list1, "_nTi_{}.npz".format(Ti)))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}.npz".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}.npz".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}.npz".format(Ti)))

        if files:
            # all_seeds_list = []
            all_ctrls_list = []
            all_fids_list = []

            for ctrls, fids in field_loader(files, ["ctrls", "fids"], prepend_Ti=False):
                # all_seeds_list.append(seeds)
                all_ctrls_list.append(ctrls)
                all_fids_list.append(fids)
            # seeds = np.concatenate(all_seeds_list, axis=0)
            ctrls = np.concatenate(all_ctrls_list, axis=0)
            fids = np.concatenate(all_fids_list, axis=0)
        else:
            ctrls = None
            fids = None

        Ti_points = [point for point in all_points if point[0] == Ti]

        if Ti_points:
            if len(Ti_points[0]) == 4:
                l,m = 2, 3
            else:
                l,m = 1, 2

            int_ctrls = np.array([p[l] for p in Ti_points])
            int_fids = np.array([p[m] for p in Ti_points])

            if ctrls is not None:
                ctrls = np.concatenate([ctrls, int_ctrls], axis=0)
                fids = np.concatenate([fids, int_fids], axis=0)
            else:
                ctrls = int_ctrls
                fids = int_fids
        if ctrls is not None:
            yield Ti, None, ctrls, fids

