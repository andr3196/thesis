
"""
Author: Andreas Springborg
Date: 2019-06-29
Goal: Display metrics as a function of duration

"""
import numpy as np

from plotguard import pyplot as plt
from pulseregression.framework.fidelity import interpolate_for_tmin
####  Define  ####
from pulseregression.framework.problems import SpinChainProblem
# problem def
from pulseregression.scenarios.landscape.main.core import plot_all
from pulseregression.styling import defaults

scp = SpinChainProblem()

#################


def load_metrics(file):
    f = np.load(file)
    return f["metrics"], f["T_range"], f["fidelities"], f["drift"]


def display_metrics():

    metrics, T_range, fidelities, drift = load_metrics(scp.save_path("metrics")/"spinchain_L_6_J_1_g_1_metrics.npz")

    # Gather metrics
    labels = ["Max fidelity", "Order parameter",
              "Mean distance", "Bang-bang fraction", "Magnetisation",
              "Drift fidelities"]
    axis_labels = ["Duration, T", ""]

    metrics_linestyles = {3: {"linestyle": "-", "color": defaults.colors[9]},
                          4: {"linestyle": "-", "color": defaults.colors[4]}}
    T_qsl = interpolate_for_tmin(T_range, fidelities, scp.fqsl)

    plt.figure(figsize=[6.4, 6.0])

    # Plot figure
    plot_all(T_range,
             metrics,
             metrics_linestyles,
             drift=drift,
             dots=[fidelities],
             vlines=[{"x": T_qsl, "ymin": 0, "ymax": scp.fqsl, "colors": 'k', 'linestyles': "-.", 'label': '$T_{min}$'}],
             dots_labels=["Individual fidelities"],
             out=None,
             plot_labels=labels,
             axis_labels=axis_labels,
             legend_loc="below",
             legend_offset=(0.5, -0.2),
             legend_point="upper center"
             )
    plt.subplots_adjust(top=0.9)

    plt.annotate('$T_c^{(1)}$', xy=(0.4, 1.0), xytext=(0.4, 1.15), annotation_clip=False,
                 arrowprops=dict(facecolor='black', width=2, headwidth=6, headlength=10), fontsize=16)
    plt.annotate('$T_b$', xy=(1.27, 1.0), xytext=(1.27, 1.15), annotation_clip=False,
                 arrowprops=dict(facecolor='black', width=2, headwidth=6, headlength=10), fontsize=16)
    defaults.set_ticksize(plt.gca())
    plt.show()



"""     OUTPUT     """

if __name__ == "__main__":
    mode = "display_metrics"
    if mode == "display_metrics":
        display_metrics()
