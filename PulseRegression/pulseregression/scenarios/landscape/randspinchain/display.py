"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot phase metrics for a spin glass problem

"""

import numpy as np

from plotguard import pyplot as plt
from pulseregression.framework.problems import RandomSpinChainProblemScan
from pulseregression.framework.utility import calculate_max_fidelity, calculate_mean_distance, \
    calculate_quantile, calculate_min_fidelity
from pulseregression.scenarios.landscape.core import load_mean_data
from pulseregression.scenarios.landscape.main.core import plot_all
####  Define  ####
from pulseregression.styling import defaults

alphas = np.linspace(0, 2, 6)

randSCPScan = RandomSpinChainProblemScan(alphas)
n_seeds = 100
n_p = 5
T_last = 8.0
save_filename_basis = "randspinchain_mean_metrics_a_{:1.1f}.npz"

#################


# Define metrics
metric_fcts = [
    lambda s, c, f, args: calculate_min_fidelity(f),
    lambda s, c, f, args: calculate_quantile(f, 0.25),
    lambda s, c, f, args: calculate_quantile(f, 0.75),
    lambda s, c, f, args: calculate_max_fidelity(f),
    lambda s, c, f, args: calculate_mean_distance(c,
                                                  normalize=args["normalize_distance"],
                                                  max_amp=args["max_amp"], single_ctrl_value=0)
]


def display_scan():
    fig, ax_array = plt.subplots(3, 2, figsize=(6.4, 7.2))
    y_labels = ["" for _ in range(len(alphas))]
    x_labels = ["Duration, T" if i > 3 else "" for i in range(len(alphas))]

    for i, rscp in enumerate(randSCPScan):

        ax = ax_array.flat[i]

        # File to load
        save_file = rscp.save_path() / save_filename_basis.format(rscp.alpha)

        mean_metrics, mean_qsl, drift_fidelities, drift_matrix = load_mean_data(save_file)

        # Plot the first 3 rows as shaded areas
        fidelity_regions = [(mean_metrics[0, :], mean_metrics[3, :], "Above min fidelity"),
                            (mean_metrics[1, :], mean_metrics[3, :], "Above 1st quartile"),
                            (mean_metrics[2, :], mean_metrics[3, :], "Above median")]

        # Plot the remaining as lines
        mean_metrics = mean_metrics[3:, :]

        labels = ["Max fidelity", "Mean distance"]

        T_range = rscp.get_T(np.arange(0, mean_metrics.shape[1]))

        # Resolve drift
        drift_times = np.linspace(rscp.dt, T_last, drift_matrix.shape[1])

        # Place legend below 4th subplot
        if i == 4:
            loc = "below"
        else:
            loc = None

        # Define colors for different metrics
        metrics_linestyles = {0: {"color": defaults.colors[0]},
                              1: {"color": defaults.colors[2]},
                              2: {"color": defaults.colors[4]},
                              3: {"color": defaults.colors[7]},
                              4: {"color": defaults.colors[6]},
                              }

        # Plot figure
        plot_all(T_range,
                 mean_metrics,
                 metric_kwargs=metrics_linestyles,
                 drift=(drift_times, drift_matrix),
                 drift_label="Drift fidelity",
                 vlines=[{"x": mean_qsl, "ymin": 0, "ymax": rscp.fqsl, "color": 'k', 'linestyle': "-.",
                          'label': '$T_{min}$'}],
                 title="$\\alpha$ = {:1.1f}".format(rscp.alpha),
                 out=None,
                 plot_labels=labels,
                 ax=ax,
                 axis_labels=(x_labels[i], y_labels[i]),
                 legend_loc=loc,
                 legend_offset=(1.05, -0.4),
                 legend_point="upper center",
                 fidelity_regions=fidelity_regions,
                 options={"set_xlim": ((0, 7.0), {})}
                 )

    plt.subplots_adjust(hspace=0.4)
    # systems.save_figure(plt, "randspinchain_problem_metrics_L_{}_J_{}_g_{}_clust.pdf".format(rscp.L, rscp.J, rscp.g))
    plt.show()


if __name__ == "__main__":
    mode = 'mean'

    if mode == 'mean':
        display_scan()
    else:
        raise ValueError("Unknown mode: {}".format(mode))
