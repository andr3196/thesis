"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot phase metrics for a spin glass problem

"""

import numpy as np

from pulseregression.framework.problems import RandomSpinChainProblemScan
from pulseregression.framework.utility import calculate_max_fidelity, calculate_mean_distance, \
    calculate_quantile, calculate_min_fidelity
from pulseregression.scenarios.landscape.core import cluster_controls, compute_mean_data

# problem def

####  Define  ####

alphas = np.linspace(0, 2, 6)

randSCPScan = RandomSpinChainProblemScan(alphas)
n_seeds = 100
n_p = 5
# The maximum duration of the step_through
T_last = 8.0

save_filename_basis = "randspinchain_mean_metrics_a_{:1.1f}.npz"

#################


# Define metrics
metric_fcts = [
    lambda s, c, f, args: calculate_min_fidelity(f),
    lambda s, c, f, args: calculate_quantile(f, 0.25),
    lambda s, c, f, args: calculate_quantile(f, 0.75),
    lambda s, c, f, args: calculate_max_fidelity(f),
    lambda s, c, f, args: calculate_mean_distance(c,
                                                  normalize=args["normalize_distance"],
                                                  max_amp=args["max_amp"], single_ctrl_value=0)
]


def compute_scan():

    for i, rscp in enumerate(randSCPScan):

        # Get files
        file_list = rscp.get_load_files()
        save_file = rscp.save_path() / save_filename_basis.format(rscp.alpha)

        args = [file_list,  # files to collect data from
                n_p, rscp.T_step,
                rscp.max_ampl, n_seeds,
                rscp.fqsl, metric_fcts,
                cluster_controls,  # Function to apply to each control before calculating metrics
                save_file,
                rscp.problem_fct,  # A function that, given a p, produces: Hd, Hc, init, targ
                rscp.dt, T_last]

        compute_mean_data(*args)


if __name__ == "__main__":
    mode = 'mean'

    if mode == 'mean':
        compute_scan()
    else:
        raise ValueError("Unknown mode: {}".format(mode))
