"""
Author: Andreas Springborg
Date: 22/12/2018
Goal:

"""
from pathlib import Path
import multiprocessing as mp
import numpy as np
from matplotlib import gridspec as gspc

from plotguard import pyplot as plt
from pulseregression import systems
from pulseregression.framework.control import stretch
from pulseregression.framework.optim.optimization import run_grape
from pulseregression.framework.procedure import field_loader, repetition_guard, get_filenames
from pulseregression.framework.termination.termination import get_default_termination_conditions
from pulseregression.framework.utility import calculate_max_fidelity, calculate_mean_distance, \
    calculate_bangbang_fraction, calculate_mean_magnetization, pdist
from pulseregression.scenarios.landscape.core import gather_fidelities, get_optimised_point, get_nearest_ctrl, get_fidelity_points, get_metrics_from_input_iterator, get_drift_fidelities
from pulseregression.scenarios.landscape.main.common import data_iterator
from pulseregression.scenarios.landscape.main.core import plot_all
from pulseregression.framework.problems import RandomHamiltonianProblem, ScanProperties

####  Define  ####
from pulseregression.styling import defaults

sp = ScanProperties(T_step=0.2)
rhp = RandomHamiltonianProblem(n=4, p=1, b=3.0, scanproperties=sp)

# data shape

n_T_dict = {0: 121, 1: 94}

n_T = n_T_dict[rhp.p]

T_range = rhp.get_T(np.arange(n_T)) / rhp.T_QSL

# IO
load_path = Path("../../../../data/distance/runs/run2/data")


#################

def plot_metrics():
    # Define problem
    H_d, H_c, psi_0, psi_targ = rhp.problem

    # Get files
    file_list = rhp.get_load_files()

    # Calculate metrics
    metric_fcts = [
        lambda s, c, f, args: calculate_max_fidelity(f),
        lambda s, c, f, args: calculate_mean_distance(c,
                                                      normalize=args["normalize_distance"], max_amp=args["max_amp"]),
        lambda s, c, f, args: calculate_bangbang_fraction(c, violations_th=0, max_amp=args["max_amp"]),
        lambda s, c, f, args: calculate_mean_magnetization(c, max_amp=args["max_amp"])
    ]

    metrics = get_metrics_from_input_iterator(data_iterator(rhp, n_T),
                                              n_T,
                                              metric_fcts,
                                              # modify_ctrls_fct=cluster_controls,
                                              normalize_distance=True,
                                              max_amp=rhp.max_ampl,
                                              prefunc_kwargs={"amp_max": rhp.max_ampl})

    drift_fidelities = get_drift_fidelities(H_d, psi_0, psi_targ, rhp.dt, rhp.T_max)
    drift_times = rhp.dt * np.arange(drift_fidelities.shape[0]) / rhp.T_QSL
    # metrics = np.concatenate((metrics, drift_fidelities[np.newaxis, :]), axis=0)

    # Gather metrics
    labels = ["Max fidelity",
              "Mean distance", "Bang-bang fraction", "Magnetisation",
              "Drift fidelities"]
    axis_labels = ["$T/T_{min}$", ""]

    metrics_linestyles = {0: {"color": defaults.colors[0]},
                          1: {"color": defaults.colors[2]},
                          2: {"color": defaults.colors[3]},
                          3: {"color": defaults.colors[4]},
                          4: {"color": defaults.colors[6]},
                          }

    # Plot figure
    plot_all(T_range,
             metrics,
             metric_kwargs=metrics_linestyles,
             drift=(drift_times, drift_fidelities),
             out=None,
             plot_labels=labels,
             axis_labels=axis_labels,
             legend_loc="below",
             legend_offset=(0.5, -0.35),
             legend_point="upper center"
             )

    # systems.save_figure(plt, "run_n4p0b3_metrics")

    plt.show()


def plot_fidelities():
    # Get files
    file_list = rhp.get_load_files()

    fidelities = gather_fidelities(file_list, n_T, rhp.n_seeds)
    interactive_files = get_filenames(rhp.save_path("swapping"), base="interactive_points", sort_tag=None)
    interactive_files2 = get_filenames(rhp.save_path("swapping"), base="interactive_swapping", sort_tag=None)
    all_points = []
    for points in field_loader(interactive_files, "points", prepend_Ti=False):
        all_points.extend(points)
    for points in field_loader(interactive_files2, "points", prepend_Ti=False):
        all_points.extend(points)

    print([p for p in all_points if p[-1] > 0.999])
    Ts = [rhp.get_T(p[0]) for p in all_points]
    Fs = [p[2] for p in all_points]

    all_data = [(Ti, c, f) for Ti, _, c, f in data_iterator(rhp, n_T)]

    backprop_file = Path("./backprop_path.npz")

    if backprop_file.exists():
        f = np.load(backprop_file)
        opt_path = f["path"]
    else:
        Ti_opt = 59
        _, c_start, f_start = all_data[Ti_opt]
        i_max = np.argmax(f_start)
        c_max = c_start[i_max, :]
        f_max = f_start[i_max]

        opt_path = [(Ti_opt, c_max, f_max)]

        for step in np.arange(1, Ti_opt - 1):
            c_last = stretch(opt_path[-1][1], -2)
            optim = rhp.generate_optim(rhp.get_T(Ti_opt - step))
            c_opt, f_opt = run_grape(optim, seed=c_last)
            opt_path.append((Ti_opt - step, c_opt, f_opt))
        np.savez(backprop_file, path=opt_path)

    forward_file = Path("./forwardprop_path.npz")

    if forward_file.exists():
        f = np.load(forward_file)
        forward_path = f["path"]
    else:
        Ti_back = -10

        forward_path = [opt_path[Ti_back]]
        Ti_begin = opt_path[Ti_back][0]

        for Ti in np.arange(Ti_begin + 1, Ti_opt + 1):
            c_last = stretch(forward_path[-1][1], 2)
            optim = rhp.generate_optim(rhp.get_T(Ti))
            c_opt, f_opt = run_grape(optim, seed=c_last)
            forward_path.append((Ti, c_opt, f_opt))
        np.savez(forward_file, path=forward_path)

    other_file = Path("./otherprop_path.npz")

    if other_file.exists():
        f = np.load(other_file)
        other_path = f["path"]
    else:
        Ti_other = -3
        Ti_opt = 59
        other_path = [opt_path[Ti_other]]
        Ti_begin = opt_path[Ti_other][0]

        for Ti in np.arange(Ti_begin + 1, Ti_opt + 10):
            c_last = stretch(other_path[-1][1], 2)
            optim = rhp.generate_optim(rhp.get_T(Ti))
            c_opt, f_opt = run_grape(optim, seed=c_last)
            other_path.append((Ti, c_opt, f_opt))
        np.savez(other_file, path=other_path)

    other_file2 = Path("./otherprop2_path.npz")

    if other_file2.exists():
        f = np.load(other_file2)
        other_path2 = f["path"]
    else:
        Ti_other = -1
        Ti_opt = 59
        other_path2 = [opt_path[Ti_other]]
        Ti_begin = opt_path[Ti_other][0]

        for Ti in np.arange(Ti_begin + 1, Ti_opt + 10):
            c_last = stretch(other_path2[-1][1], 2)
            optim = rhp.generate_optim(rhp.get_T(Ti))
            c_opt, f_opt = run_grape(optim, seed=c_last)
            other_path2.append((Ti, c_opt, f_opt))
        np.savez(other_file2, path=other_path2)

    # End file
    endprop_file = Path("./endbackprop_path.npz")

    if endprop_file.exists():
        f = np.load(endprop_file)
        end_path = f["path"]
    else:
        Ti_opt = 79
        _, c_start, f_start = all_data[Ti_opt]
        i_max = np.argmax(f_start)
        c_max = c_start[i_max, :]
        f_max = f_start[i_max]

        end_path = [(Ti_opt, c_max, f_max)]

        for step in np.arange(1, Ti_opt - 1):
            c_last = stretch(end_path[-1][1], -2)
            optim = rhp.generate_optim(rhp.get_T(Ti_opt - step))
            c_opt, f_opt = run_grape(optim, seed=c_last)
            end_path.append((Ti_opt - step, c_opt, f_opt))
        np.savez(endprop_file, path=end_path)

    fw_cs = [p[1] for p in forward_path]
    bw_cs = [p[1] for p in opt_path[:-9]]

    Ti0 = forward_path[0][0]
    Ti_opt = 59

    fig, axes = plt.subplots(2, 1, sharex=True)

    ax = axes[0]

    for f in fidelities.T:
        ax.scatter(T_range, f, s=1, c="b")
    ax.scatter(T_range[0], f[0], s=1, c="b", label="Fidelities from step_through")
    ax.scatter(np.array(Ts) / rhp.T_QSL, Fs, s=1, c="g", label="Fidelities from propagation")

    ax.set_ylabel("Fidelity, $F$", fontsize=defaults.fontsize)
    ax = axes[1]

    # Opt path
    Ts_opt = [rhp.get_T(p[0]) for p in opt_path]
    Fs_opt = [p[2] for p in opt_path]

    fw_Ts_opt = [rhp.get_T(p[0]) for p in forward_path]
    fw_Fs_opt = [p[2] for p in forward_path]

    ax.scatter(np.array(Ts_opt) / rhp.T_QSL, Fs_opt, s=14, facecolors='none', edgecolors=defaults.colors[0], marker="<")
    ax.scatter(np.array(fw_Ts_opt) / rhp.T_QSL, fw_Fs_opt, s=14, facecolors='none', edgecolors=defaults.colors[2],
               marker=">")

    # Other path
    Ts_other = [rhp.get_T(p[0]) for p in other_path]
    Fs_other = [p[2] for p in other_path]
    ax.scatter(np.array(Ts_other) / rhp.T_QSL, Fs_other, s=14, facecolors='none', edgecolors=defaults.colors[1],
               marker=">")

    # Other path 2
    Ts_other = [rhp.get_T(p[0]) for p in other_path2]
    Fs_other = [p[2] for p in other_path2]
    ax.scatter(np.array(Ts_other) / rhp.T_QSL, Fs_other, s=14, facecolors='none', edgecolors=defaults.colors[3],
               marker=">")

    # End path
    Ts_other = [rhp.get_T(p[0]) for p in end_path]
    Fs_other = [p[2] for p in end_path]
    ax.scatter(np.array(Ts_other) / rhp.T_QSL, Fs_other, s=14, facecolors='none', edgecolors=defaults.colors[4],
               marker="<")

    ax.set_xlabel("$T/T_{min}$", fontsize=defaults.fontsize)
    ax.set_ylabel("Fidelity, $F$", fontsize=defaults.fontsize)

    #systems.save_figure(plt, "run_n4p0b3_fidelities")

    plt.show()


def plot_timecorrelation():
    # Load paths
    paths = []
    filenames = ["./backprop_path.npz", "./forwardprop_path.npz", "./otherprop_path.npz", "./otherprop2_path.npz"]
    direction = [-1, 1, 1, 1]
    for i, fn in enumerate(filenames):
        pth = np.load(Path(fn))["path"]
        pth = pth[::direction[i]]
        paths.append(pth)

    # Load controls
    all_ctrls = []

    Ti_opt = 59

    for path in paths:
        k = 0
        ctrls = np.zeros((Ti_opt + 1, 2 * (Ti_opt + 1) + 1)) * np.nan
        for Ti in np.arange(Ti_opt + 1):
            p = path[k]
            if p[0] == Ti:
                ctrls[Ti, :len(p[1])] = p[1]
                k += 1
                print(Ti, p[2])
        all_ctrls.append(ctrls)

    T0 = rhp.get_T(0) / rhp.T_QSL
    Ti_opt = 59
    T_opt = rhp.get_T(Ti_opt) / rhp.T_QSL

    #T_tick = np.linspace(T0, T_opt, 5)
    #T_ticklabels = ["{:1.1f}".format(ti) for ti  ]


    T_low = 0.05 / rhp.T_QSL



    colors = [0, 2, 1, 3]
    markes = ["<", ">", ">", ">"]

    figure_names = ["backward", "forward", "other1", "other2"]

    for j, ctrls in enumerate(all_ctrls):
        plt.figure()
        ax = plt.subplot(111)
        cax = plt.imshow(ctrls, aspect="auto", cmap="jet", extent=[T0, T_opt, T_opt, T0])
        #ax.set_yticks([T0, T_opt])
        #ax.set_yticklabels(, fontsize=defaults.fontsize)
        plt.xlabel('Time, $t/T_{min}$', fontsize=defaults.latexfontsize)
        plt.ylabel('Duration, $T/T_{min}$', fontsize=defaults.latexfontsize)
        plt.xlim(0.004273504273504274, 1.0170940170940173)
        plt.ylim(1.0170940170940173, 0.008547008547008548)
        p = plt.scatter(100, 10, s=14, facecolors=defaults.colors[colors[j]], edgecolors=defaults.colors[colors[j]],
               marker=markes[j])
        if j == 1:
            bar = plt.colorbar(cax)
            bar.set_label("Control amplitude", fontsize=defaults.fontsize)
        plt.legend([p], ["Trajectory"], fontsize=defaults.fontsize, markerscale=3)
        defaults.set_ticksize(plt.gca())
        #plt.title("From short time to $T_{min}$")
        systems.save_figure(plt, "run_control_propagation_n4p0b3_" + figure_names[j])

    plt.show()


def simple_plot():
    # check to see if savefile is available
    savefile = rhp.save_path("swapping") / "interactive_swapping_n_{}_p_{}_b_{}_run_7.npz".format(rhp.n, rhp.p, rhp.b)
    repetition_guard(savefile)

    # Get files
    file_list = rhp.get_load_files()

    # Get previously set points
    file_list2 = get_filenames(rhp.save_path("swapping"), base="interactive_points", sort_tag=None)
    Tis2, Fs2, ctrls2 = get_fidelity_points(file_list2)
    file_list3 = get_filenames(rhp.save_path("swapping"), base="interactive_swapping", sort_tag=None)
    Tis3, Fs3, ctrls3 = get_fidelity_points(file_list3)
    Tis2.extend(Tis3), Fs2.extend(Fs3), ctrls2.extend(ctrls3)

    times = rhp.get_T(np.arange(n_T))
    Ts2 = rhp.get_T(Tis2)

    fidelities = gather_fidelities(file_list, n_T, rhp.n_seeds)

    fig = plt.figure(figsize=(14, 8))
    outer = gspc.GridSpec(2, 1, height_ratios=[4, 1])
    top = gspc.GridSpecFromSubplotSpec(1, 1,
                                       subplot_spec=outer[0])
    ax1 = plt.Subplot(fig, top[0, 0])

    selected_point = None
    cur_ctrl = None

    opt_points = []

    def pick_event(entity, event):
        nonlocal selected_point, cur_ctrl
        if selected_point is None:
            selected_point = (event.xdata, event.ydata)
            cur_ctrl, f_init, t_init = get_nearest_ctrl(selected_point, times, file_list, Ts2, Fs2, ctrls2)
            ax1.plot(t_init, f_init, 'r.')
            plt.draw()
            plt.pause(0.0001)
        else:
            ti, opt_ctrl, opt_fid, T = get_optimised_point(event, cur_ctrl, rhp)
            opt_points.append((ti, opt_ctrl, opt_fid))
            ax1.plot(T, opt_fid, 'g.')
            plt.draw()
            plt.pause(0.0001)
            selected_point = None

    for i, f in enumerate(fidelities):
        ax1.scatter(times[i] * np.ones(f.shape), f, s=1, c="b", picker=pick_event)
    ax1.scatter(Ts2, Fs2, s=1, c="g")

    fig.add_subplot(ax1)

    bottom = gspc.GridSpecFromSubplotSpec(1, 1,
                                          subplot_spec=outer[1])
    n_bins = 100
    M = np.zeros((n_bins, n_T))
    bin_edges = np.linspace(0, 1, n_bins + 1)
    i = 0
    for Ti, ctrls in field_loader(file_list, "ctrls"):
        dists = pdist(ctrls) / np.sqrt(ctrls.shape[1] * 2 ** 2)
        hists, _ = np.histogram(dists, bin_edges, density=True)
        M[:, i] = hists
        i += 1

    ax = plt.Subplot(fig, bottom[0, 0])
    ax.imshow(M, cmap="jet", aspect="auto")
    fig.add_subplot(ax)
    plt.show()

    input("Ready to save points?")

    np.savez(savefile, points=opt_points)


"""     OUTPUT     """


def check_files():
    files = rhp.get_load_files()

    for Ti, ctrls in field_loader(files, "ctrls"):
        print(Ti, ctrls.shape[1])
    print("interactive points")
    all_points = []
    int_files = get_filenames(rhp.save_path("swapping"), base="interactive_swapping", sort_tag=None)
    for points in field_loader(int_files, "points", prepend_Ti=False):
        all_points.extend(points)
    for p in all_points:
        if len(p) == 3:
            k = 1
        else:
            k = 2
        print(p[0], len(p[k]))


def squeeze_and_optimize(args):
    Ti, ctrl0, optim = args
    new_ctrl = stretch(ctrl0, -1)
    T = rhp.duration_range[Ti]
    opt_ctrl, opt_fid = run_grape(optim, get_default_termination_conditions(), seed=new_ctrl)
    print("Got optimised point: ({}, {})".format(T, opt_fid))
    return Ti, opt_ctrl, opt_fid


def fix_length():
    all_points = []
    int_files = get_filenames(rhp.save_path("swapping"), base="interactive_swapping", sort_tag=None)
    for points in field_loader(int_files, "points", prepend_Ti=False):
        if len(points[0]) == 4:
            points = [(p[0], p[2], p[3]) for p in points]
        all_points.extend(points)

    p = mp.Pool()
    points_to_optimise = [(p[0], p[1], rhp.generate_optim(rhp.get_T(p[0]))) for p in all_points if p[0] < n_T]
    async_out = p.map_async(squeeze_and_optimize, points_to_optimise)

    p.close()
    p.join()
    new_points = async_out.get()
    np.savez(rhp.save_path("swapping") / "interactive_points_n_4_p_0_b_3.0_all", points=new_points)


if __name__ == "__main__":
    mode = "plot_timecorrelation"

    if mode == "simple_plot":
        simple_plot()
    elif mode == "check_files":
        check_files()
    elif mode == "fix_length":
        fix_length()
    elif mode == "plot_fidelities":
        plot_fidelities()
    elif mode == "plot_timecorrelation":
        plot_timecorrelation()
    else:
        plot_metrics()
