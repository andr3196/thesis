"""
Author: Andreas Springborg
Date: 20/01/2019
Goal: Calculate metrics

"""
import itertools
from typing import Union

import numpy as np
from plotguard import pyplot as plt
from pulseregression.styling import defaults

alpha_max = 0.15
alpha_offset = 0.1


#Figsize depends on legend
figsize_params = {"outside": [7.2, 4.8], "below": [6.8, 4.8]}


def get_alpha_range(n):
    if n > 5:
        alphas = np.linspace(alpha_offset, 1.0, n)
    else:
        alphas = alpha_max * np.arange(1, n+1)
    return alphas


def plot_all(x,
             ys,
             metric_kwargs=None,
             drift=None,
             drift_label=None,
             vlines=None,
             dots=None,
             out="show",
             title=None,
             plot_labels=None,
             axis_labels=None,
             dots_labels=None,
             legend_loc=None,
             legend_point=None,
             legend_offset=None,
             fidelity_regions: Union[list, tuple] = None,
             tmin_label = "$T_{min}$",
             qsl=None,
             options=None,
             ax=None,
             fill_regions=None,
             fill_regions_kwargs=None
             ):
    """ Main function for plotting metrics as a function of duration. Highly flexible, but sort of obscure and also a
    mess - sorry.



    :param x: The durations at which to plot th metrics (vector)
    :param ys: The metrics to plot (matrix (n_metrics, n_durations))
    :param metric_kwargs: A dictionary which additional paramters to the plots. Each key in the dict is the index in
     0:n_metrics. Each value is itself a dictionary with key-value pairs provided to plt.plot as kwargs.
    :param drift: The drift fidelity(ies) (tuple or list of tuples. Each tuple consists of the x and y value(s))
    :param drift_label: The label to be used in the legend
    :param vlines: Vertical lines to be plotted. List of dicts, where each dict has the arguments of plt.axvline as key-value pairs
    :param dots: The individual fidelities. Tuple/matrix, list of tuples/matrix. If the data is a tuple the first element
    is assumed to be the durations of the fidelities.
    :param out: None: do nothing after plotting, "show" call plt.show, "view" update figure, else out is assumed to be a
    filename and the figure is saved to this file.
    :param title: The figure title
    :param plot_labels: Labels of metrics. List of strings of the same length as the number of rows in ys.
    :param axis_labels: The x and y axis labels. 2-tuple of strings.
    :param dots_labels: List of labels for dots data. There must 1 label per data set in dots
    :param legend_loc: A matplotlib legend location, "outside" (to the right of the figure) or "below". Will place the
    legend in this position. If None, no legend is plotted.
    :param legend_point: The point on the legend used for the position calculation. String.  See matplotlib legend "loc" argument
    :param legend_offset: See matplotlib legend "bbox_to_anchor". 2-tuple.
    :param fidelity_regions: Only available for backwards compatibility. Use fill_regions instead.
    :param tmin_label: The legend label used for the
    :param qsl: Place a red "x" at the T_min. Tuple (T_min, F_min)
    :param options: Call extra methods. Tuples of (method name, dictionary of parameters)
    :param ax: Plot to this axis instead of default axes
    :param fill_regions: Regions to filled with fill_between. Tuple of list of tuples:
    (durations, lower_bound, upper_bound) or (lower_bound, upper_bound) (use x)
    :param fill_regions_kwargs: Arguments for fill_between, list of dictionaries.
    :return: None
    """

    if metric_kwargs is None:
        metric_kwargs = {}
    if plot_labels is None:
        plot_labels = ["" for _ in ys]
    if dots_labels is None and dots is not None:
        dots_labels = ["" for _ in dots]

    if ax is None:

        if title is not None:
            plt.title(title)

        for i, y in enumerate(ys):

            try:
                label = plot_labels[i]
            except IndexError:
                label = ""

            try:
                ls = metric_kwargs[i]
            except KeyError:
                ls = {}
            plt.plot(x, y, label=label, **ls)

        if qsl is not None:
            plt.plot(qsl[0], qsl[1], 'rx', label="$T_{min}$")

        if drift is not None:
            if not isinstance(drift, list):
                drift = [drift]
            for d in drift:
                drift_x, drift_y = d
                plt.plot(drift_x, drift_y.T, 'r-', label="Drift fidelity")

        if dots is not None:
            if not isinstance(dots, list):
                dots = [dots]
                dots_labels = [dots_labels]

            cycol = itertools.cycle('bgrcmk')
            for dots_data, dots_label in zip(dots, dots_labels):
                c = next(cycol)
                if isinstance(dots_data, tuple):
                    x_dots, dots_data = dots_data
                else:
                    x_dots = x
                for i, y in enumerate(dots_data):
                    if i == 0:
                        plt.scatter(x_dots[i] * np.ones(y.shape), y, s=0.5, c=c, label=dots_label)
                    else:
                        plt.scatter(x_dots[i] * np.ones(y.shape), y, s=0.5, c=c)

        if fidelity_regions:
            plt.fill_between(x, fidelity_regions[0], fidelity_regions[1], alpha=0.5)

        if vlines is not None:
            for vline in vlines:
                plt.vlines(**vline)

        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=defaults.latexfontsize)
            plt.ylabel(axis_labels[1], fontsize=defaults.latexfontsize)
        if options is not None:
            for k, v in options.items():
                getattr(plt, k)(*v[0], **v[1])

        plt.xticks(fontsize=defaults.fontsize)
        plt.yticks(fontsize=defaults.fontsize)
    else:

        if title is not None:
            ax.set_title(title)

        for i, y in enumerate(ys):
            try:
                label = plot_labels[i]
            except IndexError:
                label = ""

            try:
                ls = metric_kwargs[i]
            except KeyError:
                ls = {}
            ax.plot(x, y, label=label, **ls)

        if qsl is not None:
            ax.plot(qsl[0], qsl[1], 'rx', label=tmin_label, markersize=8)

        if vlines is not None:
            for vline in vlines:
                ax.axvline(**vline)

        if drift is not None:
            drift_x, drift_y = drift
            p = ax.plot(drift_x, drift_y.T, 'r-', alpha=0.3)
            if drift_label is not None:
               p[0].set_label(drift_label)

        if dots is not None:
            if not isinstance(dots, list):
                dots = [dots]
                dots_labels = [dots_labels]

            cycol = itertools.cycle('bgrcmk')
            for dots_data, dots_label in zip(dots, dots_labels):
                c = next(cycol)
                for i, y in enumerate(dots_data):
                    if i == 0:
                        ax.scatter(x[i] * np.ones(y.shape), y, s=0.5, c=c, label=dots_label)
                    else:
                        ax.scatter(x[i] * np.ones(y.shape), y, s=0.5, c=c)

        if fidelity_regions:
            alpha_values = get_alpha_range(len(fidelity_regions))
            for region, a in zip(fidelity_regions, alpha_values):
                if len(region) == 3:
                    lbl = region[2]
                else:
                    lbl = ""
                ax.fill_between(x, region[0], region[1], color='b', edgecolor='none', alpha=a, label=lbl)

        if fill_regions:
            if fill_regions_kwargs is None:
                fill_regions_kwargs = [{} for _ in fill_regions]
            for region, kwargs in zip(fill_regions, fill_regions_kwargs):
                if len(region) == 3:
                    x_plot = region[0]
                    y1_plot = region[1]
                    y2_plot = region[2]
                else:
                    x_plot = x
                    y1_plot = region[0]
                    y2_plot = region[1]
                ax.fill_between(x_plot, y1_plot, y2_plot, **kwargs)

        if axis_labels is not None:
            ax.set_xlabel(axis_labels[0], fontsize=defaults.latexfontsize)
            ax.set_ylabel(axis_labels[1], fontsize=defaults.latexfontsize)
        if options is not None:
            for k, v in options.items():
                getattr(ax, k)(*v[0], **v[1])

    if legend_loc is not None:
        if legend_loc == "outside":
            ax = plt.gca()
            chartBox = ax.get_position()
            ax.set_position([chartBox.x0, chartBox.y0, chartBox.width * 0.7, chartBox.height])
            ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1.0), shadow=False, ncol=1, borderaxespad=0,
                      frameon=False)
        elif legend_loc == "below":
            if legend_loc is None:
                bbox_to_anchor = (0.5, -0.1)
            else:
                bbox_to_anchor = legend_offset
            if legend_point is None:
                legend_point = 'upper center'
            if ax is None:
                ax = plt.gca()
            chartBox = ax.get_position()
            ax.set_position([chartBox.x0, chartBox.y0, chartBox.width, chartBox.height * 0.2])
            ax.legend(loc=legend_point, bbox_to_anchor=bbox_to_anchor, shadow=False, ncol=2, borderaxespad=0,
                      frameon=False, fontsize=defaults.fontsize)
            plt.subplots_adjust(hspace=0, top=0.95,bottom=0.35)
        else:
            plt.legend(loc=legend_loc)

    if out is None:
        return
    elif out == "show":
        plt.show()
    elif out == "view":
        plt.draw()
        plt.pause(0.0001)
    else:
        plt.savefig(out, format='pdf')
