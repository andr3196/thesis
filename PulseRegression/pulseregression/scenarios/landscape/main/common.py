"""
Author: Andreas Springborg
Date: 2019-05-28
Goal:

"""
import numpy as np

from pulseregression.framework.procedure import select_files, field_loader, get_filenames


def data_iterator(rhp, n_T):
    file_list = rhp.get_load_files()

    interactive_files = get_filenames(rhp.save_path("swapping"), base="interactive_points", sort_tag=None)
    interactive_files2 = get_filenames(rhp.save_path("swapping"), base="interactive_swapping", sort_tag=None)
    all_points = []
    for points in field_loader(interactive_files, "points", prepend_Ti=False):
        all_points.extend(points)
    for points in field_loader(interactive_files2, "points", prepend_Ti=False):
        all_points.extend(points)

    for Ti in np.arange(n_T):
        files = select_files(file_list, "_Ti_{}.npz".format(Ti))

        if files:
            # all_seeds_list = []
            all_ctrls_list = []
            all_fids_list = []

            for ctrls, fids in field_loader(files, ["ctrls", "fids"], prepend_Ti=False):
                # all_seeds_list.append(seeds)
                all_ctrls_list.append(ctrls)
                all_fids_list.append(fids)
            # seeds = np.concatenate(all_seeds_list, axis=0)
            ctrls = np.concatenate(all_ctrls_list, axis=0)
            fids = np.concatenate(all_fids_list, axis=0)
        else:
            ctrls = None
            fids = None

        Ti_points = [point for point in all_points if point[0] == Ti]

        if Ti_points:
            int_ctrls = np.array([p[1] for p in Ti_points])
            int_fids = np.array([p[2] for p in Ti_points])

            if ctrls is not None:
                ctrls = np.concatenate([ctrls, int_ctrls], axis=0)
                fids = np.concatenate([fids, int_fids], axis=0)
            else:
                ctrls = int_ctrls
                fids = int_fids
        if ctrls is not None:
            yield Ti, None, ctrls, fids
