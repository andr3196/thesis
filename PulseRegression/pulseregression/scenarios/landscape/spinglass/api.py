"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot phase metrics for a spin glass problem

"""

import numpy as np

from pulseregression.framework.problems import SpinChainProblemScan
from pulseregression.framework.utility import calculate_max_fidelity, calculate_mean_distance, \
    calculate_quantile, calculate_min_fidelity
from pulseregression.scenarios.landscape.core import cluster_controls, compute_mean_data


####  Define  ####

# NB: The tuning parameter called "gamma" in the report is called "alpha" here.

alphas = np.linspace(0, 1, 6)
sgpscan = SpinChainProblemScan(alphas)
n_seeds = 100
n_p = 5

T_last = 8.0

save_filename_basis = "spinglass_mean_metrics_a_{:1.1f}.npz"

#################

# Define metrics
metric_fcts = [
    lambda s, c, f, args: calculate_min_fidelity(f),
    lambda s, c, f, args: calculate_quantile(f, 0.5),
    lambda s, c, f, args: calculate_quantile(f, 0.25),
    lambda s, c, f, args: calculate_max_fidelity(f),
    lambda s, c, f, args: calculate_mean_distance(c,
                                                  normalize=args["normalize_distance"], max_amp=args["max_amp"])
]


def compute_scan():
    for i, sgp in enumerate(sgpscan):
        # Get files
        file_list = sgp.get_load_files()
        save_file = sgp.save_path() / save_filename_basis.format(sgp.alpha)
        args = [file_list,  # files to collect data from
                n_p, sgp.T_step,
                sgp.max_ampl, n_seeds,
                sgp.fqsl, metric_fcts,
                cluster_controls,  # Function to apply to each control before calculating metrics
                save_file,
                sgp.problem_fct(),  # A function that, given a p, produces: Hd, Hc, init, targ
                sgp.dt, T_last]

        compute_mean_data(*args)


if __name__ == "__main__":
    mode = 'mean'

    if mode == 'mean':
        compute_scan()
    else:
        raise ValueError("Unknown mode: {}".format(mode))
