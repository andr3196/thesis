"""
Author: Andreas Springborg
Date: 2019-05-08
Goal: Scan the impact of the width of the filter on the fidelity

"""

import numpy as np
from scipy.signal import filtfilt, bessel

from pulseregression.framework.control.filtering import apply_filter
from pulseregression.framework.optim.optimization import calculate_fidelity_with_optimizer
from pulseregression.framework.procedure import field_loader, get_filenames, select_files

### Define ###

# IO
from pulseregression.framework.problems.spin import SpinChainProblem

savefile_basis = "spinchain_filter_scan_alg_{}"

scp = SpinChainProblem()


###


def scan_window_widths(file, filter_range, filt, optim):
    """ Apply a filter to the ctrls in file and run GRAPE on the filtered controls
        The algorithm assumes that the range starts from 0
    """
    n_filters = len(filter_range)

    # load data
    Ti, ctrls, fids = next(field_loader(file, ["ctrls", "fids"]))
    scanned_fidelities = np.zeros((ctrls.shape[0], n_filters))
    scanned_fidelities[:, 0] = fids

    # Prepare iteration (skip filt=0)
    filt_iter = iter(filter_range)
    next(filt_iter)

    for i, filt_w in enumerate(filt_iter):
        filt_ctrls = apply_filter(ctrls, filt, filt_w, bounds=scp.bounds())
        # Run GRAPE on each
        for j, ctrl in enumerate(filt_ctrls):
            scanned_fidelities[j, i + 1] = calculate_fidelity_with_optimizer(optim, seed=ctrl)
        print("Finished running {}/{}".format(i+1, n_filters))
    return scanned_fidelities


def compute_filtering_scan():
    Ti = 148
    file_list = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g)
    file = select_files(file_list, "Ti_{}".format(Ti))[0]

    delta = 0.01
    filter_range = np.linspace(delta, 1-delta, 12)
    #filt = lambda x, wl: savgol_filter(x, wl, 1)
    filt = lambda x, wl: filtfilt(*bessel(3, wl, 'low'), x)

    optim = scp.generate_optim(scp.T_range[Ti])
    fidelities = scan_window_widths(file, filter_range, filt, optim)
    np.savez(scp.save_path(sub_path="filtering") / savefile_basis.format("bessel"), x=filter_range, fids=fidelities)


if __name__ == "__main__":
    compute_filtering_scan()
