"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""
import numpy as np
from scipy.signal import filtfilt, bessel
from plotguard import gridspec as gspc
from plotguard import pyplot as plt
from pulseregression import systems
from pulseregression.framework.control.filtering import apply_filter
from pulseregression.framework.procedure import get_filenames, field_loader
from pulseregression.framework.problems.spin import SpinChainProblem

savefile_basis = "spinchain_filter_scan_alg_{}"

scp = SpinChainProblem()

filt = lambda x, wl: filtfilt(*bessel(3, wl, 'low'), x)


def correct_order(x, fids):
    """ Due to a bug in the collection algorithm the data was collected in the wrong order - correct this
    """
    fids = np.fliplr(np.roll(fids, -1, axis=1))
    x = np.roll(x, -1)
    x[-1] = 1.0 # Value not allowed in filter, but the limit x-> 1.0 marks the limit of no filter
    x = np.flip(x)
    return x, fids


def display_filtering_scan():
    file = get_filenames(scp.save_path("filtering"), base="spinchain_filter_scan", sort_tag=None, alg="bessel")[0]
    # Get files
    load_file = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=148)[0]

    Ti, ctrls, fids = next(field_loader(load_file, ["ctrls", "fids"]))

    # Define
    f = np.load(file)
    x, fids = f["x"], f["fids"]
    x, fids = correct_order(x, fids)

    # Calculate the percentage change
    drop = (fids - fids[:, 0, np.newaxis]) / fids[:, 0, np.newaxis]
    y = 1 - x  # It is more intuitive to read the plot left to right

    # Prepare subplot framework
    fig = plt.figure(figsize=(7.2, 6.0))
    outer = gspc.GridSpec(2, 1, height_ratios=[2, 1])
    top = gspc.GridSpecFromSubplotSpec(1, 1,
                                       subplot_spec=outer[0])
    bottom = gspc.GridSpecFromSubplotSpec(1, 3,
                                          subplot_spec=outer[1])

    # Plot fidelity drop vs smoothing
    ax = plt.Subplot(fig, top[0, 0])
    ax.plot(y, drop.T, 'b')
    ax.set_xlabel("$1 - W_n$", fontsize=15)
    ax.set_ylabel("$(F(W_n) - F^*) / F^*$", fontsize=15)
    fig.add_subplot(ax)

    # Plot examples below
    Wn = [0.8, 0.6, 0.4]
    for i, w in enumerate(Wn):
        ax = plt.Subplot(fig, bottom[0, i])
        c_smooth = apply_filter(ctrls, filt, w, scp.bounds())
        t = np.linspace(0, scp.get_T(Ti), scp.n_ts)
        ax.plot(t, c_smooth.T)
        ax.set_ylim(bottom=-4.5, top=4.5)
        ax.set_yticks([-4.0, 0, 4.0])
        ax.set_xlabel("Time")
        ax.set_title("$1 - W_n$={:1.1f}".format(1-w))
        if i == 0:
            ax.set_ylabel("Control field")

        fig.add_subplot(ax)

    plt.subplots_adjust(hspace=0.4)
    plt.subplots_adjust(wspace=0.4)
    systems.save_figure(plt, 'spinchain_filtering_scan_L6J1g1Ti148.pdf')
    #fig.savefig('spinchain_filtering_scan_L6J1g1Ti148.pdf', format='pdf')
    plt.show()


if __name__ == "__main__":
    display_filtering_scan()
