"""
Author: Andreas Springborg
Date: 2019-05-09
Goal:

"""
from plotguard import pyplot as plt
import numpy as np

from pulseregression.framework.clustering.kmeans import split_to_clusters, get_cluster_labels
from pulseregression.framework.clustering.plot import cluster_plot
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.problems import SpinChainProblem
from pulseregression.framework.procedure import get_filenames, field_loader
from matplotlib import cm
import matplotlib as mpl

from pulseregression.styling import defaults

scp = SpinChainProblem()


def display_scan():
    files = get_filenames(scp.save_path("clustering"), base="spinchain", sort_tag=None)
    beta_values = np.linspace(0, 1, 10)

    cmap = cm.get_cmap("viridis", 20)
    for Ti, fidelities in field_loader(files, 'fids', duration_tags=["Ti_", "_clust_L3norm"]):
        c = cmap((Ti + 1) / 200)
        plt.plot(beta_values, fidelities.T, color=c)
        if np.any((fidelities[:, 0] - fidelities[:, -1]) > 0.1):
            print(np.sum((fidelities[:, 0] - fidelities[:, -1]) > 0.1))

    norm = mpl.colors.Normalize(vmin=0, vmax=4.0)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    bar = plt.colorbar(sm)
    bar.set_label("Duration, $T$", fontsize=defaults.fontsize)
    plt.xlabel("Tuning parameter, $\\beta$", fontsize=defaults.fontsize)
    plt.ylabel("Fidelity, $F$", fontsize=defaults.fontsize)
    #plt.savefig("spinchain_cluster_annealing.pdf", format="pdf")
    plt.show()


def display_clusters():
    files = get_filenames(scp.load_path(), base="spinchain", sort_tag=None)
    file_Tis = [152, 156, 168, 176, 184, 160]

    for Ti in file_Tis:
        file = [file for file in files if "Ti_{}".format(Ti) in str(file)]
        Ti, ctrls, fids = next(field_loader(file, ["ctrls", "fids"]))
        ctrls = apply_default_bessel_filter(ctrls, scp.bounds())
        labels = get_cluster_labels(ctrls, scp.max_ampl)
        print(np.unique(labels))
        clusters = split_to_clusters(ctrls, fids, labels)
        cluster_plot(clusters)


def display_clusters_single_time():
    file = get_filenames(scp.load_path(), base="spinchain", sort_tag=None, L=scp.L, J=scp.J, g=scp.g, Ti=148)
    Ti, ctrls, fids = next(field_loader(file, ["ctrls", "fids"]))

    cmap = cm.get_cmap("viridis", 20)
    ctrls = apply_default_bessel_filter(ctrls, scp.bounds())
    labels = get_cluster_labels(ctrls, scp.max_ampl)
    clusters = split_to_clusters(ctrls, fids, labels)
    n_clusters = len(clusters)

    times = np.linspace(0, scp.get_T(Ti), ctrls.shape[1])

    cluster_handles = []
    cluster_labels = []
    for i in range(n_clusters):
        c, f, _ = clusters[i]
        handles = plt.plot(times, c.T, color=cmap((i + 1) / n_clusters))
        cluster_handles.append(handles[0])
        cluster_labels.append("F={:1.2f}".format(np.mean(f)))
    plt.legend(cluster_handles, cluster_labels, framealpha=1.0)
    plt.xlabel("Time, $t$", fontsize=defaults.fontsize)
    plt.ylabel("Control amplitude", fontsize=defaults.fontsize)
    # plt.savefig("spinchain_cluster_control_examples_Ti148.pdf", format="pdf")
    plt.show()


def __run__():
    mode = "display_clusters_single_time"

    if mode == "display_scan":
        # Show the fidelity as a function of tuning parameter beta between individual optima and the assigned mean.
        display_scan()
    elif mode == "display_clusters":
        display_clusters()
    elif mode == "display_clusters_single_time":
        # Show how clusters are sorted into clusters for a single duration
        display_clusters_single_time()


if __name__ == "__main__":
    __run__()
