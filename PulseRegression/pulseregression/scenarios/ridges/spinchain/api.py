"""
Author: Andreas Springborg
Date: 22/12/2018
Goal: Plot phase metrics for a savgol_smoothing problem

"""
import sys

from scipy.signal import filtfilt, bessel
from sklearn.cluster import KMeans

import numpy as np

from pulseregression.framework.clustering.kmeans import split_to_clusters, get_num_clusters
from pulseregression.framework.control.filtering import apply_filter
from pulseregression.framework.optim.optimization import calculate_fidelity_with_optimizer
from pulseregression.framework.procedure.procedure import get_filenames, field_loader

####  Define  ####
from pulseregression.framework.problems.spin import SpinChainProblem

# IO
scp = SpinChainProblem()

filt = lambda x, wl: filtfilt(*bessel(3, wl, 'low'), x) if len(x) >12 else x
mean_filt = lambda x, beta: (1-beta)*x + beta*np.mean(x, axis=0)
#################


def anneal_to_centroids(i):
    file = get_filenames(scp.load_path(), "spinchain", L=scp.L, J=scp.J, g=scp.g)[i]

    # Define step_through
    beta_values = np.linspace(0, 1, 10)


    # Load data
    Ti, ctrls, fids = next(field_loader(file, ["ctrls", "fids"]))
    T = scp.T_range[Ti]

    # Define output
    fidelities = np.zeros((ctrls.shape[0], len(beta_values)))
    fidelities[:, 0] = fids

    # Apply filter
    ctrls = apply_filter(ctrls, filt, 0.4, scp.bounds())

    k = get_num_clusters(ctrls, scp.max_ampl)
    labels = KMeans(k).fit_predict(ctrls)
    clusters = split_to_clusters(ctrls, fids, labels)

    for i, cluster in enumerate(clusters):
        c, f, indices = cluster
        for bi in range(1, len(beta_values)-1):
            beta = beta_values[bi]
            if c.shape[0] > 1:
                c_ann = mean_filt(c, beta)
                for ci, ind in zip(c_ann, indices):
                    fidelities[ind, bi] = calculate_fidelity_with_optimizer(scp.generate_optim(T), seed=ci)
            else:  # If there is only 1 control in the cluster, then annealing has no effect and the fidelity is unchanged
                fidelities[indices, bi] = f
        # The final iteration is special because it only needs to calculate 1 control = centroid
        beta = beta_values[-1]
        if c.shape[0] > 1:
            c_ann = mean_filt(c, beta)[0, :]
            fidelities[indices, -1] = calculate_fidelity_with_optimizer(scp.generate_optim(T), seed=c_ann)
        else:
            fidelities[indices, -1] = f
        print("Done with a cluster: {}% done".format((i+1)/len(clusters)))

    np.savez(scp.save_path("ridges") / file.name.replace(".npz", "_clust_L3norm.npz"), labels=labels, fids=fidelities)


def __run__(i):
    anneal_to_centroids(i)


"""     OUTPUT     """

if __name__ == "__main__":
    args = sys.argv
    if len(args) == 2:
        i = int(args[1])
        __run__(i)
    else:
        print("usage: api i")
        exit(1)
