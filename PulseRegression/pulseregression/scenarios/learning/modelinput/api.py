"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
import sys

import numpy as np
from pathlib import Path
from pulseregression.framework.models.models import select_model
from pulseregression.scenarios.learning.modelinput.core import scan_models

""" Define """

n_p = 20

data_filename_prefix = "local_data/run_models/scan_"
n = 2
p_values = np.arange(n_p)
b = 3.0

max_num_datapoints = 70000
n_training_epochs = 50
batch_size = 512

data_input_path = Path("../../../../data/distance/runs/run1/data")
data_output_path = Path("../../../../data/distance/collected/run1/fit")

"""        """

field_map = ["ctrls", "seeds"]

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Usage: display.py i  [ctrls=0, seeds=1]")
    else:
        if len(args) == 3:
            field = field_map[int(args[2])]
        else:
            print("Fitting ('ctrls', 'fids)'")
            field = field_map[0]
        k = int(args[1])

        model_generator, wrapper = select_model(k)
        scan_models(model_generator,
                    wrapper,
                    n,
                    p_values,
                    b,
                    max_num_datapoints,
                    n_training_epochs,
                    batch_size,
                    data_input_path,
                    plot_save_path=data_output_path,
                    data_save_path=data_output_path,
                    field=field)
