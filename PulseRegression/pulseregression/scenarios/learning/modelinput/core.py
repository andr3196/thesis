"""
Author: Andreas Springborg
Date: 28/01/2019
Goal:

"""
import sys

import numpy as np
from hpsklearn import HyperoptEstimator
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.python.keras.callbacks import EarlyStopping

from pulseregression.framework.learning.collector import ErrorCollector
from pulseregression.framework.learning.plotting import visualize_run
from pulseregression.framework.procedure import make_file_identifier
from pulseregression.framework.quantum import init_problem_as_vec
from pulseregression.framework.utility import remove_empty_rows, prepend_vector, stack, shuffle, crop, \
    calc_control_length

sys.path.append("../")


def import_data(path_to_data_folder, data_tag, field='ctrls'):
    file_gen = path_to_data_folder.glob(data_tag + "*.npz")

    features_list = []
    fidelities_list = []
    found_files = False
    for file in file_gen:
        found_files = True
        f = np.load(file)
        features_list.append(f[field])
        fidelities_list.append(f["fids"])

    if not found_files:
        raise IOError(
            "No files in the directory, {}, matched the specified tag: '{}'".format(path_to_data_folder, data_tag))

    return stack(features_list, fidelities_list)


def gather_problem_data(data_path, n, p_values, b, field):
    all_ctrls_list = []
    all_fids_list = []
    for p in p_values:
        data_tag = make_file_identifier(n=n, p=p, b=b)
        try:
            ctrls, fids = import_data(data_path, data_tag, field=field)
            ctrls, fids = remove_empty_rows(ctrls, fids)
        except OSError:
            continue
        problem_vec = init_problem_as_vec(n, p, b)
        ctrls = prepend_vector(problem_vec, ctrls)
        all_ctrls_list.append(ctrls)
        all_fids_list.append(fids)
    ctrls, fids = stack(all_ctrls_list, all_fids_list)

    return ctrls, fids


def make_equal_lengths(*args):
    lengths = [x.shape[0] for x in args]
    combined_x = stack(args)
    res_list = []
    cur = 0
    for l in lengths:
        res_list.append(combined_x[cur:cur + l, :])
        cur += l
    return tuple(res_list)


def prepare(data_path, n, p_values, b, max_num_datapoints, field="ctrls"):
    p_train = p_values[3:]
    p_test = p_values[:3]
    ctrls_train, fids_train = gather_problem_data(data_path, n, p_train, b, field)
    ctrls_test, fids_test = gather_problem_data(data_path, n, p_test, b, field)

    ctrls_train, fids_train = shuffle(ctrls_train, fids_train)
    ctrls_train, fids_train = crop(ctrls_train, fids_train, max_num_datapoints)

    ctrls_test, fids_test = shuffle(ctrls_test, fids_test)

    X_train, _, y_train, _ = train_test_split(ctrls_train, fids_train, test_size=0.2)
    X_valid, X_test, y_valid, y_test = train_test_split(ctrls_test, fids_test, test_size=0.5)

    X_train, X_valid, X_test = make_equal_lengths(X_train, X_valid, X_test)

    return X_train, y_train, X_valid, y_valid, X_test, y_test


def run(data, model, n_training_epochs, batch_size):
    if isinstance(model, keras.Sequential):
        es = EarlyStopping(patience=2, verbose=True)
        print(model.summary())
        fit_res = model.fit(data[0], data[1],
                            epochs=n_training_epochs,
                            batch_size=batch_size,
                            validation_data=(data[2], data[3]),
                            callbacks=[es]
                            )
        loss_history = (fit_res.history['mean_absolute_error'], fit_res.history['val_mean_absolute_error'])
    elif isinstance(model, HyperoptEstimator):
        # Hypertopt model select the validation set itself, thus combine X_train, X_valid
        X_train_and_valid = np.concatenate((data[0], data[2]), axis=0)
        y_train_and_valid = np.concatenate((data[1], data[3]), axis=0)
        model.fit(X_train_and_valid, y_train_and_valid, valid_size=0.15)
        loss_history = None
    else:  # Must be an sklearn model
        model.fit(data[0], data[1])
        loss_history = None
    test_phase_data = (data[5], model.predict(data[4]), calc_control_length(data[4]))
    return loss_history, test_phase_data


def scan_models(model_func,
                data_wrapper,
                n,
                p_values,
                b,
                max_num_datapoints,
                n_epochs,
                batch_size,
                data_path,
                savefile_prefix=None,
                plot_save_path=None,
                data_save_path=None,
                field="ctrls"):
    ec = ErrorCollector()
    data = prepare(data_path, n, p_values, b, max_num_datapoints, field=field)
    input_dim = data[0].shape[1]
    model = model_func(input_dim)

    # Run
    data = data_wrapper(data)
    loss_history, test_phase_data = run(data, model, n_epochs, batch_size)
    ec.collect(loss_history, test_phase_data)
    ec.print_last()

    tag = "n_{}_b_{}_pooled_outsample".format(n, b)

    # Evaluate
    visualize_run(loss_history, test_phase_data, name="phase_" + model.name + "_" + tag + ".pdf",
                  path=plot_save_path,
                  ext="pdf")
    if savefile_prefix is None:
        savefile_prefix = field + "_" + tag + "_"

    #save_scan(*ec.get_all_errors(), model.name, savefile_prefix, save_path=data_save_path)
