"""
Author: Andreas Springborg
Date: 03/03/2019
Goal: Load .npz files and save contents as .mat

"""
from pathlib import Path

import numpy as np
from scipy.io import savemat

""" Define """
n_p = 10
n_maes = 6
load_path = Path("../../../../data/distance/collected/run1/fit")
save_path = load_path

"""        """


def run():
    opt_files = list(load_path.glob("run_n_11*.npz"))
    seed_files = list(load_path.glob("seeds_*.npz"))
    opt_files.sort()
    seed_files.sort()

    mae_data = np.zeros((n_p, n_maes))
    mae_data[:] = np.nan

    for (i, files) in enumerate(zip(opt_files, seed_files)):
        opt_file, seed_file = files
        o_f = np.load(opt_file)
        s_f = np.load(seed_file)

        mae_data[i, :] = [o_f["maes"], o_f["min_maes"], o_f["high_fid_maes"], s_f["maes"], s_f["min_maes"], s_f["high_fid_maes"]]

    d = {"maes": mae_data}
    savemat(save_path / "mae_data_n_11_b_3.0_optseed.mat", d)


if __name__ == "__main__":
    run()
