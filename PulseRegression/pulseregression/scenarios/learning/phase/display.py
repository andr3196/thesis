"""
Author: Andreas Springborg
Date: 2019-05-30
Goal:

"""
from plotguard import pyplot as plt
from pathlib import Path

import numpy as np

from pulseregression import systems
from pulseregression.framework.procedure import get_filenames, field_loader, select_files
from pulseregression.styling import defaults

load_path = Path("../../../../data/distance/collected/run1/phase/")

def stack_plot():
    files = get_filenames(load_path, base="Deep_CNN_predictions_run", sort_tag=None)
    files = select_files(files, "n_7_p_8")

    n_bins = 50
    bins = np.linspace(0, 1, n_bins + 1)

    for calc, preds in field_loader(files, ["calc_fidelity", "pred_fidelity"], prepend_Ti=False):
        plt.scatter(calc, preds, c="g", s=0.8, marker=".")
        indices = np.digitize(calc, bins) - 1

        low_b = []
        high_b = []

        for bin_index in range(n_bins):
            filt = indices == bin_index

            pred_batch = preds[filt]
            if np.any(pred_batch):
                mu = np.mean(pred_batch)
                std = np.std(pred_batch)
                low_b.append(mu + std)
                high_b.append(mu - std)
        bin_centres = bins[:-1] + np.diff(bins) / 2
        plt.fill_between(bin_centres, low_b, high_b,
                         color="b", alpha=0.2)

    x = np.linspace(0, 1, 10)
    plt.plot(x, x, 'r--')

    plt.xlabel("True fidelity", fontsize=defaults.fontsize)
    plt.ylabel("Predicted fidelity", fontsize=defaults.fontsize)
    defaults.set_ticksize(plt.gca())
    #systems.save_figure(plt, "phase_deep_CNN_n7p0b3")

    plt.show()


if __name__ == "__main__":
    mode = "stack_plot"

    if mode == "stack_plot":
        stack_plot()
