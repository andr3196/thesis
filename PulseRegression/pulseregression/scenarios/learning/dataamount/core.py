"""
Author: Andreas Springborg
Date: 28/01/2019
Goal:

"""
from sklearn.model_selection import train_test_split

from pulseregression.framework.grendel.learning.collector import ErrorCollector
from pulseregression.framework.grendel.utility import reshape, remove_empty_rows, shuffle, crop, fill_nan
from pulseregression.framework.io.fileio import load_data
from pulseregression.framework.models.models import build_mlp
from pulseregression.scenarios.learning.models.core import run, visualize_run, save_scan



def load_all_data(data_file_tag):
    ctrls, fids = load_data(data_file_tag)
    ctrls, fids = reshape(ctrls, fids)
    ctrls, fids = remove_empty_rows(ctrls, fids)
    ctrls, fids = shuffle(ctrls, fids)
    return ctrls, fids


def data_generator(data):
    def sample_data(n_samples):
        ctrls, fids = crop(data[0], data[1], n_samples)
        ctrls = fill_nan(ctrls, 0.0)

        X_train, X_rest, y_train, y_rest = train_test_split(ctrls, fids, test_size=0.3)
        X_valid, X_test, y_valid, y_test = train_test_split(X_rest, y_rest, test_size=0.5)
        return X_train, y_train, X_valid, y_valid, X_test, y_test

    return sample_data


def scan_data_amounts(sample_data, data_amounts, layer_width, n_epochs, batch_size):
    ec = ErrorCollector()
    for n_sizes in data_amounts:
        # Initiate
        data = sample_data(n_sizes)
        input_dim = data[0].shape[1]
        model = build_mlp(input_dim, n_layers=12, layer_units=layer_width, name="MLP_L_12")

        # Train
        loss_history, test_phase_data = run(data, model, n_epochs, batch_size)
        ec.collect(loss_history, test_phase_data)
        ec.print_last()

        # Evaluate
        visualize_run(loss_history, test_phase_data, name="MLP_data_amount{}".format(n_sizes))
    save_scan(*ec.get_all_errors(), "MLP_data_amount_scan", training_samples=[int(0.7 * n) for n in data_amounts])
