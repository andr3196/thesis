"""
Author: Andreas Springborg
Date: 13/02/2019
Goal:

"""
import numpy as np

from pulseregression.scenarios.learning.dataamount.core import load_all_data, data_generator, scan_data_amounts

""" Define """

n_layers = 12
layer_width = 200
data_file_tag = "n_7_p_1"

data_amounts = [5000] + [1000 * n for n in range(10, 475, 20)]
n_epochs = 50
batch_size = 128

"""        """

if __name__ == "__main__":
    all_data = load_all_data(data_file_tag)
    sample_data = data_generator(all_data)
    scan_data_amounts(sample_data, data_amounts, layer_width, n_epochs, batch_size)
