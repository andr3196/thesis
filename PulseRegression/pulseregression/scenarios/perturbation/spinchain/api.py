"""
Author: Andreas Springborg
Date: 2019-05-12
Goal: Perturb the optimal control in different directions, and study the fidelity decay away from the peak

"""
import multiprocessing as mp
import sys

import numpy as np

from pulseregression.framework.clustering.kmeans import get_cluster_centres
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.optim.optimization import calculate_fidelity_with_optimizer
from pulseregression.framework.problems.spin import SpinChainProblem
from pulseregression.framework.procedure import field_loader, get_filenames

n_perturbs = 100
scp = SpinChainProblem()
amp_0 = 10

perturb_fct = lambda c, p, a: np.clip(c + a * p, -4.0, 4.0)
n_amps = 20
n_tries = 10
f_end = 0.05


def fixed_perturbation_ampl_scan(args):
    optim, ctrl, perturb, a0, n_sample, f0 = args

    amps = np.linspace(0, a0, n_sample)
    fids = np.ones((n_sample,))
    for i in range(n_sample):
        cur_ctrl = perturb_fct(ctrl, perturb, amps[i])
        fids[i] = calculate_fidelity_with_optimizer(optim, seed=cur_ctrl)
    return amps, fids


def perturbation(Ti):
    files = scp.get_load_files(Ti=Ti)
    _, ctrls, fids = next(field_loader(files, ["ctrls", "fids"]))

    ctrls = apply_default_bessel_filter(ctrls, scp.bounds())
    ctrls, fids_list, _ = get_cluster_centres(ctrls, fids, scp.max_ampl)

    n_clust = ctrls.shape[0]
    print(n_clust)

    perturbs = (2 * np.random.random((n_perturbs, ctrls.shape[1])) - 1)
    p = mp.Pool()

    scan_inputs = [(scp.generate_optim(scp.get_T(Ti)), ctrls[i, :], perturbs[j, :], amp_0, n_amps, np.mean(fids[i]))
                   for i in range(n_clust) for j in range(n_perturbs)]
    async_out = p.map_async(fixed_perturbation_ampl_scan, scan_inputs)
    p.close()
    p.join()
    result_tuples = async_out.get()

    amps_list = [r[0] for r in result_tuples]
    fids_list = [r[1] for r in result_tuples]
    amps = np.array(amps_list)
    fids = np.array(fids_list)
    np.savez(scp.save_path("perturbation/linesearch") / "spinchain_perturb_Ti_{}".format(Ti), amps=amps, fids=fids)


def calculate_perturbations(Ti, ctrls, fid):
    if ctrls.ndim == 1:
        ctrls = ctrls[..., np.newaxis]
    ctrls = apply_default_bessel_filter(ctrls, scp.bounds())
    perturbs = (2 * np.random.random((n_perturbs, ctrls.shape[1])) - 1)
    p = mp.Pool()

    scan_inputs = [(scp.generate_optim(scp.get_T(Ti)), ctrls[i, :], perturbs[j, :], amp_0, n_amps, fid)
                   for i in range(ctrls.shape[0]) for j in range(n_perturbs)]
    async_out = p.map_async(fixed_perturbation_ampl_scan, scan_inputs)
    p.close()
    p.join()
    result_tuples = async_out.get()

    amps_list = [r[0] for r in result_tuples]
    fids_list = [r[1] for r in result_tuples]
    amps = np.array(amps_list)
    fids = np.array(fids_list)
    return amps, fids


if __name__ == "__main__":
    mode = "perturbation_4point"
    if mode == "perturbation_scan":
        args = sys.argv
        Tis = np.arange(0, 201, 8)
        print(len(Tis))
        i = int(args[1])
        perturbation(Tis[i])
    elif mode == "perturbation_4point":

        # Calculate fidelities of perturbations of 2 optimal controls samled from global optimal and non-global optimal
        # trajectories, respectively. Each optimum is studied above and below T_min


        # Short time, high fidelity
        file1 = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=84)[0]
        # Short time, low fidelity
        file2 = \
        get_filenames(scp.save_path("swapping"), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=85, sort_tag=None)[0]
        # long time, high fidelity
        file3 = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=160)[0]

        # long_time, low fidelity
        file4 = \
        get_filenames(scp.save_path("swapping"), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=148, swapto_nTi=160,
                      sort_tag=None)[0]

        files = [file1, file2, file3, file4]
        i_values = [0, 0, 0, 1]
        Tis = [84, 84, 160, 160]

        all_amps = []
        all_fids = []
        for Ti, data, i in zip(Tis, field_loader(files, ["ctrls", "fids"], prepend_Ti=False), range(4)):
            ctrls, fids = data
            amps, perturb_fidelities = calculate_perturbations(Ti, ctrls[np.newaxis, i_values[i]],
                                                               fids[np.newaxis, i_values[i]])
            all_amps.append(amps)
            all_fids.append(perturb_fidelities)
        np.savez(scp.save_path("perturbation") / "spinchain_4point_perturbs", all_amps=all_amps, all_fids=all_fids)
