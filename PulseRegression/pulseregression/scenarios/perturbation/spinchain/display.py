"""
Author: Andreas Springborg
Date: 2019-05-16
Goal:

"""
import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import fsolve

from plotguard import pyplot as plt
from pulseregression import systems
from pulseregression.framework.procedure import get_filenames, field_loader
from pulseregression.framework.problems.spin import SpinChainProblem
from pulseregression.styling import defaults

scp = SpinChainProblem()


def step_trough():
    files = get_filenames(scp.save_path("perturbation/linesearch"), base="spinchain_perturb")

    for Ti, amps, fids in field_loader(files, ["amps", "fids"]):

        inp = input("Plot Ti={}?".format(Ti))

        rel_fids = fids / fids[:, 0, np.newaxis]

        if inp == "y":
            print(fids[:, 0])
            plt.plot(amps.T, rel_fids.T)
            # plt.ylim([0,1])
            plt.show()


def calculate_drop_distance(amps, fids, th):
    n_amps = amps.shape[0]
    drop = fids - fids[:, 0, np.newaxis]
    steps = np.zeros((drop.shape[0],))
    for i in range(n_amps):
        try:
            f = interp1d(amps[i, :], drop[i, :], kind="cubic")
            steps[i] = fsolve(lambda x: f(x) + 0.01, 0)
            if steps[i] > 100:
                print(amps[i, :], drop[i, :])
        except ValueError:
            steps[i] = np.nan
    steps[steps > np.max(amps[:, -1])] = np.max(amps[:, -1])
    steps[steps < 0] = np.nan
    return steps


def plot4():
    fig, axes = plt.subplots(2, 2, sharey=True, sharex=True)

    # index order:
    # 0: Short time, high fidelity
    # 1: Short time, low fidelity
    # 2: long time, high fidelity
    # 3: long_time, low fidelity

    all_amps, all_fids = next(
        field_loader(scp.save_path("perturbation") / "spinchain_4point_perturbs.npz", ["all_amps", "all_fids"],
                     prepend_Ti=False))

    # Plot_order
    p_order = [[0, 2], [1, 3]]
    titles = [["$T < T_{min}$", "$T > T_{min}$"], ["", ""]]
    ylabels = [["Traj 1 \n Fidelity", ""], ["Traj 3 \n Fidelity", ""]]
    xlabels = [["", ""], ["Perturbation amplitude, $A_{\\epsilon}$", "Perturbation amplitude, $A_{\\epsilon}$"]]

    for i in range(2):
        for j in range(2):
            ax = axes[i, j]
            k = p_order[i][j]
            amps = all_amps[k]
            fids = all_fids[k]
            ax.plot(amps.T, fids.T, 'b', linewidth=1)
            title = titles[i][j]
            ylabel = ylabels[i][j]
            xlabel = xlabels[i][j]
            if title:
                ax.set_title(title, fontsize=defaults.fontsize)
            if ylabel:
                ax.set_ylabel(ylabel, fontsize=defaults.fontsize)
            if xlabel:
                ax.set_xlabel(xlabel, fontsize=defaults.fontsize)
            if k == 1:
                axins = ax.inset_axes([0.2, 0.6, 0.35, 0.35])
                filt = fids[:, 0] < fids[:, -1]
                amps = amps[filt, :]
                fids = fids[filt, :]
                filt = amps[0, :] < 1
                amp_in = amps[:, filt]
                fids_in = fids[:, filt]
                axins.plot(amp_in.T, fids_in.T, 'b', linewidth=1)
                # axins.set_yticks([])
                axins.set_xticklabels('')
                # axins.set_yticklabels('')
                ax.indicate_inset_zoom(axins)

    # systems.save_figure(plt, "spinchain_4point_perturbations")
    plt.show()


if __name__ == "__main__":

    mode = "plot4"

    if mode == "step_through":
        step_trough()
    elif mode == "plot4":
        # The fidelity as a function of perturbation amplitude for the 4 scenarios: short/long duration,
        # global/non-global optimum
        plot4()
