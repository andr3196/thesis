"""
Author: Andreas Springborg
Date: 2019-05-12
Goal: Perturb the optimal control in different directions, and study the fidelity decay away from the peak

"""
import itertools

import numpy as np

from pulseregression.framework.clustering.kmeans import get_cluster_centres
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.optim.optimization import calculate_fidelity_with_optimizer
from pulseregression.framework.procedure import field_loader, get_filenames
from pulseregression.framework.problems.spin import SpinChainProblem

scp = SpinChainProblem()
interpolation_fct = lambda c1, c2, alpha: alpha * c2 + (1 - alpha) * c1
n_alpha = 20


def optima_interpolations():
    """ Study how the fidelity changes in between optima by interpolating between them
    """

    # Load data
    # Short time, high fidelity
    file1 = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=84)[0]
    # Short time, low fidelity
    file2 = get_filenames(scp.save_path("swapping"), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=85, sort_tag=None)[
        0]
    # long time, high fidelity
    file3 = get_filenames(scp.load_path(), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=160)[0]
    # long_time, low fidelity
    file4 = \
        get_filenames(scp.save_path("swapping"), base="spinchain", L=scp.L, J=scp.J, g=scp.g, Ti=148, swapto_nTi=160,
                      sort_tag=None)[0]

    # Create the functions that interpolate and the optimisers to calculate the fidelity
    ctrl_pairs = []
    optims = []
    normalised_dists = []


    # Short time - only a single pair
    c1 = next(field_loader(file1, "ctrls", prepend_Ti=False))[0, :]
    c2 = next(field_loader(file2, "ctrls", prepend_Ti=False))[0, :]
    ctrl_pairs.append((c1, c2))
    optims.append(scp.generate_optim(scp.get_T(84)))
    normalised_dists.append(np.sqrt(np.sum(np.square(c1 - c2))) / np.sqrt(len(c1)*(2*scp.max_ampl)**2))

    # long time
    ctrls1, fids1 = next(field_loader(file3, ["ctrls", "fids"], prepend_Ti=False))
    ctrls2, fids2 = next(field_loader(file4, ["ctrls", "fids"], prepend_Ti=False))
    ctrls = np.concatenate([ctrls1, ctrls2], axis=0)
    fids = np.concatenate([fids1, fids2], axis=0)
    ctrls = apply_default_bessel_filter(ctrls, scp.bounds())
    ctrs = get_cluster_centres(ctrls, fids, scp.max_ampl, transform_f="mean")

    c_means = ctrs[0]
    f_mean = ctrs[1]



    # Gather interpolation functions for each pair
    for c1, c2 in itertools.combinations(c_means, 2):
        ctrl_pairs.append((c1,c2))
        optims.append(scp.generate_optim(scp.get_T(160)))
        normalised_dists.append(np.sqrt(np.sum(np.square(c1-c2)))/np.sqrt(len(c1)*(2*scp.max_ampl)**2))
    n_interps = len(ctrl_pairs)

    # Collect the fidelity at each point interpolated between each pair of optima
    alpha_values = np.linspace(0, 1, n_alpha)
    trajectories = np.zeros((n_interps, n_alpha))
    for i, pair, o in zip(range(n_interps), ctrl_pairs, optims):
        c1, c2 = pair
        for j, a in enumerate(alpha_values):
            c_next = interpolation_fct(c1, c2, a)
            trajectories[i, j] = calculate_fidelity_with_optimizer(o, seed=c_next)
        print("Finished interpolation: {:1.3f}%".format((i + 1) / n_interps * 100))

    np.savez(scp.save_path("interpolations") / "spinchain_optima_interpolations", alpha=alpha_values,
             trajectories=trajectories, norm_dists=np.array(normalised_dists), ctrls=c_means, fids=f_mean)


if __name__ == "__main__":
    mode = "interpolation"

    if mode == "interpolation":
        optima_interpolations()
