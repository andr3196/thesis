"""
Author: Andreas Springborg
Date: 2019-05-16
Goal:

"""
import itertools
import matplotlib as mpl

import numpy as np
from sklearn.manifold import Isomap

from plotguard import pyplot as plt
from pulseregression.framework.procedure import field_loader
from pulseregression.framework.problems.spin import SpinChainProblem

scp = SpinChainProblem()

interpolation_fct = lambda c1, c2, alpha: alpha * c2 + (1 - alpha) * c1


def interpolate_optima():
    path = scp.save_path("interpolations") / "spinchain_optima_interpolations.npz"
    alpha_values, trajectories, norm_dists = next(
        field_loader(path, ["alpha", "trajectories", "norm_dists"], prepend_Ti=False))
    dists = np.outer(norm_dists, alpha_values)
    plt.plot(dists.T, trajectories.T)
    plt.show()


def graph_plot():
    path = scp.save_path("interpolations") / "spinchain_optima_interpolations.npz"
    alpha_values, trajectories, ctrls, fids = next(
        field_loader(path, ["alpha", "trajectories", "ctrls", "fids"], prepend_Ti=False))

    model = Isomap()
    emb_ctrls = model.fit_transform(ctrls)

    cmap = plt.get_cmap("jet")
    k = 0
    for i, j in itertools.combinations(np.arange(ctrls.shape[0]), 2):
        p1 = emb_ctrls[i, :]
        p2 = emb_ctrls[j, :]
        traj = trajectories[k, :]
        p_last = p1
        for i, a in enumerate(alpha_values[1:]):
            p = interpolation_fct(p1, p2, a)

            plt.plot([p_last[0], p[0]], [p_last[1], p[1]], color=cmap(np.mean(traj[i: i + 2])), linewidth=4)
            p_last = p

        k += 1
    plt.scatter(emb_ctrls[:, 0], emb_ctrls[:, 1], s=100, c=fids, cmap=cmap)

    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    bar = plt.colorbar(sm)

    plt.show()


if __name__ == "__main__":

    mode = "graph_plot"

    if mode == "interpolate_optima":
        interpolate_optima()
    elif mode == "graph_plot":
        graph_plot()
