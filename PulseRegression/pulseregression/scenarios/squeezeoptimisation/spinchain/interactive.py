"""
Author: Andreas Springborg
Date: 2019-05-26
Goal:

"""
from scipy.spatial.distance import pdist

from plotguard import pyplot as plt
from matplotlib import gridspec as gspc
import numpy as np
from pulseregression.framework.procedure import repetition_guard, get_filenames, field_loader, select_files
from pulseregression.scenarios.landscape.core import get_fidelity_points, gather_fidelities, get_nearest_ctrl, \
    get_optimised_point
from pulseregression.framework.problems import SpinChainProblem

scp = SpinChainProblem()

Ti_map = lambda Ti: 200


def all_data_loop(Ti_range):
    fields = ["ctrls", "fids"]
    file_list1 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, ignore="nnTi")
    file_list2 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, L=scp.L, J=scp.J, g=scp.g,
                               nTi=92)
    file_list3 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag=None, L=scp.L, J=scp.J, g=scp.g,
                               nTi=141)

    for Ti in Ti_range:
        files = select_files(file_list1, "_nTi_{}.npz".format(Ti))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}.npz".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}.npz".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}.npz".format(Ti)))

        for file in files:
            out = next(field_loader(file, fields, prepend_Ti=False))
            if not isinstance(out, list) and not isinstance(out, tuple):
                out = [out]
            for data in zip(*out):
                yield (Ti,) + tuple(data)


def get_swap_points():
    Ti_range = np.arange(0, 200)
    points = [point for point in all_data_loop(Ti_range)]
    Tis = [point[0] for point in points]
    Fs = [point[2] for point in points]
    ctrls = [point[1] for point in points]
    return Tis, Fs, ctrls


def get_flip_points():
    f = np.load(scp.save_path("swapping") / "spinchain_L_6_J_1_g_1_flipping.npz")
    points = f["points"]
    Tis = [point[0] for point in points]
    Fs = [point[3] for point in points]
    ctrls = [point[2] for point in points]
    return Tis, Fs, ctrls


def simple_plot():
    # check to see if savefile is available
    savefile = scp.save_path("swapping") / "end_points_L_{}_J_{}_g_{}_run_1.npz".format(scp.L, scp.J, scp.g)
    repetition_guard(savefile)

    # Get files
    file_list = scp.get_load_files()

    # Get previously set points
    file_list2 = get_filenames(scp.save_path("swapping"), base="interactive_swapping", L=scp.L, J=scp.J, g=scp.g,
                               sort_tag=None)
    Tis2, Fs2, ctrls2 = get_fidelity_points(file_list2)

    # Get points obtained from swapping
    Tis3, Fs3, ctrls3 = get_swap_points()
    Tis2.extend(Tis3), Fs2.extend(Fs3), ctrls2.extend(ctrls3)

    # Get points obtained from flipping
    Tis4, Fs4, ctrls4 = get_flip_points()
    Tis2.extend(Tis4), Fs2.extend(Fs4), ctrls2.extend(ctrls4)


    n_T = len(file_list)
    times = scp.get_T(np.arange(200))
    Ts2 = scp.get_T(np.array(Tis2))

    fidelities, Tis = gather_fidelities(file_list, n_T, scp.n_seeds, continuous_index=True, return_Ti=True)

    fig = plt.figure(figsize=(14, 8))
    outer = gspc.GridSpec(2, 1, height_ratios=[4, 1])
    top = gspc.GridSpecFromSubplotSpec(1, 1,
                                       subplot_spec=outer[0])
    ax1 = plt.Subplot(fig, top[0, 0])

    selected_point = None
    cur_ctrl = None

    opt_points = []

    def pick_event(entity, event):
        nonlocal selected_point, cur_ctrl
        if selected_point is None:
            selected_point = (event.xdata, event.ydata)
            cur_ctrl, f_init, t_init = get_nearest_ctrl(selected_point, times, file_list, Ts2, Fs2, ctrls2)
            if cur_ctrl is None:
                print("No ctrl found - retry")
                return
            print("Plotting point: ({}, {}) ".format(t_init, f_init))
            ax1.plot(t_init, f_init, 'r.')
            plt.draw()
            plt.pause(0.0001)
        else:
            ti, opt_ctrl, opt_fid, T = get_optimised_point(event, cur_ctrl, scp, Ti_map)
            opt_points.append((ti, opt_ctrl, opt_fid))
            ax1.plot(T, opt_fid, 'g.')
            plt.draw()
            plt.pause(0.0001)
            selected_point = None

    for Ti, f in zip(Tis, fidelities):
        ax1.scatter(scp.get_T(Ti) * np.ones(f.shape), f, s=1, c="b", picker=pick_event)
    ax1.scatter(Ts2, Fs2, s=1, c="g")

    fig.add_subplot(ax1)

    bottom = gspc.GridSpecFromSubplotSpec(1, 1,
                                          subplot_spec=outer[1])
    n_bins = 100
    M = np.zeros((n_bins, n_T))
    bin_edges = np.linspace(0, 1, n_bins + 1)
    i = 0
    for Ti, ctrls in field_loader(file_list, "ctrls"):
        dists = pdist(ctrls) / np.sqrt(ctrls.shape[1] * 2 ** 2)
        hists, _ = np.histogram(dists, bin_edges, density=True)
        M[:, i] = hists
        i += 1

    ax = plt.Subplot(fig, bottom[0, 0])
    ax.imshow(M, cmap="plasma", aspect="auto")
    fig.add_subplot(ax)
    plt.show()

    #input("Ready to save points?")

    np.savez(savefile, points=opt_points)


if __name__ == "__main__":
    simple_plot()
