"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""
import numpy as np
from matplotlib import cm
from scipy.spatial.distance import pdist, squareform

from plotguard import pyplot as plt
from pulseregression import systems
from pulseregression.framework.clustering.kmeans import get_cluster_centres
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.procedure import select_files, field_loader, get_filenames
from pulseregression.framework.problems.spin import RandomSpinChainRealisation
from pulseregression.styling import defaults

band_width = 0.015

alpha_values = np.linspace(0, 2, 6)


def get_fidelity_bins(fidelities, threshold=0.015):
    indices = np.argsort(fidelities)
    fidelities = fidelities[indices]
    delta_fidelities = np.abs(np.diff(fidelities))
    section_bounds = delta_fidelities > threshold
    bound_indices = np.flatnonzero(section_bounds) + 1
    inds_list = np.split(indices, bound_indices)
    return inds_list




def all_data_cluster_loop(Ti_range, file_lists):
    file_list, file_list1, file_list2, file_list3 = file_lists

    for Ti in Ti_range:
        files = select_files(file_list, "_Ti_{}".format(Ti))
        files.extend(select_files(file_list1, "_nTi_{}".format(Ti)))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}".format(Ti)))

        if not files:
            continue

        ctrls_list = []
        fids_list = []
        for file in files:
            ctrls, fids = next(field_loader(file, ["ctrls", "fids"], prepend_Ti=False))
            ctrls_list.append(ctrls)
            fids_list.append(fids)

        all_ctrls = np.concatenate(ctrls_list, axis=0)
        all_fids=  np.concatenate(fids_list, axis=0)

        all_ctrls = apply_default_bessel_filter(all_ctrls, scp.bounds())
        centres, fidelities, _ = get_cluster_centres(all_ctrls, all_fids, scp.max_ampl, transform_f="mean")
        for c, f in zip(centres, fidelities):
            yield Ti, c, f




def plot_trajectories():
    Ti_range = np.arange(40, 200)
    file_lists = load_all_files()
    trajectories = sort_into_trajectories(Ti_range, file_lists, cluster=True)

    cmap = cm.get_cmap("viridis")
    for k, traj in enumerate(trajectories):
        Tis = np.array([t[0] for t in traj])
        Fs = np.array([t[2] for t in traj])
        plt.plot(Tis[0], Fs[0], color=cmap((k + 1) / len(trajectories)), label="Traj: {}".format(k))
        plt.plot(Tis[1:], Fs[1:], color=cmap((k + 1) / len(trajectories)))
    plt.show()

    plt.ylabel("Fidelity, $F$", fontsize=defaults.fontsize)
    plt.xlabel("Duration, $T$", fontsize=defaults.fontsize)
    plt.legend()
    systems.save_figure(plt, "spinchain_fidelity_trajectories")
    plt.show()


def plot_families_stack():
    Ti_range = np.arange(0, 200)

    alpha_values = [0.4, 1.2, 2.0]
    p_values = [0, 1, 3]

    fig = plt.figure(constrained_layout=True)
    #gs = GridSpec(3, 3, figure=fig, )
    axes = fig.subplots(3,3, sharey=True, sharex=True)
    #fig, axes_arr = plt.subplots(3, 4, sharex=True, sharey=True,)

    for i in range(3):

        alpha = alpha_values[i]

        for k in range(len(p_values)):
            p = p_values[k]
            ax = axes[i,k] # fig.add_subplot(gs[i, k])
            scp = RandomSpinChainRealisation(alpha, p)
            file_list = get_filenames(scp.load_path(), base="randspinchain", L=scp.L, J0=scp.J, g=scp.g,
                                      a=[alpha, ":01.1f"], p=p)

            max_ctrls = []
            Ti_min = None
            for Ti, ctrls, fids in field_loader(file_list, ["ctrls", "fids"]):
                if Ti > 16:
                    break
                ctrls = apply_default_bessel_filter(ctrls, bounds=scp.bounds())
                ctrls, fids, inds = get_cluster_centres(ctrls, fids, scp.max_ampl, "mean")
                q = np.argmax(fids)
                max_ctrls.append(ctrls[q,:])
                max_f = fids[q]
                if Ti_min is None and max_f > 0.99:
                    Ti_min = Ti
                #max_filt = max_f - fids < 0.01
                #max_ctrl = np.mean(ctrls[max_filt, :], axis=0)
                #max_ctrls.append(max_ctrl)
            max_c_arr = np.array(max_ctrls)
            cax = ax.imshow(max_c_arr, cmap="jet", aspect='auto', extent=[0, 200, scp.T_step*(16+1), scp.T_step])
            ax.axhline(0.4*(Ti_min+1), 0, 200, color='k', linestyle='-.')
            if k == 0:
                ax.set_ylabel(" $\\alpha = {:1.1f}$ \n Duration, T".format(alpha), fontsize=defaults.fontsize)
            if i == 2:
                ax.set_xlabel("Time step index, $i$", fontsize=defaults.fontsize)
            if i == 0:
                ax.set_title("p = {}".format(p))

    cbar = fig.colorbar(cax, ax=axes.ravel().tolist(), shrink=0.95)
    cbar.set_label("Control field", fontsize=defaults.fontsize)
    systems.save_figure(plt, "anisotropic_spinchain_time_correlation", )
    plt.show()


def plot_families_scan():
    Ti_range = np.arange(0, 200)
    file_lists = load_all_files()
    trajectories = sort_into_trajectories(Ti_range, file_lists, cluster=True)
    cmap = cm.get_cmap("viridis")
    for k, traj in enumerate(trajectories):
        fig = plt.figure()
        Tis = [t[0] for t in traj]
        ctrls = np.array([t[1] for t in traj])
        plt.imshow(ctrls, cmap="jet", interpolation="none", extent=[0,1,0,1])
        plt.title("Traj: {}".format(k))
        #for i in range(len(Tis)):
        #    plt.plot(ctrls[i,:], color=cmap(Tis[i]/170))
    plt.show()


def plot_correlations():
    Ti_range = np.arange(40, 170)
    file_lists = load_all_files()
    trajectories = sort_into_trajectories(Ti_range, file_lists, cluster=True)
    #for traj in trajectories:


    [print(len(traj)) for traj in trajectories]
    all_ctrls = [t[1] for traj in trajectories for t in traj]


    ctrls_M = np.array(all_ctrls)

    #kms = KMeans(9)
    #labels = kms.fit_predict(ctrls_M)

    #indices = np.argsort(labels)
    #ctrls_M = ctrls_M[indices, :]
    #_, counts = np.unique(labels, return_counts=True)
    #splits = np.cumsum(counts[:-1])
    #plt.figure()
    #M_parts = np.split(ctrls_M, splits, axis=0)
    #n_parts = len(M_parts)
    #s = int(np.sqrt(n_parts)) + 1
    #plt.figure()
    #for i, part in enumerate(M_parts):
    #    plt.subplot(s*(110) + i +1)
    #    plt.plot(part.T)

    #ctrls_M = apply_default_bessel_filter(ctrls_M, scp.bounds())
    corr_M = squareform(pdist(ctrls_M, 'correlation'))

    plt.imshow(corr_M, cmap='jet', interpolation='nearest')
    plt.colorbar()
    plt.show()


def __run__():
    mode = "plot_families_stack"
    if mode == "plot_families_stack":
        plot_families_stack()
    elif mode == "plot_families_scan":
        plot_families_scan()
    else:
        raise NotImplementedError("Unknown scenario")


if __name__ == "__main__":
    __run__()
