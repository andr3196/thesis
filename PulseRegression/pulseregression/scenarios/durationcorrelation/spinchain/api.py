"""
Author: Andreas Springborg
Date: 2019-05-08
Goal:

"""

import numpy as np
from shapely.geometry import Polygon, Point
from scipy.spatial.distance import pdist, squareform, cdist
import itertools
from plotguard import pyplot as plt
from matplotlib import cm
import matplotlib as mpl
from matplotlib import gridspec as gdspc

from pulseregression import systems
from pulseregression.framework.clustering.kmeans import get_cluster_centres
from pulseregression.framework.control.filtering import apply_default_bessel_filter
from pulseregression.framework.optim.optimization import run_grape
from pulseregression.framework.procedure import get_filenames, select_files, field_loader
from pulseregression.framework.termination.termination import get_default_termination_conditions
from pulseregression.scenarios.landscape.spinchain.common import load_all_files, load_point_files, load_flip_files
from pulseregression.framework.problems.spin import SpinChainProblem
from pulseregression.styling import defaults

scp = SpinChainProblem()

band_width = 0.015


def get_fidelity_bins(fidelities, threshold=0.015):
    indices = np.argsort(fidelities)
    fidelities = fidelities[indices]
    delta_fidelities = np.abs(np.diff(fidelities))
    section_bounds = delta_fidelities > threshold
    bound_indices = np.flatnonzero(section_bounds) + 1
    inds_list = np.split(indices, bound_indices)
    return inds_list


def trajectories():
    file_list, file_list1, file_list2, file_list3 = load_all_files()

    # Find files belonging to a particular solution
    # In terms of fidelity:
    # trajectory_bands = [(0.97, 1.0), (0.94, 0.97), (0.8, 0.85), (0, 0.05)]
    trajectories = []  # [[] for _ in trajectory_bands]
    # num_points = [0 for _ in trajectory_bands]
    Ti_range_rev = np.arange(40, 200)

    th_1 = 0.1
    th_2 = 0.015
    Ti_2 = 50
    Ti_1 = 150
    # fidelity_bins_threshold_map = lambda Ti: (th_2 - th_1)/(Ti_2-Ti_1)*(Ti-Ti_1) + th_1
    # fidelity_bins_threshold_map = lambda Ti: 0.015 if Ti > 140 else 0.1
    fidelity_bins_threshold_map = lambda Ti: 0.15  # 0.015 if Ti > 140 else 0.1

    for Ti in Ti_range_rev:
        files = select_files(file_list, "_Ti_{}".format(Ti))
        files.extend(select_files(file_list1, "_nTi_{}".format(Ti)))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}".format(Ti)))

        threshold = fidelity_bins_threshold_map(Ti)

        if scp.get_T(Ti) > 2.75:
            print("stop")

        for file in files:
            ctrls, fids = next(field_loader(file, ["ctrls", "fids"], prepend_Ti=False))

            fid_bin_indices = get_fidelity_bins(fids, 0.01)
            for bin in fid_bin_indices:
                in_ctrls = ctrls[bin, :]
                in_fids = fids[bin]
                c_mean = np.mean(in_ctrls, axis=0)
                f_mean = np.mean(in_fids)

                closest_end = None
                if trajectories:
                    traj_ends = [traj[0] for traj in trajectories]
                    end_dists = [np.abs(t[1] - f_mean) if np.abs((t[0] - Ti)) < 5 else 2 for t in traj_ends]
                    end_Ti = [t[0] for t in traj_ends]
                    closest_end = np.argmin(end_dists)
                    T_end = end_Ti[closest_end]
                    dF_end = end_dists[closest_end]
                    if Ti == T_end and dF_end > 0.01:
                        closest_end = None
                    elif dF_end > 0.1:
                        closest_end = None

                if closest_end is None:
                    trajectories.append([(Ti, f_mean), [(Ti, c_mean, f_mean)]])
                else:
                    trajectories[closest_end][0] = (Ti, f_mean)
                    trajectories[closest_end][1].append((Ti, c_mean, in_fids))

                # for traj in reversed(trajectories):
                #    if np.abs((traj[0][0] - Ti)) < 5 and np.abs(traj[0][1] - f_mean) < threshold:
                #        traj[0] = (Ti, f_mean)  # 0.5 * (traj[0] + f_mean)
                #        traj[1].append((Ti, c_mean, in_fids))
                #        break
                # else:
                #    trajectories.append([(Ti, f_mean), [(Ti, c_mean, f_mean)]])

    n_traj = len(trajectories)
    print(n_traj)

    # for i, traj_band in enumerate(trajectory_bands):
    #     in_band = np.logical_and(fids > traj_band[0], fids <= traj_band[1])
    #     if not np.any(in_band):
    #         continue
    #     mean_ctrl = np.mean(ctrls[in_band, :], axis=0)
    #     mean_fid = np.mean(fids[in_band])
    #     trajectories[i].append((Ti, mean_ctrl, mean_fid))
    #     trajectory_bands[i] = (mean_fid - band_width, mean_fid + band_width)
    #     num_points[i] += 1

    cmap = cm.get_cmap("jet")
    for k, traj in enumerate(trajectories):
        Tis = [t[0] for t in traj[1]]
        Fs = [t[2] for t in traj[1]]
        Ts = scp.T_step * (np.array(Tis) + 1)

        plt.plot(Ts[0], Fs[0], color=cmap((k + 1) / len(trajectories)), label="Traj: {}".format(k))
        for i in range(1, len(Ts)):
            T_arr = [Ts[i] for _ in range(len(Fs[i]))]
            plt.plot(T_arr, Fs[i], '.', color=cmap((k + 1) / len(trajectories)))
    plt.ylabel("Fidelity, $F$", fontsize=defaults.fontsize)
    plt.xlabel("Duration, $T$", fontsize=defaults.fontsize)
    plt.legend()
    # systems.save_figure(plt, "spinchain_fidelity_trajectories")
    plt.show()

    # Plot those solutions on top of each other, color according to duration
    # trajectories = [reversed(traj) for traj in trajectories]
    cmap = cm.get_cmap("viridis", 40)
    all_ctrls = [[] for _ in trajectories]
    for i, traj_tup in enumerate(trajectories):
        ident, traj = traj_tup
        if len(traj) == 1:
            continue
        plt.figure()
        plt.title("Trajectory: {}".format(i))
        traj_iter = iter(traj)
        _, c_last, _ = next(traj_iter)
        c_mean_last = apply_default_bessel_filter(c_last, scp.bounds(), filt_val=0.2)
        all_ctrls[i].append(c_mean_last)

        for Ti, c_mean, f_mean in traj_iter:
            c_mean = apply_default_bessel_filter(c_mean, scp.bounds(), filt_val=0.2)
            all_ctrls[i].append(c_mean)
            d_c = c_mean_last - c_mean
            c_mean_last = c_mean
            plt.plot(d_c)  # , color=cmap((Ti + 1) / 200))
        all_ctrls[i].reverse()
        norm = mpl.colors.Normalize(vmin=0, vmax=200)
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
        bar = plt.colorbar(sm)
    all_ctrls = [ctrls for ctrls in all_ctrls if ctrls]
    ctrls_M = [np.stack(all_ctrls[i]) for i in range(len(all_ctrls))]
    corr_M = [squareform(pdist(ctrls_M[i], 'correlation')) for i in range(len(ctrls_M))]

    for i in range(len(corr_M)):
        plt.figure()
        plt.title("Traj: {}".format(i))
        plt.imshow(corr_M[i], cmap='hot', interpolation='nearest')
        plt.colorbar()

    plt.show()


def bifurcation():
    # Load all files (spinchain, swapping, 2nd swapping)
    file_list = get_filenames(scp.load_path(), base="spinchain")
    file_list1 = get_filenames(scp.save_path("swapping"), base="spinchain", sort_tag="nTi_", ignore="nnTi")

    Tis = np.arange(int(np.floor(2.5 / scp.dt)) - 2, int(np.floor(3.2 / scp.dt)) - 1)

    file_lists = select_files(file_list, ["Ti_{}".format(Ti) for Ti in Tis])
    file_list1s = select_files(file_list1, ["nTi_{}".format(Ti) for Ti in Tis])

    Ti_top = []
    F_top = []
    for Ti, fids in field_loader(file_lists, "fids"):
        Ti_top.append(Ti)
        F_top.append(np.mean(fids[fids > 0.95]))

    Ti_bottom = []
    F_bottom = []
    for Ti, fids in field_loader(file_list1s, "fids", duration_tags=["swapto_nTi_"]):
        print(fids[fids > 0.9])
        Ti_bottom.append(Ti)
        F_bottom.append(np.mean(fids[np.logical_and(fids > 0.9, fids < 0.97)]))

    # initial solution
    Ti_sample = 124
    init_file1 = select_files(file_list, "Ti_{}".format(Ti_sample))
    init_file2 = select_files(file_list1, "nTi_{}".format(Ti_sample))
    Ti, ctrls1, fids1 = next(field_loader(init_file1, ["ctrls", "fids"]))
    Ti, ctrls2, fids2 = next(field_loader(init_file2, ["ctrls", "fids"], duration_tags=["swapto_nTi_"]))
    ctrl_center_1 = np.mean(ctrls1, axis=0)
    ctrl_center_2 = np.mean(ctrls2, axis=0)

    # Final solutions Ti = 156
    top_file = select_files(file_list, "Ti_156")
    Ti, ctrls, fids = next(field_loader(top_file, ["ctrls", "fids"]))
    ctrl_center_top = np.mean(ctrls[fids > 0.99, :], axis=0)

    bottom_file = select_files(file_list1, "_nTi_156")
    Ti, ctrls, fids = next(field_loader(bottom_file, ["ctrls", "fids"], duration_tags=["swapto_nTi_"]))
    ctrl_center_bottom = np.mean(ctrls[np.logical_and(fids > 0.9, fids < 0.99), :], axis=0)

    plt.plot(scp.T_range[Ti_top], F_top, 'b+', markersize=8)
    plt.plot(scp.T_range[Ti_bottom], F_bottom, 'gx')
    plt.xlim([2.0, 3.7])
    plt.ylim([0.955, 1.00])

    # Inset ctrls
    ax = plt.gca()

    axins_top = ax.inset_axes([0.72, 0.70, 0.25, 0.25])
    axins_top.plot(ctrl_center_top)
    axins_bottom = ax.inset_axes([0.72, 0.15, 0.25, 0.25])
    axins_bottom.plot(ctrl_center_bottom, 'g-')
    axins_init = ax.inset_axes([0.05, 0.15, 0.25, 0.25])
    axins_init.plot(ctrl_center_1, 'b-')
    axins_init.plot(ctrl_center_2, 'g-')
    inset_axes = [axins_bottom, axins_top, axins_init]

    for a in inset_axes:
        a.set_xticklabels('')
        a.set_yticklabels('')

    plt.xlabel("Duration, $T$", fontsize=defaults.fontsize)
    plt.ylabel("Fildelity, $F$", fontsize=defaults.fontsize)
    systems.save_figure(plt, "spinchain_bifurcation_exmp1")
    plt.show()


def assign_graphical():
    file_lists = load_all_files()
    point_files = load_point_files()
    point_files.append(load_flip_files())

    Ti_range = np.arange(40, 200)

    all_data = [point for point in all_data_loop(Ti_range, file_lists)]

    if point_files is not None:
        if not isinstance(point_files, list):
            point_files = [point_files]
        for file_list in point_files:
            for points in field_loader(file_list, "points", prepend_Ti=False):
                if len(points[0]) == 4:
                    points = [(Ti, c, f) for Ti, _, c, f in points]
                all_data.extend(points)

    x = [p[0] for p in all_data]
    y = [p[2] for p in all_data]
    plt.scatter(x, y, s=1, c="k", marker=".")

    all_traj = []
    done = "n"
    while done[0] != "y":
        out = plt.ginput(n=-1, timeout=0)
        all_traj.append(out)
        x = [o[0] for o in out]
        y = [o[1] for o in out]
        x.append(out[0][0]), y.append(out[0][1])
        plt.plot(np.array(x), np.array(y), 'm')
        done = input("Are you done?")

    np.savez(scp.save_path() / "trajectory_polygons", all_traj)


def all_data_loop(Ti_range, file_lists, fields=None):
    if fields is None:
        fields = ["ctrls", "fids"]
    file_list, file_list1, file_list2, file_list3 = file_lists

    for Ti in Ti_range:
        files = select_files(file_list, "_Ti_{}.npz".format(Ti))
        files.extend(select_files(file_list1, "_nTi_{}.npz".format(Ti)))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}.npz".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}.npz".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}.npz".format(Ti)))

        for file in files:
            out = next(field_loader(file, fields, prepend_Ti=False))
            if not isinstance(out, list) and not isinstance(out, tuple):
                out = [out]
            for data in zip(*out):
                yield (Ti,) + tuple(data)


def all_data_cluster_loop(Ti_range, file_lists):
    file_list, file_list1, file_list2, file_list3 = file_lists

    for Ti in Ti_range:
        files = select_files(file_list, "_Ti_{}".format(Ti))
        files.extend(select_files(file_list1, "_nTi_{}".format(Ti)))
        files.extend(select_files(file_list1, "_ccswapto_Ti_{}".format(Ti)))
        files.extend(select_files(file_list2, "_nnTi_{}".format(Ti)))
        files.extend(select_files(file_list3, "_nnTi_{}".format(Ti)))

        if not files:
            continue

        ctrls_list = []
        fids_list = []
        for file in files:
            ctrls, fids = next(field_loader(file, ["ctrls", "fids"], prepend_Ti=False))
            ctrls_list.append(ctrls)
            fids_list.append(fids)

        all_ctrls = np.concatenate(ctrls_list, axis=0)
        all_fids = np.concatenate(fids_list, axis=0)

        all_ctrls = apply_default_bessel_filter(all_ctrls, scp.bounds())

        centres, fidelities, _ = get_cluster_centres(all_ctrls, all_fids, scp.max_ampl, transform_f="mean")
        for c, f in zip(centres, fidelities):
            yield Ti, c, f


def sort_into_trajectories(Ti_range, file_lists, fields=None, cluster=False, return_outside=False, point_files=None):
    if cluster:
        all_data = [point for point in all_data_cluster_loop(Ti_range, file_lists)]
    else:
        if fields is None:
            fields = ["ctrls", "fids"]
        all_data = [point for point in all_data_loop(Ti_range, file_lists, fields=fields)]

    if point_files is not None:
        if not isinstance(point_files, list):
            point_files = [point_files]
        for file_list in point_files:
            for points in field_loader(file_list, "points", prepend_Ti=False):
                all_data.extend(points)

    traj_file = np.load("../../../../data/distance/collected/run6/trajectory_polygons.npz")
    trajectory_polygons = traj_file["arr_0"]

    trajectories = [[] for _ in range(len(trajectory_polygons))]
    outside = []
    for point in all_data:
        if len(point) == 2:
            Ti, f = point
        elif len(point) == 4:
            Ti, _, c, f = point
            point = (Ti, c, f)
        else:
            Ti, c, f = point
        p = Point(Ti, f)
        for i in range(len(trajectory_polygons)):
            traj_poly = Polygon(trajectory_polygons[i])
            if traj_poly.contains(p):
                trajectories[i].append(point)
                break
        else:
            outside.append(point)
    if return_outside:
        return trajectories, outside
    return trajectories


def plot_trajectories():
    Ti_range = np.arange(1, 200)
    # file_lists = load_all_files()
    # trajectories, outside = sort_into_trajectories(Ti_range, file_lists, cluster=False, return_outside=True)
    trajectories, outside = get_trajectories(Ti_range, return_outside=True, force_recalculate=True)

    cmap = cm.get_cmap("viridis")
    symbols = ".*2+^"
    for k, traj in enumerate(trajectories):
        Tis = np.array([t[0] for t in traj])
        Ts = scp.get_T(Tis)
        Fs = np.array([t[2] for t in traj])
        plt.plot(Ts[0], Fs[0], symbols[k], color=cmap((k + 1) / len(trajectories)), label="Traj: {}".format(k + 1))
        plt.plot(Ts[1:], Fs[1:], symbols[k], color=cmap((k + 1) / len(trajectories)))
    Tis = np.array([t[0] for t in outside])
    Ts = scp.get_T(Tis)
    Fs = np.array([t[2] for t in outside])
    plt.plot(Ts[0], Fs[0], "rx", markersize=3, label="Not assigned")
    plt.plot(Ts[1:], Fs[1:], "rx", markersize=3)

    point_map = [4, 4, 4, 4, 2, 2, 2, 2, 3, 3, 3, 3]
    points = next(field_loader(scp.save_path("swapping") / "end_points_L_6_J_1_g_1_run_1.npz", "points", prepend_Ti=False))
    for i, p in enumerate(points):
        plt.plot(scp.get_T(p[0]), p[2], symbols[point_map[i]], color=cmap((point_map[i] + 1) / len(trajectories)))
    plt.ylabel("Fidelity, $F$", fontsize=defaults.fontsize)
    plt.xlabel("Duration, $T$", fontsize=defaults.fontsize)
    plt.legend() #loc=(0.15, 0.05))
    systems.save_figure(plt, "spinchain_fidelity_trajectories")
    plt.show()


def get_trajectories(Ti_range, return_outside=False, force_recalculate=False, return_subtrajs=False, path=None,
                     interactive=False):
    if path is None:
        path = "../../../../data/distance/collected/run6/sorted_trajectories.npz"

    try:
        if force_recalculate:
            raise FileNotFoundError("Forced recalculation")
        f = np.load(path)
        print("Loading trajectories")
        if return_outside:
            if return_subtrajs:
                return f["trajectories"], f["outside"], f["sub_trajectories"]
            else:
                return f["trajectories"], f["outside"]
        return f["trajectories"]
    except FileNotFoundError:
        file_lists = load_all_files()
        point_file_lists = load_point_files()
        flip_file = load_flip_files()
        trajectories, outside = sort_into_trajectories(Ti_range, file_lists, cluster=False,
                                                       point_files=[point_file_lists, flip_file], return_outside=True)

        sub_trajs = [[], [], [], []]
        n_trajectories = len(trajectories)
        for i in range(n_trajectories):
            l = trajectories[i]
            l_new = []

            for Ti in Ti_range:
                c_Ti = [point[1] for point in l if point[0] == Ti]
                f_Ti = [point[2] for point in l if point[0] == Ti]

                n = len(c_Ti)

                if n > 1 and l_new and interactive:
                    # multiple controls for time, select the one closest to last accepted control

                    dists = [np.linalg.norm(c_Ti_k - l_new[-1][1]) for c_Ti_k in c_Ti]
                    if i == 2:
                        plt.clf()
                        for p, c in enumerate(c_Ti):
                            plt.plot(c, label=str(p))
                        plt.legend()
                        plt.draw()
                        plt.pause(0.001)
                        str_labels = input("Select controls for sub 1")

                        str_comps = str_labels.split("-")
                        for m, sec in enumerate(str_comps):
                            if sec:
                                labels = [int(s) for s in sec.split(" ")]
                                sub = (Ti, c_Ti[labels[0]], f_Ti[labels[0]])
                                sub_trajs[m].append(sub)

                    q = np.argmin(dists)
                    l_new.append((Ti, c_Ti[q], f_Ti[q]))
                elif n > 0:
                    l_new.append((Ti, c_Ti[0], f_Ti[0]))

                # if c_Ti:
                #    c_mean = np.mean(np.array(c_Ti), axis=0)
                #    F_mean = np.mean(f_Ti)

            trajectories[i] = l_new
        np.savez("../../../../data/distance/collected/run6/sorted_trajectories.npz", sub_trajectories=sub_trajs,
                 trajectories=trajectories, outside=outside)
        if return_outside:
            return trajectories, outside
        return trajectories


def plot_families():
    Ti_range = np.arange(1, 200)
    path = "../../../../data/distance/collected/run6/sorted_subtrajectories.npz"
    _, _, subtrajs = get_trajectories(Ti_range, return_outside=True, return_subtrajs=True, path=path)
    trajectories = get_trajectories(Ti_range)
    trajectories = [t for t in itertools.chain(trajectories, subtrajs)]

    cmap = cm.get_cmap("viridis")
    for k, traj in enumerate(trajectories):
        fig = plt.figure()
        Tis = np.array([t[0] for t in traj])
        ctrls = []
        for i in Ti_range:
            indices = np.flatnonzero(Tis == i)
            l = len(indices)
            if l > 1:
                # multiple controls for time, select the one closest to last accepted control
                dists = [np.linalg.norm(ctrls[-1] - traj[j][1]) for j in indices]
                q = indices[np.argmin(dists)]
                ctrls.append(traj[q][1])
            elif l > 0:
                q = indices[0]
                ctrls.append(traj[q][1])
            else:
                ctrls.append(np.nan * np.zeros(traj[0][1].shape))
        ctrls = np.array(ctrls)

        # Determine when image begins and ends
        empty_rows = np.flatnonzero(np.isnan(ctrls[:, 0]) == False)
        i_begin = empty_rows[0]
        i_end = empty_rows[-1]
        T_begin = scp.T_step * (i_begin + Ti_range[0] + 1)
        T_end = scp.T_step * (i_end + Ti_range[0] + 1)

        # Crop controls
        ctrls = ctrls[i_begin:i_end, :]

        plt.imshow(ctrls, cmap="jet", interpolation="none", aspect=1 / 0.0148, extent=[0, 200, T_end, T_begin])
        if k == 0:
            bar = plt.colorbar()
            bar.set_label("Control amplitude", fontsize=defaults.fontsize)
        plt.ylabel("Duration, $T$", fontsize=defaults.fontsize)
        plt.xlabel("Time step index, $i$", fontsize=defaults.fontsize)



        plt.title("Traj: {}".format(k + 1))
        # for i in range(len(Tis)):
        #    plt.plot(ctrls[i,:], color=cmap(Tis[i]/170))
        # systems.save_figure(plt, "spinchain_time_correlation_traj_{}".format(k + 1))
    plt.show()


def plot_family_grid():
    Ti_range = np.arange(1, 200)
    path = "../../../../data/distance/collected/run6/sorted_subtrajectories.npz"
    _, _, subtrajs = get_trajectories(Ti_range, return_outside=True, return_subtrajs=True, path=path)
    trajectories = get_trajectories(Ti_range)
    trajectories = [t for t in itertools.chain(trajectories, subtrajs)]

    gs0 = gdspc.GridSpec(1, 2)

    gs00 = gdspc.GridSpecFromSubplotSpec(3, 1, subplot_spec=gs0[0], height_ratios=[0.5, 0.25, 0.25])
    gs01 = gdspc.GridSpecFromSubplotSpec(3, 1, subplot_spec=gs0[1])

    ax1 = plt.subplot(gs00[0])  # Trajectory 1

    ax2 = plt.subplot(gs00[1])  # Trajectory 2

    ax3 = plt.subplot(gs00[2])  # Trajectory 5

    ax4 = plt.subplot(gs01[0])  # Trajectory 4

    gs011 = gdspc.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs01[1], )

    ax5 = plt.subplot(gs011[0])  # Trajectory 3a

    ax6 = plt.subplot(gs011[1])  # Trajectory 3b
    plt.setp(ax6.get_yticklabels(), visible=False)

    ax7 = plt.subplot(gs01[2])  # Trajectory 3c

    axes_map = {0: ax1, 1: ax2, 3:ax4, 4:ax3, 5:ax5, 6: ax6, 7:ax7}
    limits_map = {5: [1.8, 3.09], 6: [1.8, 3.09], 7:[2.9, 3.69]}
    trajectory_labels = {0:"Traj 1", 1:"Traj 2", 3:"Traj 4", 4:"Traj 5", 5:"Traj 3a", 6:"Traj 3b", 7:"Traj 3c"}

    for ax in axes_map.values():
        ax.set_xticks([])

    for k, traj in enumerate(trajectories):

        try:
            ax = axes_map[k]
        except KeyError:
            continue

        Tis = np.array([t[0] for t in traj])
        ctrls = []
        for i in Ti_range:
            indices = np.flatnonzero(Tis == i)
            l = len(indices)
            if l > 1:
                # multiple controls for time, select the one closest to last accepted control
                dists = [np.linalg.norm(ctrls[-1] - traj[j][1]) for j in indices]
                q = indices[np.argmin(dists)]
                ctrls.append(traj[q][1])
            elif l > 0:
                q = indices[0]
                ctrls.append(traj[q][1])
            else:
                ctrls.append(np.nan * np.zeros(traj[0][1].shape))
        ctrls = np.array(ctrls)

        if k in limits_map:
            Ts = scp.get_T(Ti_range)
            limits = limits_map[k]
            filt = np.logical_and(Ts > limits[0], Ts <= limits[1])
            T_filt = Ts[filt]
            filt_index = np.flatnonzero(filt)
            i_begin = filt_index[0]
            i_end = filt_index[-1]
            T_begin = T_filt[0]
            T_end = T_filt[-1]
        else:
            # Determine when image begins and ends based on nans
            empty_rows = np.flatnonzero(np.isnan(ctrls[:, 0]) == False)
            i_begin = empty_rows[0]
            i_end = empty_rows[-1]
            T_begin = scp.T_step * (i_begin + Ti_range[0] + 1)
            T_end = scp.T_step * (i_end + Ti_range[0] + 1)

        # Crop controls
        ctrls = ctrls[i_begin:i_end, :]
        #asp = 1 / 0.0148
        cax = ax.imshow(ctrls, cmap="jet", interpolation="none", aspect="auto", extent=[0, 200, T_end, T_begin])
        ax.set_title(trajectory_labels[k])

        if k == 0:
            ax.set_ylabel("Duration, $T$", fontsize=defaults.latexfontsize)
            plt.colorbar(cax, ax=ax)

        if k == 4:
            ax.set_xlabel("Time step index, $i$", fontsize=defaults.latexfontsize)

    systems.save_figure(plt, "spinchain_timecorrelation_L6J1g1_all")
    plt.show()


def plot_correlations():
    Ti_range = np.arange(40, 170)
    file_lists = load_all_files()
    trajectories = get_trajectories(Ti_range)
    # for traj in trajectories:

    [print(len(traj)) for traj in trajectories]
    all_ctrls = [t[1] for traj in trajectories for t in traj]

    ctrls_M = np.array(all_ctrls)

    # kms = KMeans(9)
    # labels = kms.fit_predict(ctrls_M)

    # indices = np.argsort(labels)
    # ctrls_M = ctrls_M[indices, :]
    # _, counts = np.unique(labels, return_counts=True)
    # splits = np.cumsum(counts[:-1])
    # plt.figure()
    # M_parts = np.split(ctrls_M, splits, axis=0)
    # n_parts = len(M_parts)
    # s = int(np.sqrt(n_parts)) + 1
    # plt.figure()
    # for i, part in enumerate(M_parts):
    #    plt.subplot(s*(110) + i +1)
    #    plt.plot(part.T)

    # ctrls_M = apply_default_bessel_filter(ctrls_M, scp.bounds())
    corr_M = squareform(pdist(ctrls_M, 'correlation'))

    plt.imshow(corr_M, cmap='jet', interpolation='nearest')
    plt.colorbar()
    plt.show()


def plot_families_stack():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    Ti_range = np.arange(40, 200)
    trajectories = get_trajectories(Ti_range)

    traj = trajectories[2]

    for t in traj:
        plt.plot(np.arange(200), t[0] * np.ones((200,)), t[1])
    plt.show()


def extend_subtrajs():
    Ti_range = np.arange(40, 200)
    trajectories, outside, subtrajs = get_trajectories(Ti_range, return_outside=True, return_subtrajs=True)

    tc = get_default_termination_conditions()

    subtrajs = [s for s in subtrajs if s]
    try:
        for i, st in enumerate(subtrajs):

            # backwards prop
            give_up = False
            while not give_up:
                p0 = subtrajs[i][0]
                ctrl0 = p0[1]
                Ti = p0[0]
                print("Propagating backwards: {}".format(Ti))
                optim = scp.generate_optim(scp.get_T(Ti - 1))
                ctrl_opt, fid_opt = run_grape(optim, tc, seed=ctrl0)

                if cdist(ctrl0[np.newaxis, :], ctrl_opt[np.newaxis, :], 'correlation') < 0.3:
                    subtrajs[i].insert(0, (Ti - 1, ctrl_opt, fid_opt))
                    print("Succes!")
                else:
                    give_up = True
                    print("Failure!")
            np.savez("../../../../data/distance/collected/run6/sorted_trajectories.npz", sub_trajectories=subtrajs,
                     trajectories=trajectories, outside=outside)

            # forwards prop
            give_up = False
            k = 0
            Ti_step = 1
            Ti_step_max = 4

            while not give_up:
                p_cur = subtrajs[i][k]
                Ti = p_cur[0]

                if k >= len(subtrajs[i]) - 1:
                    do_run = True
                else:
                    do_run = subtrajs[i][k + 1][0] != Ti + Ti_step

                if do_run:  # They are not subsequent time steps
                    ctrl_cur = p_cur[1]
                    optim = scp.generate_optim(scp.get_T(Ti + Ti_step))
                    print("Propagating forward: {}".format(Ti))
                    ctrl_opt, fid_opt = run_grape(optim, tc, seed=ctrl_cur)
                    if cdist(ctrl_cur[np.newaxis, :], ctrl_opt[np.newaxis, :], 'correlation') < 0.3:
                        subtrajs[i].insert(k + 1, (Ti + Ti_step, ctrl_opt, fid_opt))
                        Ti_step = 1
                        k += 1
                        print("Succes!")
                    else:
                        if Ti_step < Ti_step_max:
                            Ti_step += 1
                        else:
                            if k < len(subtrajs) - 1:
                                k += 1
                                Ti_step = 1
                            else:
                                give_up = True
                        print("Failure!")
                else:
                    k += 1
                    Ti_step = 1
                if Ti > 180:
                    give_up = True
            print("Done trajectory {}".format(i + 1))
            np.savez("../../../../data/distance/collected/run6/sorted_trajectories.npz", sub_trajectories=subtrajs,
                     trajectories=trajectories, outside=outside)
    except Exception as e:
        np.savez("../../../../data/distance/collected/run6/sorted_trajectories.npz", sub_trajectories=subtrajs,
                 trajectories=trajectories, outside=outside)
        raise e


def __run__():
    mode = "plot_family_grid"
    if mode == "trajectory":
        trajectories()
    elif mode == "bifurcation":
        bifurcation()
    elif mode == "graphical_assign_trajectory":
        assign_graphical()
    elif mode == "plot_trajectories":
        plot_trajectories()
    elif mode == "plot_families_color":
        plot_families()
    elif mode == "plot_families_stack":
        plot_families_stack()
    elif mode == "plot_correlations":
        plot_correlations()
    elif mode == "extend_subtrajs":
        extend_subtrajs()
    elif mode == "plot_family_grid":
        plot_family_grid()


if __name__ == "__main__":
    __run__()
